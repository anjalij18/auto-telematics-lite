(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./dashboard/alerts/add-geofence/add-geofence.module": [
		"./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.module.ts",
		"dashboard-alerts-add-geofence-add-geofence-module"
	],
	"./dashboard/alerts/alerts.module": [
		"./src/app/tabs/dashboard/alerts/alerts.module.ts",
		"dashboard-alerts-alerts-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/tabs/dashboard/dashboard.module.ts",
		"dashboard-dashboard-module"
	],
	"./dashboard/map/map.module": [
		"./src/app/tabs/dashboard/map/map.module.ts",
		"default~dashboard-map-map-module~live-track-live-track-module",
		"dashboard-map-map-module"
	],
	"./dashboard/reports/report-detail/report-detail.module": [
		"./src/app/tabs/dashboard/reports/report-detail/report-detail.module.ts",
		"default~dashboard-reports-report-detail-report-detail-module~dashboard-vehicle-list-vehicle-list-mod~29c67a0a",
		"dashboard-reports-report-detail-report-detail-module"
	],
	"./dashboard/reports/reports.module": [
		"./src/app/tabs/dashboard/reports/reports.module.ts",
		"dashboard-reports-reports-module"
	],
	"./dashboard/settings/settings.module": [
		"./src/app/tabs/dashboard/settings/settings.module.ts",
		"dashboard-settings-settings-module"
	],
	"./dashboard/settings/users/add-user/add-user.module": [
		"./src/app/tabs/dashboard/settings/users/add-user/add-user.module.ts",
		"dashboard-settings-users-add-user-add-user-module"
	],
	"./dashboard/settings/users/users.module": [
		"./src/app/tabs/dashboard/settings/users/users.module.ts",
		"dashboard-settings-users-users-module"
	],
	"./dashboard/vehicle-list/add-attribute/add-attribute.module": [
		"./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.module.ts",
		"dashboard-vehicle-list-add-attribute-add-attribute-module"
	],
	"./dashboard/vehicle-list/add-device/add-device.module": [
		"./src/app/tabs/dashboard/vehicle-list/add-device/add-device.module.ts",
		"dashboard-vehicle-list-add-device-add-device-module"
	],
	"./dashboard/vehicle-list/second-tabs/second-tabs.module": [
		"./src/app/tabs/dashboard/vehicle-list/second-tabs/second-tabs.module.ts",
		"common"
	],
	"./dashboard/vehicle-list/vehicle-list.module": [
		"./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts",
		"default~dashboard-reports-report-detail-report-detail-module~dashboard-vehicle-list-vehicle-list-mod~29c67a0a",
		"common",
		"dashboard-vehicle-list-vehicle-list-module"
	],
	"./fleet-manager/fleet-manager.module": [
		"./src/app/fleet-manager/fleet-manager.module.ts",
		"fleet-manager-fleet-manager-module"
	],
	"./history/history.module": [
		"./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.module.ts",
		"history-history-module"
	],
	"./live-track/live-track.module": [
		"./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.module.ts",
		"default~dashboard-reports-report-detail-report-detail-module~dashboard-vehicle-list-vehicle-list-mod~29c67a0a",
		"default~dashboard-map-map-module~live-track-live-track-module",
		"live-track-live-track-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"login-login-module"
	],
	"./more/more.module": [
		"./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.module.ts",
		"more-more-module"
	],
	"./tabs/tabs.module": [
		"./src/app/tabs/tabs.module.ts",
		"common",
		"tabs-tabs-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		"common",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		"common",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		"common",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		"common",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/modal-filter.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/modal-filter.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-backdrop></ion-backdrop> -->\n<div style=\"position: absolute;\nheight: 100%;\nwidth: 100%;\nbackground: rgba(0,0,0,0.65);\ncolor: #fff;\">\n    <div style=\"height: auto;\nwidth: 80%;\nbackground: #fff;\nmargin: auto;\nborder-radius: 15px;\nmargin-top: 35%;\">\n        <ion-list style=\"border-radius: 15px;\">\n            <ion-item>\n                <ion-label>Alert Types</ion-label>\n                <!-- <ion-select multiple=\"true\" cancelText=\"Nah\" okText=\"Okay!\" [(ngModel)]=\"selectedAlertType\"\n                    (ionChange)=\"callThis(selectedAlertType)\"> -->\n                <ion-select multiple=\"true\" cancelText=\"Nah\" okText=\"Okay!\" [(ngModel)]=\"selectedAlertType\">\n                    <ion-select-option value=\"allEvents\">All alerts</ion-select-option>\n                    <ion-select-option value=\"geofenceExit\">Geofence exited</ion-select-option>\n                    <ion-select-option value=\"deviceStopped\">Device stopped</ion-select-option>\n                    <ion-select-option value=\"maintenance\">Maintainence required</ion-select-option>\n                    <ion-select-option value=\"deviceOnline\">Status online</ion-select-option>\n                    <ion-select-option value=\"deviceOffline\">Status offline</ion-select-option>\n                    <ion-select-option value=\"geofenceEnter\">Geofence entered</ion-select-option>\n                    <ion-select-option value=\"deviceOverspeed\">Speed limit exceeded</ion-select-option>\n                    <ion-select-option value=\"deviceUnknown\">Status unknown</ion-select-option>\n                    <ion-select-option value=\"deviceMoving\">Device Moving</ion-select-option>\n                    <ion-select-option value=\"textMessage\">Text message received</ion-select-option>\n                    <ion-select-option value=\"alarm\">Alarm</ion-select-option>\n                    <ion-select-option value=\"deviceFuelDrop\">Fuel drop</ion-select-option>\n                    <ion-select-option value=\"commandResult\">Command result</ion-select-option>\n                    <ion-select-option value=\"ignitionOn\">Ignition on</ion-select-option>\n                    <ion-select-option value=\"ignitionOff\">Ignition off</ion-select-option>\n                </ion-select>\n            </ion-item>\n            <ion-item>\n                <ion-label>From Date</ion-label>\n                <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [min]=\"twoMonthsLater\"\n                    [max]=\"today\" [(ngModel)]=\"datetimeStart\" style=\"font-size: 15px;\">\n                </ion-datetime>\n                <!-- <ion-datetime displayFormat=\"MM/DD/YYYY\" min=\"1994-03-14\" max=\"2012-12-09\"></ion-datetime> -->\n            </ion-item>\n            <ion-item>\n                <ion-label>To Date</ion-label>\n                <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\"\n                    [(ngModel)]=\"datetimeEnd\" style=\"font-size: 15px;\">\n                </ion-datetime>\n                <!-- <ion-datetime displayFormat=\"MM/DD/YYYY\" min=\"1994-03-14\" max=\"2012-12-09\"></ion-datetime> -->\n            </ion-item>\n\n            <ion-button color=\"tertiary\" expand=\"block\" (click)=\"applyFilter()\">Apply Filter</ion-button>\n        </ion-list>\n\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/maintabs/tabs/vehicle-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Alerts</ion-title>\n    <ion-icon slot=\"end\" name=\"list\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"showFilter()\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor=\"let item of eventsData\">\n      <ion-label>\n        <h3>{{data.name}}</h3>\n        <p>{{splitString(item.type)}}</p>\n      </ion-label>\n      <ion-chip slot=\"end\" color=\"tertiary\">{{item.serverTime | date:'medium'}}</ion-chip>\n    </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/auth.guard */ "./src/app/services/auth.guard.ts");




const routes = [
    // { path: '', redirectTo: 'fleet-manager', pathMatch: 'full' },
    { path: '', redirectTo: 'maintabs/tabs/dashboard', pathMatch: 'full' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    {
        path: 'maintabs',
        loadChildren: './tabs/tabs.module#TabsPageModule',
        canActivate: [_services_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    { path: 'fleet-manager', loadChildren: './fleet-manager/fleet-manager.module#FleetManagerPageModule' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _tabs_tabs_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs/tabs.service */ "./src/app/tabs/tabs.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/firebase/ngx */ "./node_modules/@ionic-native/firebase/ngx/index.js");





// import { DashboardPage } from './tabs/dashboard/dashboard.page';




// import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen/ngx';
// import { Storage } from '@ionic/storage';
// import { AppService } from './app.service';
// import { AuthService } from './services/auth.service';
// import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
// import { AppService } from './app.service';
// declare var cookieMaster: any;
// declare var lottie: any;
let AppComponent = class AppComponent {
    constructor(platform, splashScreen, 
    // private lottieSplashScreen: LottieSplashScreen,
    statusBar, tabs, authenticationService, router, 
    // private apiCall: AppService,
    navController, 
    // private toastController: ToastController,
    modalCtrl, popoverCtrl, actionSheetCtrl, 
    // private auth: AuthService
    // private push: Push
    firebase) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.tabs = tabs;
        this.authenticationService = authenticationService;
        this.router = router;
        this.navController = navController;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.firebase = firebase;
        // set up hardware back button event.
        this.lastTimeBackPress = 0;
        this.timePeriodToExit = 2000;
        this.initializeApp();
        // Initialize BackButton Eevent.
        this.backButtonEvent();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            // this.rootPage = DashboardPage;
            this.statusBar.styleDefault();
            //Check if user is logged-in; if so the dashboard will be loaded otherwise the login-page
            this.authenticationService.authState.subscribe(state => {
                if (state) {
                    console.log("user is logged in");
                    this.navController.navigateRoot(['maintabs/tabs/dashboard']);
                    this.splashScreen.hide();
                    // setTimeout(() => {
                    this.splashScreen.hide();
                    // }, 3000);
                }
                else {
                    console.log("user is NOT logged in");
                    this.navController.navigateRoot('fleet-manager');
                    // setTimeout(() => {
                    this.splashScreen.hide();
                    // }, 3000);
                }
            });
            this.firebaseSetup();
        });
    }
    // active hardware back button
    backButtonEvent() {
        this.platform.backButton.subscribe(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // close action sheet
            try {
                const element = yield this.actionSheetCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
            }
            // close popover
            try {
                const element = yield this.popoverCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
            }
            // close modal
            try {
                const element = yield this.modalCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
                console.log(error);
            }
            // close side menua
            // try {
            //   const element = await this.menu.getOpen();
            //   if (element !== null) {
            //     this.menu.close();
            //     return;
            //   }
            // } catch (error) {
            // }
            this.routerOutlets.forEach((outlet) => {
                debugger;
                if (outlet && outlet.canGoBack()) {
                    // outlet.pop();
                    this.router.navigateByUrl('/maintabs');
                }
                else if (this.router.url === '/login') {
                    if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                        // this.platform.exitApp(); // Exit from app
                        navigator['app'].exitApp(); // work for ionic 4
                    }
                    else {
                        navigator['app'].exitApp();
                        // if (this.router.isActive('/miantabs', true)) {
                        // this.alertController.create({
                        //   message: 'Do you want to exit the app?',
                        //   buttons: [
                        //     {
                        //       text: 'YES PROCCED',
                        //       handler: () => {
                        //         navigator['app'].exitApp();
                        //       }
                        //     },
                        //     {
                        //       text: 'Back',
                        //     }
                        //   ]
                        // }).then(alertEl => {
                        //   alertEl.present();
                        // })
                        // }
                        // this.toast.show(
                        //   `Press back again to exit App.`,
                        //   '2000',
                        //   'center')
                        //   .subscribe(toast => {
                        //     // console.log(JSON.stringify(toast));
                        //   });
                        // this.toast.create({
                        //   message: 'Press back again to exit App.',
                        //   duration: 2000,
                        //   position: 'middle'
                        // }).then((toastEl => {
                        //   toastEl.present();
                        // }))
                        this.lastTimeBackPress = new Date().getTime();
                    }
                }
            });
        }));
    }
    firebaseSetup() {
        this.firebase.getToken()
            .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
            .catch(error => console.error('Error getting token', error));
        this.firebase.onNotificationOpen()
            .subscribe(data => console.log(`User opened a notification ${data}`));
        this.firebase.onTokenRefresh()
            .subscribe((token) => {
            console.log(`Got a new token ${token}`);
            localStorage.setItem("notificationTokens", token);
            // this.saveToken(token);
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
    { type: _tabs_tabs_service__WEBPACK_IMPORTED_MODULE_5__["TabsService"] },
    { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_8__["Firebase"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
], AppComponent.prototype, "routerOutlets", void 0);
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
        _tabs_tabs_service__WEBPACK_IMPORTED_MODULE_5__["TabsService"],
        _services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_8__["Firebase"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.model.ts":
/*!******************************!*\
  !*** ./src/app/app.model.ts ***!
  \******************************/
/*! exports provided: URLs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URLs", function() { return URLs; });
class URLs {
    constructor() {
        this.mainUrl = 'https://www.oneqlik.in/';
    }
}


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _app_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.model */ "./src/app/app.model.ts");
/* harmony import */ var _tabs_tabs_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./tabs/tabs.service */ "./src/app/tabs/tabs.service.ts");
/* harmony import */ var _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/google-maps/ngx */ "./node_modules/@ionic-native/google-maps/ngx/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_auth_guard__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./services/auth.guard */ "./src/app/services/auth.guard.ts");
/* harmony import */ var _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/firebase/ngx */ "./node_modules/@ionic-native/firebase/ngx/index.js");
/* harmony import */ var _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./interceptors/token.interceptor */ "./src/app/interceptors/token.interceptor.ts");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
/* harmony import */ var _tabs_dashboard_vehicle_list_second_tabs_more_more_page__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./tabs/dashboard/vehicle-list/second-tabs/more/more.page */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");




// import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen/ngx';







// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
// const config: SocketIoConfig = { url: 'ws://128.199.21.17:8082/api/socket', options: {} };
// const config: SocketIoConfig = { url: 'https://www.oneqlik.in/gps', options: {} };
// const notifConfig: SocketIoConfig = { url: 'https://www.oneqlik.in/notifIO', options: {} }





// import { Push } from '@ionic-native/push/ngx';





// import { Injectable } from '@angular/core';
// import { SocketIoModule } from 'ngx-socket-io';
// import { Socket } from 'ngx-socket-io';
// @Injectable({providedIn: 'root'})
// export class SocketOne extends Socket {
//   constructor() {
//     super({ url: 'https://www.oneqlik.in/gps', options: {} });
//   }
// }
// @Injectable({providedIn: 'root'})
// export class SocketTwo extends Socket {
//   constructor() {
//     super({ url: 'https://www.oneqlik.in/notifIO', options: {} });
//   }
// }
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"], _tabs_dashboard_vehicle_list_second_tabs_more_more_page__WEBPACK_IMPORTED_MODULE_19__["ModalFilterPage"]],
        entryComponents: [_tabs_dashboard_vehicle_list_second_tabs_more_more_page__WEBPACK_IMPORTED_MODULE_19__["ModalFilterPage"]],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["IonicStorageModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_20__["FormsModule"]
            // SocketIoModule.forRoot(config)
        ],
        providers: [
            // SocketOne, SocketTwo,
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
            // LottieSplashScreen,
            _tabs_tabs_service__WEBPACK_IMPORTED_MODULE_12__["TabsService"],
            _app_model__WEBPACK_IMPORTED_MODULE_11__["URLs"],
            _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_13__["GoogleMaps"],
            _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_18__["NativeGeocoder"],
            _services_auth_guard__WEBPACK_IMPORTED_MODULE_15__["AuthGuard"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_14__["AuthenticationService"],
            // Push,
            _ionic_native_firebase_ngx__WEBPACK_IMPORTED_MODULE_16__["Firebase"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] },
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"],
                useClass: _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_17__["AuthInterceptor"],
                multi: true
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/app.service.ts":
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/*! exports provided: AppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppService", function() { return AppService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let AppService = class AppService {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    startLoading() {
        throw new Error("Method not implemented.");
    }
    authenticateUser(authParams) {
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let body = `email=${authParams.email}&password=${authParams.password}`;
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        return this.http
            .post(this.server_ip + "/api/session", body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
            // console.log(respData)
        }));
    }
    getService(passedUrl) {
        return this.http.get(passedUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getService123(passedUrl, token) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        var string = token.email + ':' + pass;
        // var string = token.email + ':' + token.name;
        // Encode the String
        var encodedString = btoa(string);
        console.log(encodedString);
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': encodedString
        });
        return this.http.get(this.server_ip + passedUrl, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    postService123(passedUrl, token, payload) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        var string = token.email + ':' + pass;
        // Encode the String
        var encodedString = btoa(string);
        console.log(encodedString);
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': encodedString
        });
        return this.http.post(this.server_ip + passedUrl, payload, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    putService123(passedUrl, token, payload) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        var string = token.email + ':' + pass;
        // Encode the String
        var encodedString = btoa(string);
        console.log(encodedString);
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': encodedString
        });
        return this.http.put(this.server_ip + passedUrl, payload, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getServiceTemp(url) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
        });
        return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    // getServiceTemp111(url, token) {
    //   if (localStorage.getItem('server_ip') !== null) {
    //     this.server_ip = localStorage.getItem('server_ip');
    //   } else {
    //     this.router.navigate(['fleet-manager']);
    //   }
    //   var string = token.email + ':' + token.name;
    //   // Encode the String
    //   var encodedString = btoa(string);
    //   let headers = new HttpHeaders(
    //     {
    //       'Content-Type': 'application/json',
    //       'Authorization': encodedString
    //     });
    //   return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true }).pipe(tap(respData => {
    //   }))
    // }
    getServiceTemp123(url, authParams) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let body = `email=${authParams.email}&password=${pass}`;
        // let body = `email=${authParams.email}&password=${authParams.name}`;
        return this.http.post(this.server_ip + url, body, { headers: headers, withCredentials: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getSessionwithToken(token) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        // let body = `email=${authParams.email}&password=${authParams.name}`;
        return this.http.get(this.server_ip + '/api/session?token=' + token, { headers: headers, withCredentials: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getReportData(url, data) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        var string = data.email + ':' + pass;
        // Encode the String
        var encodedString = btoa(string);
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': encodedString
        });
        return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true });
    }
    getCookieWithSession(passedUrl, token) {
        if (localStorage.getItem('server_ip') !== null) {
            this.server_ip = localStorage.getItem('server_ip');
        }
        else {
            this.router.navigate(['fleet-manager']);
        }
        let pass = localStorage.getItem("password");
        let body = `email=${token.email}&password=${pass}`;
        // let body = `email=${token.email}&password=${token.name}`;
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/x-www-form-urlencoded'
            // 'Authorization': encodedString
        });
        return this.http.post(this.server_ip + passedUrl, body, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getServiceWithParams(passedUrl, data) {
        return this.http.post(passedUrl, data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(respData => {
        }));
    }
    getData(eventName) {
        // return this.socket.fromEvent(eventName).pipe(map(data => data)); // with pipe rxjs6
    }
};
AppService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AppService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AppService);



/***/ }),

/***/ "./src/app/interceptors/token.interceptor.ts":
/*!***************************************************!*\
  !*** ./src/app/interceptors/token.interceptor.ts ***!
  \***************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



// import {
//     Router
// } from '@angular/router';
// import { ToastController } from '@ionic/angular';

let AuthInterceptor = class AuthInterceptor {
    intercept(req, next) {
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(event => {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                event = event.clone({
                    headers: event.headers.set('Set-Cookie', 'JSESSIONID')
                });
            }
            return event;
        }));
    }
};
AuthInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])()
], AuthInterceptor);

// export class TokenInterceptor implements HttpInterceptor {
//     constructor(private router: Router, public toastController: ToastController) {
//     }
//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         const token = localStorage.getItem('token');
//         if (token) {
//             request = request.clone({
//                 setHeaders: {
//                     'Authorization': token
//                 }
//             });
//         }
//         if (!request.headers.has('Content-Type')) {
//             request = request.clone({
//                 setHeaders: {
//                     'content-type': 'application/json'
//                 }
//             });
//         }
//         request = request.clone({
//             headers: request.headers.set('Accept', 'application/json')
//         });
//         return next.handle(request).pipe(
//             map((event: HttpEvent<any>) => {
//                 if (event instanceof HttpResponse) {
//                     console.log('event--->>>', event);
//                 }
//                 return event;
//             }),
//             catchError((error: HttpErrorResponse) => {
//                 if (error.status === 401) {
//                     if (error.error.success === false) {
//                         this.presentToast('Login failed');
//                     } else {
//                         this.router.navigate(['login']);
//                     }
//                 }
//                 return throwError(error);
//             }));
//     }
//     async presentToast(msg) {
//         const toast = await this.toastController.create({
//           message: msg,
//           duration: 2000,
//           position: 'top'
//         });
//         toast.present();
//       }
// }


/***/ }),

/***/ "./src/app/services/auth.guard.ts":
/*!****************************************!*\
  !*** ./src/app/services/auth.guard.ts ***!
  \****************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./authentication.service */ "./src/app/services/authentication.service.ts");



let AuthGuard = class AuthGuard {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    canActivate() {
        return this.authenticationService.isAuthenticated();
    }
};
AuthGuard.ctorParameters = () => [
    { type: _authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");






let AuthenticationService = class AuthenticationService {
    constructor(storage, platform, toastController, router) {
        this.storage = storage;
        this.platform = platform;
        this.toastController = toastController;
        this.router = router;
        this.authState = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        this.platform.ready().then(() => {
            this.ifLoggedIn();
        });
    }
    ifLoggedIn() {
        this.storage.get('token').then((response) => {
            if (response) {
                this.authState.next(true);
            }
        });
    }
    login() {
        this.authState.next(true);
    }
    logout() {
        this.storage.remove('token').then(val => {
            this.storage.remove('vehicleToken');
            this.router.navigate(['fleet-manager']);
            this.authState.next(false);
        });
    }
    isAuthenticated() {
        return this.authState.value;
    }
};
AuthenticationService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AuthenticationService);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-custom-class .modal-wrapper {\n  background: #222;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9zZWNvbmQtdGFicy9tb3JlL21vcmUucGFnZS5zY3NzIiwic3JjL2FwcC90YWJzL2Rhc2hib2FyZC92ZWhpY2xlLWxpc3Qvc2Vjb25kLXRhYnMvbW9yZS9tb3JlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC90YWJzL2Rhc2hib2FyZC92ZWhpY2xlLWxpc3Qvc2Vjb25kLXRhYnMvbW9yZS9tb3JlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teS1jdXN0b20tY2xhc3MgLm1vZGFsLXdyYXBwZXIge1xuICAgIGJhY2tncm91bmQ6ICMyMjI7XG4gIH0iLCIubXktY3VzdG9tLWNsYXNzIC5tb2RhbC13cmFwcGVyIHtcbiAgYmFja2dyb3VuZDogIzIyMjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.ts ***!
  \***************************************************************************/
/*! exports provided: MorePage, ModalFilterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MorePage", function() { return MorePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalFilterPage", function() { return ModalFilterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let MorePage = class MorePage {
    constructor(appService, ionStorage, modalController, loadingController) {
        this.appService = appService;
        this.ionStorage = ionStorage;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.eventsData = [];
        this.from = moment__WEBPACK_IMPORTED_MODULE_2__({ hours: 0 }).format();
        // console.log("start date", this.datetimeStart);
        this.to = moment__WEBPACK_IMPORTED_MODULE_2__().format();
        // console.log("stop date", this.to);
        if (localStorage.getItem("CurrentDevice") !== null) {
            this.data = JSON.parse(localStorage.getItem("CurrentDevice"));
            console.log("check notifications data: ", this.data);
        }
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.ionStorage.get('token').then(val => {
            this.userToken = val;
            let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString();
            this.getEventsReport(_bUrl);
        });
    }
    getEventsReport(_bUrl) {
        this.callInnerFunction(_bUrl);
    }
    callInnerFunction(_bUrl) {
        let that = this;
        let _SUrl = '/api/session';
        this.appService.getCookieWithSession(_SUrl, this.userToken)
            .subscribe((response) => {
            let res = JSON.parse(JSON.stringify(response));
            console.log("session res: ", res);
            that.loadingController.create({
                message: 'Loading alerts...'
            }).then((loadEl) => {
                loadEl.present();
                that.appService.getReportData(_bUrl, that.userToken).subscribe((response) => {
                    loadEl.dismiss();
                    let res = JSON.parse(JSON.stringify(response));
                    console.log("check report data: ", res);
                    that.eventsData = [];
                    that.eventsData = res.reverse();
                }, err => {
                    loadEl.dismiss();
                    console.log("getting error from reports", err);
                });
            });
        }, err => {
            console.log("session err: ", err);
        });
    }
    splitString(str) {
        const name = str;
        const nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
        let str1 = nameCapitalized.match(/[A-Z][a-z]+/g);
        return str1[0] + ' ' + str1[1];
    }
    showFilter() {
        this.modalController.create({
            component: ModalFilterPage,
            cssClass: 'my-custom-class'
        }).then((modalEl) => {
            modalEl.present();
            // let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString();
            modalEl.onDidDismiss().then((data) => {
                debugger;
                console.log("check modal data: ", data.data.data.data.length);
                // if (data.data.data.datefrom !== undefined) {
                let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(data.data.data.datefrom ? data.data.data.datefrom : this.from).toISOString() + '&to=' + new Date(data.data.data.dateto ? data.data.data.dateto : this.to).toISOString();
                // }
                if (data.data.data.data.length > 0) {
                    for (var i = 0; i < data.data.data.data.length; i++) {
                        _bUrl += '&type=' + data.data.data.data[i];
                    }
                }
                this.getEventsReport(_bUrl);
            });
        });
    }
};
MorePage.ctorParameters = () => [
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] }
];
MorePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-more',
        template: __webpack_require__(/*! raw-loader!./more.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.html"),
        styles: [__webpack_require__(/*! ./more.page.scss */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/more/more.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]])
], MorePage);

let ModalFilterPage = class ModalFilterPage {
    constructor(modalController) {
        this.modalController = modalController;
        this.twoMonthsLater = moment__WEBPACK_IMPORTED_MODULE_2__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = moment__WEBPACK_IMPORTED_MODULE_2__().format("YYYY-MM-DD");
    }
    dismiss(data) {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            'dismissed': true,
            'data': data
        });
    }
    callThis(data) {
        this.dismiss(data);
    }
    applyFilter() {
        let data = {
            data: this.selectedAlertType,
            datefrom: this.datetimeStart,
            dateto: this.datetimeEnd
        };
        this.dismiss(data);
    }
};
ModalFilterPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] }
];
ModalFilterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'modal-page',
        template: __webpack_require__(/*! raw-loader!./modal-filter.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/more/modal-filter.html"),
        styles: ["\n \n  "]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]])
], ModalFilterPage);



/***/ }),

/***/ "./src/app/tabs/tabs.service.ts":
/*!**************************************!*\
  !*** ./src/app/tabs/tabs.service.ts ***!
  \**************************************/
/*! exports provided: TabsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsService", function() { return TabsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let TabsService = class TabsService {
    constructor(router, platform) {
        this.router = router;
        this.platform = platform;
        this.hideTabBarPages = [
            'analytics', 'service', 'more'
        ];
        this.routeParamPages = [
            'live-track', 'history'
        ];
        this.platform.ready().then(() => {
            console.log('Core service init');
            this.navEvents();
        });
    }
    hideTabs() {
        const tabBar = document.getElementById('myTabBar');
        if (tabBar.style.display !== 'none')
            tabBar.style.display = 'none';
    }
    showTabs() {
        const tabBar = document.getElementById('myTabBar');
        if (tabBar) {
            if (tabBar.style.display !== 'flex')
                tabBar.style.display = 'flex';
        }
    }
    // A simple subscription that tells us what page we're currently navigating to.
    navEvents() {
        this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(e => e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])).subscribe((e) => {
            console.log(e);
            this.showHideTabs(e);
        });
    }
    showHideTabs(e) {
        // Result:  e.url: "/tabs/groups/new-group?type=group"
        // Split the URL up into an array.
        const urlArray = e.url.split('/');
        // Result: urlArray: ["", "tabs", "groups", "new-group?type=group"]
        // Grab the parentUrl
        const pageUrlParent = urlArray[urlArray.length - 2];
        // Grab the last page url.
        const pageUrl = urlArray[urlArray.length - 1];
        // Result: new-group?type=group
        const page = pageUrl.split('?')[0];
        // Result: new-group
        // Check if it's a routeParamPage that we need to hide on
        const hideParamPage = this.routeParamPages.indexOf(pageUrlParent) > -1 && !isNaN(Number(page));
        // Check if we should hide or show tabs.
        const shouldHide = this.hideTabBarPages.indexOf(page) > -1 || hideParamPage;
        // Result: true
        // Not ideal to set the timeout, but I haven't figured out a better method to wait until the page is in transition...
        try {
            setTimeout(() => shouldHide ? this.hideTabs() : this.showTabs(), 300);
        }
        catch (err) {
        }
    }
};
TabsService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
];
TabsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
], TabsService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/apple/Desktop/oneqlik-projects/fleet-track-traccar/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map