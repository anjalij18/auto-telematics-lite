(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-alerts-add-geofence-add-geofence-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/alerts\"></ion-back-button>\n    <ion-title>Add Geofence</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Name</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"name\"></ion-input>\n          </ion-item>\n          <ion-row>\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n              <ion-item>\n                <ion-label position=\"floating\">Description</ion-label>\n                <ion-input type=\"text\" [(ngModel)]=\"description\"></ion-input>\n              </ion-item>\n            </ion-col>\n            <ion-col size=\"6\" style=\"padding:15px;\">\n              <ion-button expand=\"block\" color=\"light\" (click)=\"addArea()\">Add Area</ion-button>\n            </ion-col>\n          </ion-row>\n\n          <!-- <div style=\"padding-left: 5px; padding-right: 5px\"> -->\n          <!-- <ion-list style=\"margin: 0px; border-radius: 25px; background: #d5dbe3;\"> -->\n          <ion-item *ngIf=\"showMap\">\n            <ion-range min=\"200\" max=\"10000\" step=\"100\" snaps=\"true\" ticks=\"false\" color=\"tertiary\" [(ngModel)]=\"fence\"\n              (ionChange)=\"callThis(fence)\">\n              <ion-icon slot=\"start\" name=\"remove-circle\" color=\"tertiary\"></ion-icon>\n              <ion-icon slot=\"end\" name=\"add-circle\" color=\"tertiary\"></ion-icon>\n            </ion-range>\n          </ion-item>\n          <ion-row *ngIf=\"showMap\">\n            <ion-col size=\"6\" style=\"padding-left: 10px;\">Radius:</ion-col>\n            <ion-col size=\"6\" style=\"text-align: right;padding-right: 10px;\">\n              <ion-badge color=\"tertiary\">{{ fence }}</ion-badge>\n            </ion-col>\n          </ion-row>\n\n        </ion-list>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div *ngIf=\"showMap\" #map111 id=\"map_geofence\" style=\"height:50%;\"></div>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-button expand=\"block\" color=\"tertiary\" (click)=\"addGeofence()\">Add Geofence</ion-button>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.module.ts ***!
  \***************************************************************************/
/*! exports provided: AddGeofencePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGeofencePageModule", function() { return AddGeofencePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_geofence_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-geofence.page */ "./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.ts");







const routes = [
    {
        path: '',
        component: _add_geofence_page__WEBPACK_IMPORTED_MODULE_6__["AddGeofencePage"]
    }
];
let AddGeofencePageModule = class AddGeofencePageModule {
};
AddGeofencePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_add_geofence_page__WEBPACK_IMPORTED_MODULE_6__["AddGeofencePage"]]
    })
], AddGeofencePageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL2FsZXJ0cy9hZGQtZ2VvZmVuY2UvYWRkLWdlb2ZlbmNlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.ts ***!
  \*************************************************************************/
/*! exports provided: AddGeofencePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGeofencePage", function() { return AddGeofencePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/google-maps/ngx */ "./node_modules/@ionic-native/google-maps/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let AddGeofencePage = class AddGeofencePage {
    constructor(apiCall, ionStorage, elementRef, toastCtrl, navCtrl) {
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.elementRef = elementRef;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.showMap = false;
        this.fence = 200;
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.ionStorage.get('token').then((val) => {
            this.userToken = val;
        });
    }
    addArea() {
        this.showMap = true;
        this.initMap();
    }
    initMap() {
        if (this.map111 === undefined) {
            let mapOptions = {
                gestures: {
                    rotate: false,
                    tilt: false,
                    compass: false
                }
            };
            let that = this;
            this.map111 = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMaps"].create('map_geofence', mapOptions);
            this.map111.getMyLocation()
                .then((location) => {
                console.log(JSON.stringify(location, null, 2));
                this.latlat = location.latLng.lat;
                this.lnglng = location.latLng.lng;
                // Move the map camera to the location with animation
                this.map111.animateCamera({
                    target: location.latLng,
                    zoom: 14,
                    tilt: 30,
                    duration: 500
                }).then(() => {
                    // this.map111.addMarker({
                    //   title: 'My Location',
                    //   draggable: true,
                    //   position: new LatLng(location.latLng.lat, location.latLng.lng),
                    // }).then((marker: Marker) => {
                    //   console.log("Marker added")
                    //   marker.on(GoogleMapsEvent.MARKER_DRAG_END)
                    //     .subscribe(() => {
                    //       // this.markerlatlong = marker.getPosition();
                    //     });
                    // })
                    // if (that.circleData != undefined) {
                    //   that.circleData.remove();
                    // }
                    // let ltln: ILatLng = { "lat": location.latLng.lat, "lng": location.latLng.lng };
                    // let circle: Circle = this.map111.addCircleSync({
                    //   'center': ltln,
                    //   'radius': that.fence,
                    //   'strokeColor': '#7044ff',
                    //   'strokeWidth': 2,
                    //   'fillColor': 'rgb(112, 68, 255, 0.5)'
                    // });
                    // that.circleData = circle;
                    // console.log("circle data: => " + that.circleData)
                });
                if (that.circleData != undefined) {
                    that.circleData.remove();
                }
                ////////////////////////////////////////
                let marker = this.map111.addMarkerSync({
                    position: location.latLng,
                    draggable: true
                });
                // Add circle
                let circle = this.map111.addCircleSync({
                    center: marker.getPosition(),
                    radius: 10,
                    fillColor: "rgba(0, 0, 255, 0.5)",
                    strokeColor: "rgba(0, 0, 255, 0.75)",
                    strokeWidth: 1
                });
                // circle.center = marker.position
                marker.bindTo("position", circle, "center");
                that.circleData = circle;
            });
        }
    }
    callThis(fence) {
        let that = this;
        that.circleData.setRadius(fence);
    }
    addGeofence() {
        if (this.name === undefined || this.fence === 0 || this.circleData === undefined) {
            this.toastCtrl.create({
                message: 'Please enter the name and select the geofence area to proceed further...',
                duration: 3000,
                position: 'middle'
            }).then((toastEl) => {
                toastEl.present();
            });
            return;
        }
        // debugger
        // console.log("bounds: " + this.circleData.getBounds())
        // console.log("check center: ", this.circleData.getCenter());
        // console.log("check radius: ", this.circleData.getRadius());
        let circleString = `CIRCLE (${this.circleData.getCenter().lat} ${this.circleData.getCenter().lng}, ${this.circleData.getRadius()})`;
        let url = '/api/geofences';
        let payload = {
            "name": this.name,
            "description": (this.description ? this.description : null),
            "area": circleString,
            "calendarId": 0
        };
        this.apiCall.postService123(url, this.userToken, payload).subscribe((response) => {
            let res = JSON.parse(JSON.stringify(response));
            if (res) {
                this.toastCtrl.create({
                    message: 'Geofence created successfully.',
                    duration: 2000,
                    position: 'middle'
                }).then((toastEl) => {
                    toastEl.present();
                });
                setTimeout(() => {
                    this.navCtrl.pop();
                }, 2000);
            }
            // console.log(res);
        }, err => {
            console.log("geofence error", err);
        });
    }
};
AddGeofencePage.ctorParameters = () => [
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map111', {
        static: true
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], AddGeofencePage.prototype, "element", void 0);
AddGeofencePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-geofence',
        template: __webpack_require__(/*! raw-loader!./add-geofence.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.html"),
        styles: [__webpack_require__(/*! ./add-geofence.page.scss */ "./src/app/tabs/dashboard/alerts/add-geofence/add-geofence.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]])
], AddGeofencePage);



/***/ })

}]);
//# sourceMappingURL=dashboard-alerts-add-geofence-add-geofence-module-es2015.js.map