(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-vehicle-list-vehicle-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-title>Devices</ion-title>\n    <!-- <ion-icon *ngIf=\"(userToken ? userToken.administrator : false)\" slot=\"end\" name=\"add\"\n      style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addDevice()\"></ion-icon> -->\n    <!-- <div *ngIf=\"userToken\"> -->\n      <ion-icon *ngIf=\"userToken ? (userToken.administrator === true || userToken.deviceLimit === -1) : false\" slot=\"end\" name=\"add\"\n        style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addDevice()\"></ion-icon>\n    <!-- </div> -->\n\n  </ion-toolbar>\n  <ion-segment scrollable mode=\"md\" (ionChange)=\"onSegmentChange($event)\" [(ngModel)]=\"segment\">\n    <ion-segment-button mode=\"md\" checked value=\"all\">\n      <ion-label>All</ion-label>\n    </ion-segment-button>\n    <ion-segment-button mode=\"md\" value=\"moving\">\n      <ion-label>Online</ion-label>\n    </ion-segment-button>\n    <ion-segment-button mode=\"md\" value=\"out_of_reach\">\n      <ion-label>Offline</ion-label>\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card *ngFor=\"let veh of vehicleList; let i = index;\">\n          <ion-row>\n            <ion-col size=\"8\" (click)=\"onVehDetail(veh)\" class=\"ion-no-padding\">\n              <ion-row>\n                <ion-col size=\"3\">\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == true && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/red-{{veh.category ? veh.category : 'car'}}.png\" title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == false && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/yellow-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == true && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == false && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == undefined && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/red-{{veh.category ? veh.category : 'car'}}.png\" title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == undefined && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/blue-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\" *ngIf=\"veh.status === 'offline' || veh.status === 'unknown'\">\n                  </ion-img>\n                </ion-col>\n\n                <ion-col size=\"9\">\n                  <ion-row>\n                    <ion-col size=\"12\">\n                      <p style=\"margin: 0px; padding: 0px; color: black;\"><b>{{veh.name}}</b></p>\n                    </ion-col>\n                    <ion-col size=\"12\">\n                      <p style=\"margin: 0px; padding: 0px; font-size: 0.8em;\">{{veh.status}} |\n                        {{veh.lastUpdate | date:'mediumDate'}}, {{veh.lastUpdate | date:'shortTime'}}</p>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-row>\n                <ion-col size=\"3\"></ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon *ngIf=\"veh.attributes.ignition === false\" name=\"key\" style=\"font-size: 1.5em; color: red;\">\n                  </ion-icon>\n                  <ion-icon *ngIf=\"veh.attributes.ignition === true\" name=\"key\" style=\"font-size: 1.5em; color: green;\">\n                  </ion-icon>\n                  <ion-icon *ngIf=\"veh.attributes.ignition === undefined\" name=\"key\" style=\"font-size: 1.5em;\">\n                  </ion-icon>\n                </ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon name=\"power\" *ngIf=\"!veh.attributes.charge\" style=\"font-size: 1.3em; color: red;\">\n                  </ion-icon>\n                  <ion-icon name=\"power\" *ngIf=\"veh.attributes.charge\" style=\"font-size: 1.3em; color: green;\">\n                  </ion-icon>\n                </ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon name=\"settings\" color=\"dark\" (click)=\"setAttribute(veh)\" color=\"tertiary\"\n                    style=\"font-size: 1.5em;\">\n                  </ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"loading more data\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll> -->\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts ***!
  \********************************************************************/
/*! exports provided: OnCreateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnCreateDirective", function() { return OnCreateDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var OnCreateDirective = /** @class */ (function () {
    function OnCreateDirective() {
        this.appOnCreate = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    OnCreateDirective.prototype.ngOnInit = function () {
        this.appOnCreate.emit('dummy');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], OnCreateDirective.prototype, "appOnCreate", void 0);
    OnCreateDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[appOnCreate]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], OnCreateDirective);
    return OnCreateDirective;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts ***!
  \********************************************************************/
/*! exports provided: VehicleListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPageModule", function() { return VehicleListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vehicle-list.page */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");
/* harmony import */ var _on_create_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./on-create.directive */ "./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");










var routes = [
    {
        path: '',
        component: _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"]
    }
];
var VehicleListPageModule = /** @class */ (function () {
    function VehicleListPageModule() {
    }
    VehicleListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"], _on_create_directive__WEBPACK_IMPORTED_MODULE_8__["OnCreateDirective"]],
            providers: [src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__["NativeGeocoder"]]
        })
    ], VehicleListPageModule);
    return VehicleListPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC92ZWhpY2xlLWxpc3QucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts ***!
  \******************************************************************/
/*! exports provided: VehicleListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPage", function() { return VehicleListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");








var VehicleListPage = /** @class */ (function () {
    function VehicleListPage(activatedRoute, router, ionicStorage, appService, alertController, toastController, dataService, // public events: Events
    geocoderApi) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.ionicStorage = ionicStorage;
        this.appService = appService;
        this.alertController = alertController;
        this.toastController = toastController;
        this.dataService = dataService;
        this.geocoderApi = geocoderApi;
        this.segment = "all";
        this.pageNo = 0;
        this.limit = 6;
        this.vehicleList = [];
        this.vehicleList123 = [];
    }
    VehicleListPage.prototype.ionViewWillEnter = function () {
    };
    VehicleListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionicStorage.get("token").then(function (val) {
            _this.userToken = val;
            console.log("user token: ", _this.userToken);
            if (_this.activatedRoute.snapshot.paramMap.get('id')) {
                console.log("activated route param map: ", _this.activatedRoute.snapshot.paramMap.get('id'));
                var idKey = _this.activatedRoute.snapshot.paramMap.get('id');
                _this.segment = idKey;
                _this.getVehicleList();
            }
            else {
                _this.segment = 'all';
                _this.getVehicleList();
            }
        });
    };
    VehicleListPage.prototype.ngOnInit = function () {
    };
    VehicleListPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.infiniteScroll = infiniteScroll;
        this.pageNo = this.pageNo + 1;
        setTimeout(function () {
            // this.getVehicleList();
            _this.getVehicleList();
        }, 500);
    };
    VehicleListPage.prototype.onSegmentChange = function (ev) {
        // console.log(ev);
        // debugger
        var stateName = ev.detail.value;
        this.segment = stateName;
        this.stat = undefined;
        this.pageNo = 0;
        this.limit = 6;
        // this.vehicleList = [];
        this.infiniteScroll = undefined;
        this.getVehicleList();
        // let tempArray = [];
        // for (var h = 0; h < this.vehicleList123.length; h++) {
        //   if (stateName === 'moving') {
        //     if (this.vehicleList123[h].status === 'online') {
        //       tempArray.push(this.vehicleList123[h]);
        //     }
        //   } else if (stateName === 'out_of_reach') {
        //     if (this.vehicleList123[h].status === 'offline') {
        //       tempArray.push(this.vehicleList123[h]);
        //     }
        //   } else if (stateName === 'all' || stateName === 'stopped') {
        //     tempArray.push(this.vehicleList123[h]);
        //   }
        // }
        // this.vehicleList = tempArray;
        // this.getVehicleList();
    };
    VehicleListPage.prototype.getVehicleList = function () {
        var _this = this;
        var _bUrl = '/api/positions';
        this.appService.getService123(_bUrl, this.userToken).subscribe(function (response) {
            var res111 = JSON.parse(JSON.stringify(response));
            var baseURLp = '/api/devices?entityId=' + _this.userToken.id;
            if (!_this.infiniteScroll) {
                _this.appService.getService123(baseURLp, _this.userToken).subscribe(function (resp) {
                    console.log("vehicle list: ", resp);
                    var res123 = JSON.parse(JSON.stringify(resp));
                    var res = [];
                    for (var i = 0; i < res123.length; i++) {
                        for (var j = 0; j < res111.length; j++) {
                            if (res111[j].deviceId === res123[i].id) {
                                res123[i].attributes = res111[j].attributes;
                            }
                        }
                    }
                    if (_this.segment === "moving") {
                        for (var t = 0; t < res123.length; t++) {
                            if (res123[t].status === 'online') {
                                res.push(res123[t]);
                            }
                        }
                    }
                    else if (_this.segment === "stopped") {
                        res = res123;
                    }
                    else if (_this.segment === "out_of_reach") {
                        for (var t = 0; t < res123.length; t++) {
                            if (res123[t].status === 'offline') {
                                res.push(res123[t]);
                            }
                        }
                    }
                    else {
                        res = res123;
                    }
                    // debugger
                    _this.vehicleList = [];
                    _this.vehicleList = res;
                    _this.vehicleList123 = res;
                }, function (err) {
                    _this.toastController
                        .create({
                        message: "Something went wrong. Please try after some time..",
                        position: "bottom",
                        duration: 1500
                    })
                        .then(function (toastEl) {
                        toastEl.present();
                    });
                });
            }
            else {
                _this.appService.getService(baseURLp).subscribe(function (resp) {
                    var that = _this;
                    var data = JSON.parse(JSON.stringify(resp)).devices;
                    for (var i = 0; i < data.length; i++) {
                        that.vehicleList.push(data[i]);
                        //console.log("vehicle list length: ", that.vehicleList.length);
                    }
                    _this.infiniteScroll.target.complete();
                }, function (err) {
                    _this.toastController
                        .create({
                        message: "Something went wrong. Please try after some time..",
                        position: "bottom",
                        duration: 1500
                    })
                        .then(function (toastEl) {
                        toastEl.present();
                    });
                    _this.infiniteScroll.target.complete();
                });
            }
        }, function (err) {
            console.log("error: ", err);
        });
    };
    VehicleListPage.prototype.onVehDetail = function (veh) {
        this.ionicStorage.set("vehicleToken", veh);
        this.dataService.setData(42, veh);
        this.router.navigateByUrl("/maintabs/tabs/vehicle-list/second-tabs/second-main-tabs/live-track/42");
    };
    VehicleListPage.prototype.setAttribute = function (veh) {
        var _this = this;
        // async presentAlertRadio() {
        this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Radio',
            inputs: [
                {
                    name: 'Set Attribute',
                    type: 'radio',
                    label: 'Set Attribute',
                    value: 'attribute'
                },
                {
                    name: 'Notifications',
                    type: 'radio',
                    label: 'Notifications',
                    value: 'notification'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: function () {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: function (data) {
                        console.log('Confirm Ok');
                        debugger;
                        if (data === 'attribute') {
                            _this.dataService.setData(45, veh);
                            _this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-attribute/45");
                        }
                        else {
                            if (data === 'notification') {
                                _this.callOther(veh);
                            }
                        }
                    }
                }
            ]
        }).then(function (alertEl) {
            alertEl.present();
        });
        // }
    };
    VehicleListPage.prototype.callOther = function (veh) {
        var _this = this;
        var alertOptions = [];
        // let url = '/api/notifications?deviceId=' + veh.id;
        var url = '/api/notifications';
        this.appService.getService123(url, this.userToken).subscribe(function (res) {
            var response = JSON.parse(JSON.stringify(res));
            console.log("check response: ", response);
            var url2 = '/api/notifications?deviceId=' + veh.id;
            _this.appService.getService123(url2, _this.userToken).subscribe(function (res) {
                var response1 = JSON.parse(JSON.stringify(res));
                var alertOptions = [];
                var alertOptions123 = [];
                for (var i = 0; i < response.length; i++) {
                    alertOptions.push({
                        name: response[i].type,
                        type: 'checkbox',
                        label: response[i].type,
                        value: response[i]
                    });
                }
                for (var j = 0; j < response1.length; j++) {
                    alertOptions123.push({
                        name: response1[j].type,
                        type: 'checkbox',
                        label: response1[j].type,
                        value: response1[j],
                        checked: true
                    });
                }
                var result = Object.values([].concat(alertOptions, alertOptions123)
                    .reduce(function (r, c) { return (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r); }, {}));
                _this.alertController.create({
                    header: 'Select Device',
                    inputs: result,
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: function () {
                                console.log('Confirm Cancel');
                            }
                        }, {
                            text: 'Ok',
                            handler: function (data) {
                                console.log('Confirm Ok', data);
                                _this.addNotification(veh, data);
                            }
                        }
                    ]
                }).then(function (alertEl) {
                    alertEl.present();
                });
            }, function (err) {
                console.log("again somthing got wrong!");
            });
        }, function (err) {
            console.log("somethinf went wrong...");
        });
    };
    VehicleListPage.prototype.addNotification = function (veh, selectedData) {
        var url = '/api/permissions';
        var that = this;
        // outerthis.stoppageReport = [];
        var i = 0, howManyTimes = selectedData.length;
        function f() {
            var payload = {
                "deviceId": veh.id,
                "notificationId": selectedData[i].id
            };
            that.appService.postService123(url, that.userToken, payload).subscribe(function (resp) {
                var res = JSON.parse(JSON.stringify(resp));
                console.log("check permissions: ", res);
            }, function (err) {
                console.log("got err: ", err);
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    VehicleListPage.prototype.device_address = function (device, index) {
        var that = this;
        that.vehicleList[index].address = "N/A";
        if (!device.latitude) {
            that.vehicleList[index].address = "N/A";
            return;
        }
        var tempcord = {
            "lat": device.latitude,
            "long": device.longitude
        };
        this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.vehicleList[index].address = str;
        });
    };
    VehicleListPage.prototype.addDevice = function () {
        // debugger
        this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-device");
    };
    VehicleListPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"] },
        { type: src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"] }
    ]; };
    VehicleListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-vehicle-list",
            template: __webpack_require__(/*! raw-loader!./vehicle-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html"),
            styles: [__webpack_require__(/*! ./vehicle-list.page.scss */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"],
            src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"]])
    ], VehicleListPage);
    return VehicleListPage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-vehicle-list-vehicle-list-module-es5.js.map