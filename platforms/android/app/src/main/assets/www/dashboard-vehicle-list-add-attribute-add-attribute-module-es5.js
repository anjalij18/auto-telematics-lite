(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-vehicle-list-add-attribute-add-attribute-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/vehicle-list\"></ion-back-button>\n    <ion-title>Add Attribute</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Name</ion-label>\n            <ion-select cancelText=\"Nah\" okText=\"Okay!\" [(ngModel)]=\"selectedName\">\n              <ion-select-option value=\"speedLimit\">Speed Limit</ion-select-option>\n          </ion-select>\n          </ion-item>\n          <ion-row>\n            <ion-col size=\"10\">\n              <ion-item>\n                <ion-label position=\"floating\">Value</ion-label>\n                <ion-input type=\"number\" [(ngModel)]=\"value\"></ion-input>\n              </ion-item>\n            </ion-col>\n            <ion-col size=\"2\">\n              <p>m/h</p>\n            </ion-col>\n          </ion-row>\n         \n          <ion-button expand=\"block\" color=\"tertiary\" (click)=\"addAttribute()\">Add attribute</ion-button>\n\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.module.ts ***!
  \***********************************************************************************/
/*! exports provided: AddAttributePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAttributePageModule", function() { return AddAttributePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_attribute_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-attribute.page */ "./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.ts");







var routes = [
    {
        path: '',
        component: _add_attribute_page__WEBPACK_IMPORTED_MODULE_6__["AddAttributePage"]
    }
];
var AddAttributePageModule = /** @class */ (function () {
    function AddAttributePageModule() {
    }
    AddAttributePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_attribute_page__WEBPACK_IMPORTED_MODULE_6__["AddAttributePage"]]
        })
    ], AddAttributePageModule);
    return AddAttributePageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9hZGQtYXR0cmlidXRlL2FkZC1hdHRyaWJ1dGUucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.ts ***!
  \*********************************************************************************/
/*! exports provided: AddAttributePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAttributePage", function() { return AddAttributePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");






var AddAttributePage = /** @class */ (function () {
    function AddAttributePage(route, toastCtrl, apiCall, ionStorage, navCtrl) {
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.navCtrl = navCtrl;
        if (this.route.snapshot.data['vehicle_data']) {
            this.vehicleData = this.route.snapshot.data['vehicle_data'];
            // this.device_type = this.data.device_model.device_type;
            console.log("parammap: ", this.vehicleData);
        }
    }
    AddAttributePage.prototype.ngOnInit = function () {
    };
    AddAttributePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionStorage.get('token').then(function (val) {
            _this.userToken = val;
        });
    };
    AddAttributePage.prototype.addAttribute = function () {
        var _this = this;
        var _a;
        if (this.selectedName === undefined || this.value === undefined) {
            this.toastCtrl.create({
                message: 'Please fill all required field and try again...',
                duration: 2000,
                position: 'middle'
            }).then(function (toastEl) {
                toastEl.present();
            });
            return;
        }
        // if(this.vehicleData.attributes) {
        //   var temp =  this.vehicleData.attributes.this.selectedName
        // }
        // debugger
        this.vehicleData.attributes = (_a = {},
            _a[this.selectedName] = this.convertNumberIntoKmperhour(this.value),
            _a);
        var payload = this.vehicleData;
        var url = "/api/devices/" + this.vehicleData.id;
        this.apiCall.putService123(url, this.userToken, payload).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            _this.toastCtrl.create({
                message: 'Attributed added successfully...!',
                duration: 2000,
                position: 'middle'
            }).then(function (toastEl) {
                toastEl.present();
            });
            setTimeout(function () {
                _this.navCtrl.pop();
            }, 2000);
        }, function (err) {
            console.log("error: ", err);
        });
    };
    AddAttributePage.prototype.convertNumberIntoKmperhour = function (valNum) {
        return valNum * 1.609344;
    };
    AddAttributePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
    ]; };
    AddAttributePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-attribute',
            template: __webpack_require__(/*! raw-loader!./add-attribute.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.html"),
            styles: [__webpack_require__(/*! ./add-attribute.page.scss */ "./src/app/tabs/dashboard/vehicle-list/add-attribute/add-attribute.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            src_app_app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], AddAttributePage);
    return AddAttributePage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-vehicle-list-add-attribute-add-attribute-module-es5.js.map