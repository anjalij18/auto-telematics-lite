(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-vehicle-list-vehicle-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-title>Devices</ion-title>\n    <!-- <ion-icon *ngIf=\"(userToken ? userToken.administrator : false)\" slot=\"end\" name=\"add\"\n      style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addDevice()\"></ion-icon> -->\n    <!-- <div *ngIf=\"userToken\"> -->\n      <ion-icon *ngIf=\"userToken ? (userToken.administrator === true || userToken.deviceLimit === -1) : false\" slot=\"end\" name=\"add\"\n        style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addDevice()\"></ion-icon>\n    <!-- </div> -->\n\n  </ion-toolbar>\n  <ion-segment scrollable mode=\"md\" (ionChange)=\"onSegmentChange($event)\" [(ngModel)]=\"segment\">\n    <ion-segment-button mode=\"md\" checked value=\"all\">\n      <ion-label>All</ion-label>\n    </ion-segment-button>\n    <ion-segment-button mode=\"md\" value=\"moving\">\n      <ion-label>Online</ion-label>\n    </ion-segment-button>\n    <ion-segment-button mode=\"md\" value=\"out_of_reach\">\n      <ion-label>Offline</ion-label>\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card *ngFor=\"let veh of vehicleList; let i = index;\">\n          <ion-row>\n            <ion-col size=\"8\" (click)=\"onVehDetail(veh)\" class=\"ion-no-padding\">\n              <ion-row>\n                <ion-col size=\"3\">\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == true && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/red-{{veh.category ? veh.category : 'car'}}.png\" title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == false && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/yellow-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == true && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == false && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/green-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == undefined && veh.attributes.motion == true\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/red-{{veh.category ? veh.category : 'car'}}.png\" title=\"{{veh.Device_Name}}\"\n                    *ngIf=\"veh.status === 'online' && veh.attributes.ignition == undefined && veh.attributes.motion == false\">\n                  </ion-img>\n                  <ion-img src=\"assets/imgs/blue-{{veh.category ? veh.category : 'car'}}.png\"\n                    title=\"{{veh.Device_Name}}\" *ngIf=\"veh.status === 'offline' || veh.status === 'unknown'\">\n                  </ion-img>\n                </ion-col>\n\n                <ion-col size=\"9\">\n                  <ion-row>\n                    <ion-col size=\"12\">\n                      <p style=\"margin: 0px; padding: 0px; color: black;\"><b>{{veh.name}}</b></p>\n                    </ion-col>\n                    <ion-col size=\"12\">\n                      <p style=\"margin: 0px; padding: 0px; font-size: 0.8em;\">{{veh.status}} |\n                        {{veh.lastUpdate | date:'mediumDate'}}, {{veh.lastUpdate | date:'shortTime'}}</p>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-row>\n                <ion-col size=\"3\"></ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon *ngIf=\"veh.attributes.ignition === false\" name=\"key\" style=\"font-size: 1.5em; color: red;\">\n                  </ion-icon>\n                  <ion-icon *ngIf=\"veh.attributes.ignition === true\" name=\"key\" style=\"font-size: 1.5em; color: green;\">\n                  </ion-icon>\n                  <ion-icon *ngIf=\"veh.attributes.ignition === undefined\" name=\"key\" style=\"font-size: 1.5em;\">\n                  </ion-icon>\n                </ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon name=\"power\" *ngIf=\"!veh.attributes.charge\" style=\"font-size: 1.3em; color: red;\">\n                  </ion-icon>\n                  <ion-icon name=\"power\" *ngIf=\"veh.attributes.charge\" style=\"font-size: 1.3em; color: green;\">\n                  </ion-icon>\n                </ion-col>\n                <ion-col size=\"3\">\n                  <ion-icon name=\"settings\" color=\"dark\" (click)=\"setAttribute(veh)\" color=\"tertiary\"\n                    style=\"font-size: 1.5em;\">\n                  </ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"loading more data\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll> -->\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts ***!
  \********************************************************************/
/*! exports provided: OnCreateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnCreateDirective", function() { return OnCreateDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let OnCreateDirective = class OnCreateDirective {
    constructor() {
        this.appOnCreate = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
        this.appOnCreate.emit('dummy');
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], OnCreateDirective.prototype, "appOnCreate", void 0);
OnCreateDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appOnCreate]'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], OnCreateDirective);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.module.ts ***!
  \********************************************************************/
/*! exports provided: VehicleListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPageModule", function() { return VehicleListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vehicle-list.page */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");
/* harmony import */ var _on_create_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./on-create.directive */ "./src/app/tabs/dashboard/vehicle-list/on-create.directive.ts");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");










const routes = [
    {
        path: '',
        component: _vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"]
    }
];
let VehicleListPageModule = class VehicleListPageModule {
};
VehicleListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_vehicle_list_page__WEBPACK_IMPORTED_MODULE_6__["VehicleListPage"], _on_create_directive__WEBPACK_IMPORTED_MODULE_8__["OnCreateDirective"]],
        providers: [src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_9__["NativeGeocoder"]]
    })
], VehicleListPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC92ZWhpY2xlLWxpc3QucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.ts ***!
  \******************************************************************/
/*! exports provided: VehicleListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleListPage", function() { return VehicleListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");








let VehicleListPage = class VehicleListPage {
    constructor(activatedRoute, router, ionicStorage, appService, alertController, toastController, dataService, // public events: Events
    geocoderApi) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.ionicStorage = ionicStorage;
        this.appService = appService;
        this.alertController = alertController;
        this.toastController = toastController;
        this.dataService = dataService;
        this.geocoderApi = geocoderApi;
        this.segment = "all";
        this.pageNo = 0;
        this.limit = 6;
        this.vehicleList = [];
        this.vehicleList123 = [];
    }
    ionViewWillEnter() {
    }
    ionViewDidEnter() {
        this.ionicStorage.get("token").then(val => {
            this.userToken = val;
            console.log("user token: ", this.userToken);
            if (this.activatedRoute.snapshot.paramMap.get('id')) {
                console.log("activated route param map: ", this.activatedRoute.snapshot.paramMap.get('id'));
                const idKey = this.activatedRoute.snapshot.paramMap.get('id');
                this.segment = idKey;
                this.getVehicleList();
            }
            else {
                this.segment = 'all';
                this.getVehicleList();
            }
        });
    }
    ngOnInit() {
    }
    doInfinite(infiniteScroll) {
        this.infiniteScroll = infiniteScroll;
        this.pageNo = this.pageNo + 1;
        setTimeout(() => {
            // this.getVehicleList();
            this.getVehicleList();
        }, 500);
    }
    onSegmentChange(ev) {
        // console.log(ev);
        // debugger
        let stateName = ev.detail.value;
        this.segment = stateName;
        this.stat = undefined;
        this.pageNo = 0;
        this.limit = 6;
        // this.vehicleList = [];
        this.infiniteScroll = undefined;
        this.getVehicleList();
        // let tempArray = [];
        // for (var h = 0; h < this.vehicleList123.length; h++) {
        //   if (stateName === 'moving') {
        //     if (this.vehicleList123[h].status === 'online') {
        //       tempArray.push(this.vehicleList123[h]);
        //     }
        //   } else if (stateName === 'out_of_reach') {
        //     if (this.vehicleList123[h].status === 'offline') {
        //       tempArray.push(this.vehicleList123[h]);
        //     }
        //   } else if (stateName === 'all' || stateName === 'stopped') {
        //     tempArray.push(this.vehicleList123[h]);
        //   }
        // }
        // this.vehicleList = tempArray;
        // this.getVehicleList();
    }
    getVehicleList() {
        let _bUrl = '/api/positions';
        this.appService.getService123(_bUrl, this.userToken).subscribe((response) => {
            let res111 = JSON.parse(JSON.stringify(response));
            let baseURLp = '/api/devices?entityId=' + this.userToken.id;
            if (!this.infiniteScroll) {
                this.appService.getService123(baseURLp, this.userToken).subscribe(resp => {
                    console.log("vehicle list: ", resp);
                    let res123 = JSON.parse(JSON.stringify(resp));
                    let res = [];
                    for (var i = 0; i < res123.length; i++) {
                        for (var j = 0; j < res111.length; j++) {
                            if (res111[j].deviceId === res123[i].id) {
                                res123[i].attributes = res111[j].attributes;
                            }
                        }
                    }
                    if (this.segment === "moving") {
                        for (var t = 0; t < res123.length; t++) {
                            if (res123[t].status === 'online') {
                                res.push(res123[t]);
                            }
                        }
                    }
                    else if (this.segment === "stopped") {
                        res = res123;
                    }
                    else if (this.segment === "out_of_reach") {
                        for (var t = 0; t < res123.length; t++) {
                            if (res123[t].status === 'offline') {
                                res.push(res123[t]);
                            }
                        }
                    }
                    else {
                        res = res123;
                    }
                    // debugger
                    this.vehicleList = [];
                    this.vehicleList = res;
                    this.vehicleList123 = res;
                }, err => {
                    this.toastController
                        .create({
                        message: "Something went wrong. Please try after some time..",
                        position: "bottom",
                        duration: 1500
                    })
                        .then(toastEl => {
                        toastEl.present();
                    });
                });
            }
            else {
                this.appService.getService(baseURLp).subscribe(resp => {
                    let that = this;
                    var data = JSON.parse(JSON.stringify(resp)).devices;
                    for (let i = 0; i < data.length; i++) {
                        that.vehicleList.push(data[i]);
                        //console.log("vehicle list length: ", that.vehicleList.length);
                    }
                    this.infiniteScroll.target.complete();
                }, err => {
                    this.toastController
                        .create({
                        message: "Something went wrong. Please try after some time..",
                        position: "bottom",
                        duration: 1500
                    })
                        .then(toastEl => {
                        toastEl.present();
                    });
                    this.infiniteScroll.target.complete();
                });
            }
        }, err => {
            console.log("error: ", err);
        });
    }
    onVehDetail(veh) {
        this.ionicStorage.set("vehicleToken", veh);
        this.dataService.setData(42, veh);
        this.router.navigateByUrl("/maintabs/tabs/vehicle-list/second-tabs/second-main-tabs/live-track/42");
    }
    setAttribute(veh) {
        // async presentAlertRadio() {
        this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Radio',
            inputs: [
                {
                    name: 'Set Attribute',
                    type: 'radio',
                    label: 'Set Attribute',
                    value: 'attribute'
                },
                {
                    name: 'Notifications',
                    type: 'radio',
                    label: 'Notifications',
                    value: 'notification'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        console.log('Confirm Ok');
                        debugger;
                        if (data === 'attribute') {
                            this.dataService.setData(45, veh);
                            this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-attribute/45");
                        }
                        else {
                            if (data === 'notification') {
                                this.callOther(veh);
                            }
                        }
                    }
                }
            ]
        }).then((alertEl) => {
            alertEl.present();
        });
        // }
    }
    callOther(veh) {
        let alertOptions = [];
        // let url = '/api/notifications?deviceId=' + veh.id;
        let url = '/api/notifications';
        this.appService.getService123(url, this.userToken).subscribe(res => {
            let response = JSON.parse(JSON.stringify(res));
            console.log("check response: ", response);
            let url2 = '/api/notifications?deviceId=' + veh.id;
            this.appService.getService123(url2, this.userToken).subscribe(res => {
                let response1 = JSON.parse(JSON.stringify(res));
                let alertOptions = [];
                let alertOptions123 = [];
                for (var i = 0; i < response.length; i++) {
                    alertOptions.push({
                        name: response[i].type,
                        type: 'checkbox',
                        label: response[i].type,
                        value: response[i]
                    });
                }
                for (var j = 0; j < response1.length; j++) {
                    alertOptions123.push({
                        name: response1[j].type,
                        type: 'checkbox',
                        label: response1[j].type,
                        value: response1[j],
                        checked: true
                    });
                }
                const result = Object.values([].concat(alertOptions, alertOptions123)
                    .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {}));
                this.alertController.create({
                    header: 'Select Device',
                    inputs: result,
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: () => {
                                console.log('Confirm Cancel');
                            }
                        }, {
                            text: 'Ok',
                            handler: (data) => {
                                console.log('Confirm Ok', data);
                                this.addNotification(veh, data);
                            }
                        }
                    ]
                }).then((alertEl) => {
                    alertEl.present();
                });
            }, err => {
                console.log("again somthing got wrong!");
            });
        }, err => {
            console.log("somethinf went wrong...");
        });
    }
    addNotification(veh, selectedData) {
        let url = '/api/permissions';
        let that = this;
        // outerthis.stoppageReport = [];
        var i = 0, howManyTimes = selectedData.length;
        function f() {
            let payload = {
                "deviceId": veh.id,
                "notificationId": selectedData[i].id
            };
            that.appService.postService123(url, that.userToken, payload).subscribe((resp) => {
                let res = JSON.parse(JSON.stringify(resp));
                console.log("check permissions: ", res);
            }, err => {
                console.log("got err: ", err);
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    }
    device_address(device, index) {
        let that = this;
        that.vehicleList[index].address = "N/A";
        if (!device.latitude) {
            that.vehicleList[index].address = "N/A";
            return;
        }
        let tempcord = {
            "lat": device.latitude,
            "long": device.longitude
        };
        this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
            .then(res => {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.vehicleList[index].address = str;
        });
    }
    addDevice() {
        // debugger
        this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-device");
    }
};
VehicleListPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"] },
    { type: src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"] }
];
VehicleListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-vehicle-list",
        template: __webpack_require__(/*! raw-loader!./vehicle-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.html"),
        styles: [__webpack_require__(/*! ./vehicle-list.page.scss */ "./src/app/tabs/dashboard/vehicle-list/vehicle-list.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
        src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
        src_app_services_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"],
        src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"]])
], VehicleListPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-vehicle-list-vehicle-list-module-es2015.js.map