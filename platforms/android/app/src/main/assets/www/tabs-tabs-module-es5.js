(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/tabs.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\n    <ion-tab-bar slot=\"bottom\" #myTabBar id=\"myTabBar\">\n        <ion-tab-button tab=\"dashboard\">\n            <ion-icon name=\"home\"></ion-icon>\n            <ion-label>Home</ion-label>\n        </ion-tab-button>\n        <ion-tab-button tab=\"map\">\n            <ion-icon name=\"map\"></ion-icon>\n            <ion-label>Map</ion-label>\n        </ion-tab-button>\n        <ion-tab-button tab=\"vehicle-list\">\n            <ion-icon name=\"car\"></ion-icon>\n            <ion-label>Vehicles</ion-label>\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"reports\">\n            <ion-icon name=\"albums\"></ion-icon>\n            <ion-label>Reports</ion-label>\n        </ion-tab-button>\n\n        <ion-tab-button tab=\"alerts\">\n            <ion-icon src=\"assets/imgs/locate.svg\"></ion-icon>\n            <ion-label>Geofences</ion-label>\n        </ion-tab-button>\n\n        <!-- <ion-tab-button tab=\"settings\">\n            <ion-icon name=\"cog\"></ion-icon>\n            <ion-label>Settings</ion-label>\n        </ion-tab-button> -->\n\n    </ion-tab-bar>\n</ion-tabs>"

/***/ }),

/***/ "./src/app/tabs/tabs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsRoutingModule", function() { return TabsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _resolver_data_resolver_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../resolver/data-resolver.service */ "./src/app/resolver/data-resolver.service.ts");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");





// import { SecondTabsPage } from './dashboard/vehicle-list/second-tabs/second-tabs.page';
var routes = [
    {
        path: "tabs",
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_4__["TabsPage"],
        children: [
            {
                path: "dashboard",
                children: [
                    {
                        path: "",
                        loadChildren: "./dashboard/dashboard.module#DashboardPageModule"
                    }
                ]
            },
            {
                path: 'vehicle-list',
                children: [
                    {
                        path: '',
                        loadChildren: "./dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule"
                    },
                    {
                        path: 'add-device',
                        loadChildren: './dashboard/vehicle-list/add-device/add-device.module#AddDevicePageModule'
                    },
                    {
                        path: 'add-attribute/:id',
                        resolve: {
                            vehicle_data: _resolver_data_resolver_service__WEBPACK_IMPORTED_MODULE_3__["DataResolverService"]
                        },
                        loadChildren: './dashboard/vehicle-list/add-attribute/add-attribute.module#AddAttributePageModule'
                    },
                    {
                        path: "second-tabs",
                        loadChildren: "./dashboard/vehicle-list/second-tabs/second-tabs.module#SecondTabsPageModule"
                    }
                ]
            },
            {
                path: "reports",
                children: [
                    {
                        path: "",
                        loadChildren: "./dashboard/reports/reports.module#ReportsPageModule"
                    },
                    {
                        path: ":id",
                        loadChildren: "./dashboard/reports/report-detail/report-detail.module#ReportDetailPageModule"
                    },
                    {
                        path: "",
                        redirectTo: "/maintabs/tabs/dashboard",
                        pathMatch: "full"
                    }
                ]
            },
            {
                path: "alerts",
                children: [
                    {
                        path: "",
                        loadChildren: "./dashboard/alerts/alerts.module#AlertsPageModule"
                    },
                    {
                        path: 'add-geofence',
                        loadChildren: './dashboard/alerts/add-geofence/add-geofence.module#AddGeofencePageModule'
                    }
                ]
            },
            {
                path: 'map',
                children: [
                    {
                        path: '',
                        loadChildren: './dashboard/map/map.module#MapPageModule'
                    }
                ]
            },
            {
                path: "settings",
                children: [
                    {
                        path: '',
                        loadChildren: './dashboard/settings/settings.module#SettingsPageModule'
                    },
                    {
                        path: 'users',
                        children: [
                            {
                                path: '',
                                loadChildren: './dashboard/settings/users/users.module#UsersPageModule'
                            },
                            {
                                path: 'add-user',
                                loadChildren: './dashboard/settings/users/add-user/add-user.module#AddUserPageModule'
                            }
                        ]
                    }
                ]
            },
            {
                path: "",
                redirectTo: "/maintabs/tabs/dashboard",
                pathMatch: "full"
            }
        ]
    }
    // { path: 'map', loadChildren: './dashboard/map/map.module#MapPageModule' }
];
var TabsRoutingModule = /** @class */ (function () {
    function TabsRoutingModule() {
    }
    TabsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TabsRoutingModule);
    return TabsRoutingModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/tabs/tabs-routing.module.ts");
/* harmony import */ var _dashboard_vehicle_list_second_tabs_second_tabs_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard/vehicle-list/second-tabs/second-tabs.module */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/second-tabs.module.ts");








var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _tabs_routing_module__WEBPACK_IMPORTED_MODULE_6__["TabsRoutingModule"],
                _dashboard_vehicle_list_second_tabs_second_tabs_module__WEBPACK_IMPORTED_MODULE_7__["SecondTabsPageModule"]
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_5__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvdGFicy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");




var TabsPage = /** @class */ (function () {
    function TabsPage(router, 
    // private toastCtrl: ToastController,
    // private push: Push,
    // private platform: Platform,
    ionicStorage) {
        this.router = router;
        this.ionicStorage = ionicStorage;
        this.userToken = {};
    }
    TabsPage.prototype.ngOnInit = function () {
    };
    // pushSetup() {
    //   this.push.createChannel({
    //     id: "ignitionon",
    //     description: "ignition on description",
    //     sound: 'ignitionon',
    //     // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
    //     importance: 4
    //   }).then(() => console.log('Channel created'));
    //   this.push.createChannel({
    //     id: "ignitionoff",
    //     description: "ignition off description",
    //     sound: 'ignitionoff',
    //     // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
    //     importance: 4
    //   }).then(() => console.log('ignitionoff Channel created'));
    //   this.push.createChannel({
    //     id: "devicepowerdisconnected",
    //     description: "devicepowerdisconnected description",
    //     sound: 'devicepowerdisconnected',
    //     // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
    //     importance: 4
    //   }).then(() => console.log('devicepowerdisconnected Channel created'));
    //   this.push.createChannel({
    //     id: "default",
    //     description: "default description",
    //     sound: 'default',
    //     // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
    //     importance: 4
    //   }).then(() => console.log('default Channel created'));
    //   this.push.createChannel({
    //     id: "notif",
    //     description: "default description",
    //     sound: 'notif',
    //     // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
    //     importance: 4
    //   }).then(() => console.log('sos Channel created'));
    //   // Delete a channel (Android O and above)
    //   // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
    //   // Return a list of currently configured channels
    //   this.push.listChannels().then((channels) => console.log('List of channels', channels))
    //   let that = this;
    //   // const options: PushOptions = {
    //     const options: PushOptions = {
    //       android: {},
    //       ios: {
    //         alert: 'true',
    //         badge: true,
    //         sound: 'false'
    //       },
    //       windows: {},
    //       browser: {
    //         pushServiceURL: 'http://push.api.phonegap.com/v1/push'
    //       }
    //     }
    //   const pushObject: PushObject = that.push.init(options);
    //   pushObject.on('notification').subscribe((notification: any) => {
    //     if (notification.additionalData.foreground) {
    //       this.toastCtrl.create({
    //         message: notification.message,
    //         duration: 2000
    //       }).then((toastEl)=> {
    //         toastEl.present();
    //       })
    //     }
    //   });
    //   pushObject.on('registration')
    //     .subscribe((registration: any) => {
    //       console.log("dashboard entered device token => " + registration.registrationId);
    //       localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
    //       that.addPushNotify();
    //     });
    //   pushObject.on('error').subscribe(error => {
    //     console.error('Error with Push plugin', error)
    //   });
    // }
    TabsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.ionicStorage.get('token').then(function (val) {
            console.log('Your age is', val);
            _this.userToken = val;
            // this.addPushNotify();
        });
    };
    TabsPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
    ]; };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es5.js.map