(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-reports-reports-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/reports/reports.page.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/reports/reports.page.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"tertiary\">\n        <ion-title>Reports</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-list lines=\"full\">\n        <!-- <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','Overspeed']\"\n            routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"speedometer\" slot=\"start\"></ion-icon>\n            <ion-label>Overspeed</ion-label>\n        </ion-item> -->\n        <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','Stoppage']\"\n            routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"stopwatch\" slot=\"start\"></ion-icon>\n            <ion-label>Stoppage</ion-label>\n        </ion-item>\n        <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','Summary']\"\n        routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"car\" slot=\"start\"></ion-icon>\n            <ion-label>Summary</ion-label>\n        </ion-item>\n        <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','Trip']\"\n        routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"menu\" slot=\"start\"></ion-icon>\n            <ion-label>Trip Report</ion-label>\n        </ion-item>\n        <!-- <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','routes']\"\n        routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"map\" slot=\"start\"></ion-icon>\n            <ion-label>Route Report</ion-label>\n        </ion-item> -->\n        <ion-item detail [routerLink]=\"['/','maintabs','tabs','reports','events']\"\n        routerLinkActive=\"router-link-active\">\n            <ion-icon name=\"list-box\" slot=\"start\"></ion-icon>\n            <ion-label>Events Report</ion-label>\n        </ion-item>\n    </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/reports/reports.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/tabs/dashboard/reports/reports.module.ts ***!
  \**********************************************************/
/*! exports provided: ReportsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsPageModule", function() { return ReportsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _reports_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reports.page */ "./src/app/tabs/dashboard/reports/reports.page.ts");







const routes = [
    {
        path: '',
        component: _reports_page__WEBPACK_IMPORTED_MODULE_6__["ReportsPage"]
    }
];
let ReportsPageModule = class ReportsPageModule {
};
ReportsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_reports_page__WEBPACK_IMPORTED_MODULE_6__["ReportsPage"]]
    })
], ReportsPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/reports/reports.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/tabs/dashboard/reports/reports.page.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3JlcG9ydHMvcmVwb3J0cy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tabs/dashboard/reports/reports.page.ts":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/reports/reports.page.ts ***!
  \********************************************************/
/*! exports provided: ReportsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsPage", function() { return ReportsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let ReportsPage = class ReportsPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
};
ReportsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ReportsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-reports',
        template: __webpack_require__(/*! raw-loader!./reports.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/reports/reports.page.html"),
        styles: [__webpack_require__(/*! ./reports.page.scss */ "./src/app/tabs/dashboard/reports/reports.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], ReportsPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-reports-reports-module-es2015.js.map