(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-settings-settings-module"],{

/***/ "./node_modules/@ionic-native/app-version/ngx/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/@ionic-native/app-version/ngx/index.js ***!
  \*************************************************************/
/*! exports provided: AppVersion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppVersion", function() { return AppVersion; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");



var AppVersion = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(AppVersion, _super);
    function AppVersion() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppVersion.prototype.getAppName = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getAppName", {}, arguments); };
    AppVersion.prototype.getPackageName = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getPackageName", {}, arguments); };
    AppVersion.prototype.getVersionCode = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getVersionCode", {}, arguments); };
    AppVersion.prototype.getVersionNumber = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getVersionNumber", {}, arguments); };
    AppVersion.pluginName = "AppVersion";
    AppVersion.plugin = "cordova-plugin-app-version";
    AppVersion.pluginRef = "cordova.getAppVersion";
    AppVersion.repo = "https://github.com/whiteoctober/cordova-plugin-app-version";
    AppVersion.platforms = ["Android", "iOS", "Windows"];
    AppVersion = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], AppVersion);
    return AppVersion;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2FwcC12ZXJzaW9uL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLDhCQUFzQyxNQUFNLG9CQUFvQixDQUFDOztJQWtDeEMsOEJBQWlCOzs7O0lBTS9DLCtCQUFVO0lBU1YsbUNBQWM7SUFXZCxtQ0FBYztJQVNkLHFDQUFnQjs7Ozs7O0lBbkNMLFVBQVU7UUFEdEIsVUFBVSxFQUFFO09BQ0EsVUFBVTtxQkFuQ3ZCO0VBbUNnQyxpQkFBaUI7U0FBcEMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG4vKipcbiAqIEBuYW1lIEFwcCBWZXJzaW9uXG4gKiBAcHJlbWllciBhcHAtdmVyc2lvblxuICogQGRlc2NyaXB0aW9uXG4gKiBSZWFkcyB0aGUgdmVyc2lvbiBvZiB5b3VyIGFwcCBmcm9tIHRoZSB0YXJnZXQgYnVpbGQgc2V0dGluZ3MuXG4gKlxuICogUmVxdWlyZXMgQ29yZG92YSBwbHVnaW46IGBjb3Jkb3ZhLXBsdWdpbi1hcHAtdmVyc2lvbmAuIEZvciBtb3JlIGluZm8sIHBsZWFzZSBzZWUgdGhlIFtDb3Jkb3ZhIEFwcCBWZXJzaW9uIGRvY3NdKGh0dHBzOi8vZ2l0aHViLmNvbS93aGl0ZW9jdG9iZXIvY29yZG92YS1wbHVnaW4tYXBwLXZlcnNpb24pLlxuICpcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgQXBwVmVyc2lvbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvYXBwLXZlcnNpb24vbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwcFZlcnNpb246IEFwcFZlcnNpb24pIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqXG4gKiB0aGlzLmFwcFZlcnNpb24uZ2V0QXBwTmFtZSgpO1xuICogdGhpcy5hcHBWZXJzaW9uLmdldFBhY2thZ2VOYW1lKCk7XG4gKiB0aGlzLmFwcFZlcnNpb24uZ2V0VmVyc2lvbkNvZGUoKTtcbiAqIHRoaXMuYXBwVmVyc2lvbi5nZXRWZXJzaW9uTnVtYmVyKCk7XG4gKlxuICogYGBgXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnQXBwVmVyc2lvbicsXG4gIHBsdWdpbjogJ2NvcmRvdmEtcGx1Z2luLWFwcC12ZXJzaW9uJyxcbiAgcGx1Z2luUmVmOiAnY29yZG92YS5nZXRBcHBWZXJzaW9uJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS93aGl0ZW9jdG9iZXIvY29yZG92YS1wbHVnaW4tYXBwLXZlcnNpb24nLFxuICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCcsICdpT1MnLCAnV2luZG93cyddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcHBWZXJzaW9uIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuICAvKipcbiAgICogUmV0dXJucyB0aGUgbmFtZSBvZiB0aGUgYXBwLCBlLmcuOiBcIk15IEF3ZXNvbWUgQXBwXCJcbiAgICogQHJldHVybnMge1Byb21pc2U8c3RyaW5nPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZ2V0QXBwTmFtZSgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBwYWNrYWdlIG5hbWUgb2YgdGhlIGFwcCwgZS5nLjogXCJjb20uZXhhbXBsZS5teWF3ZXNvbWVhcHBcIlxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxzdHJpbmc+fVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBnZXRQYWNrYWdlTmFtZSgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBidWlsZCBpZGVudGlmaWVyIG9mIHRoZSBhcHAuXG4gICAqIEluIGlPUyBhIHN0cmluZyB3aXRoIHRoZSBidWlsZCB2ZXJzaW9uIGxpa2UgXCIxLjYwOTVcIlxuICAgKiBJbiBBbmRyb2lkIGEgbnVtYmVyIGdlbmVyYXRlZCBmcm9tIHRoZSB2ZXJzaW9uIHN0cmluZywgbGlrZSAxMDIwMyBmb3IgdmVyc2lvbiBcIjEuMi4zXCJcbiAgICogQHJldHVybnMge1Byb21pc2U8c3RyaW5nIHwgbnVtYmVyPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZ2V0VmVyc2lvbkNvZGUoKTogUHJvbWlzZTxzdHJpbmcgfCBudW1iZXI+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgdmVyc2lvbiBvZiB0aGUgYXBwLCBlLmcuOiBcIjEuMi4zXCJcbiAgICogQHJldHVybnMge1Byb21pc2U8c3RyaW5nPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZ2V0VmVyc2lvbk51bWJlcigpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybjtcbiAgfVxufVxuIl19

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/settings.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/settings/settings.page.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-title>Settings</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-avatar>\n              <img src=\"assets/Images/user-image-.png\">\n            </ion-avatar>\n          </ion-col>\n          <ion-col size=\"8\">\n            <ion-title>{{userdetails.name}}</ion-title>\n            <p style=\"padding: 3px 0px 0px 20px; margin: 0px;\">{{userdetails.email}}</p>\n          </ion-col>\n          <ion-col size=\"2\" *ngIf=\"userdetails.administrator\">\n            <ion-chip color=\"tertiary\">Admin</ion-chip>\n          </ion-col>\n        </ion-row>\n        <ion-list lines=\"full\">\n          <ion-item detail (click)=\"onClick()\">\n            <ion-icon name=\"switch\"></ion-icon>\n            <ion-label>&nbsp;&nbsp;Configure Alerts</ion-label>\n          </ion-item>\n          <!-- <ion-item>\n            <ion-icon name=\"notifications\"></ion-icon>\n            <ion-label>&nbsp;&nbsp;Mute Notifications</ion-label>\n            <ion-toggle color=\"primary\"></ion-toggle>\n          </ion-item> -->\n        </ion-list>\n\n        <ion-list lines=\"full\">\n          <ion-item detail [routerLink]=\"['/', 'maintabs','tabs', 'settings', 'users']\">\n            <ion-icon name=\"people\"></ion-icon>\n            <ion-label>&nbsp;&nbsp;Users</ion-label>\n          </ion-item>\n          <!-- <ion-item detail (click)=\"cpassword()\">\n            <ion-icon name=\"key\"></ion-icon>\n            <ion-label>&nbsp;&nbsp;Change Password</ion-label>\n          </ion-item> -->\n          <!-- <ion-item detail (click)=\"support()\">\n            <ion-icon name=\"people\"></ion-icon>\n            <ion-label>&nbsp;&nbsp;Support</ion-label>\n          </ion-item> -->\n        </ion-list>\n        <ion-row>\n          <ion-col style=\"text-align: center;\">\n            <p>App version - {{app_version}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-button expand=\"block\" fill=\"outline\" style=\"color:black\" (click)=\"onLogout()\">Logout</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/settings.module.ts":
/*!************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/settings.module.ts ***!
  \************************************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.page */ "./src/app/tabs/dashboard/settings/settings.page.ts");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");








var routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }
];
var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]],
            providers: [_ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_7__["AppVersion"]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/settings/settings.page.scss":
/*!************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/settings.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-grid {\n  background-color: #f7f7f7;\n  margin-top: 25px;\n}\n\nion-list {\n  margin-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGFicy9kYXNoYm9hcmQvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRENBO0VBRUksZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1ncmlkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG59XG5pb24tbGlzdCB7XG4gICAgXG4gICAgbWFyZ2luLXRvcDogMjVweDtcbn1cbi8vIGlvbi1jb250ZW50e1xuLy8gIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2Y3ZjdmNztcbi8vIH1cbiIsImlvbi1ncmlkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cblxuaW9uLWxpc3Qge1xuICBtYXJnaW4tdG9wOiAyNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/settings.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/settings.page.ts ***!
  \**********************************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");





// import { AuthService } from 'src/app/services/auth.service';

// import { AppService } from 'src/app/app.service';

var SettingsPage = /** @class */ (function () {
    function SettingsPage(router, ionicStorage, authService, 
    // private auth: AuthService,
    // private apiCall: AppService,
    // private platform: Platform,
    alertCtrl, 
    // private loadingController: LoadingController,
    appVersion) {
        var _this = this;
        this.router = router;
        this.ionicStorage = ionicStorage;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.appVersion = appVersion;
        this.userdetails = {};
        this.appVersion.getVersionNumber().then(function (number) {
            _this.app_version = number;
        });
    }
    SettingsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.ionicStorage.get("token").then(function (val) {
            console.log("Your data is", val);
            _this.userdetails = val;
        });
    };
    SettingsPage.prototype.ngOnInit = function () {
        // this.ionicStorage.get("token").then(val => {
        //   console.log("Your data is", val);
        //   this.userdetails = val;
        // });
    };
    // onLogout() {
    //   // this.ionicStorage.remove('token').then(val => {
    //   //   this.ionicStorage.remove('vehicleToken');
    //   //   this.router.navigateByUrl("/login");
    //   // })
    //   this.authService.logout();
    //   // this.auth.logout();
    // }
    SettingsPage.prototype.onLogout = function () {
        // debugger
        // this.token = localStorage.getItem("DEVICE_TOKEN");
        // var pushdata = {};
        // if (this.platform.is('android')) {
        //   pushdata = {
        //     "uid": this.userdetails._id,
        //     "token": this.token,
        //     "os": "android"
        //   }
        // } else {
        //   pushdata = {
        //     "uid": this.userdetails._id,
        //     "token": this.token,
        //     "os": "ios"
        //   }
        // }
        var _this = this;
        this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        localStorage.clear();
                        localStorage.setItem('count', null);
                        _this.ionicStorage.clear().then(function () {
                            console.log("ionic storage cleared!");
                        });
                        // this.navCtrl.setRoot('LoginPage');
                        _this.authService.logout();
                        // this.loadingController.create({
                        //   message: "please wait..."
                        // }).then((loadEl) => {
                        //   loadEl.present();
                        //   var url = "https://www.oneqlik.in/users/PullNotification";
                        //   this.apiCall.getServiceWithParams(url, pushdata)
                        //     .subscribe(data => {
                        //       loadEl.dismiss();
                        //       // this.apiCall.stopLoading();
                        //       console.log("push notifications updated " + JSON.parse(JSON.stringify(data)).message)
                        //       localStorage.clear();
                        //       localStorage.setItem('count', null)
                        //       this.ionicStorage.clear().then(() => {
                        //         console.log("ionic storage cleared!")
                        //       })
                        //       // this.navCtrl.setRoot('LoginPage');
                        //       this.authService.logout();
                        //     },
                        //       err => {
                        //         loadEl.dismiss();
                        //         console.log(err)
                        //       });
                        // })
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        }).then(function (alertEl) {
            alertEl.present();
        });
        // alert.present();
    };
    SettingsPage.prototype.onClick = function () {
        this.router.navigateByUrl("/maintabs/tabs/settings/configure");
    };
    SettingsPage.prototype.cpassword = function () {
        this.router.navigateByUrl("/maintabs/tabs/settings/change-password");
    };
    SettingsPage.prototype.support = function () {
        this.router.navigateByUrl('/maintabs/tabs/settings/support');
    };
    SettingsPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"] }
    ]; };
    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-settings",
            template: __webpack_require__(/*! raw-loader!./settings.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/settings.page.html"),
            styles: [__webpack_require__(/*! ./settings.page.scss */ "./src/app/tabs/dashboard/settings/settings.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"]])
    ], SettingsPage);
    return SettingsPage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-settings-settings-module-es5.js.map