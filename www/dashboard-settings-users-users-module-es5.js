(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-settings-users-users-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/users.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/settings/users/users.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/settings\"></ion-back-button>\n    <ion-title>Users</ion-title>\n    <!-- <ion-icon *ngIf=\"(userToken ? userToken.administrator : false)\" slot=\"end\" name=\"add\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addUser()\"></ion-icon> -->\n    <!-- <div *ngIf=\"userToken\"> -->\n    <ion-icon *ngIf=\"(userToken ? (userToken.administrator === true || userToken.userLimit !== 0) : false)\" slot=\"end\" name=\"add\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addUser()\"></ion-icon>\n    <!-- </div> -->\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor=\"let item of userList\">\n      <ion-thumbnail slot=\"start\">\n        <img src=\"assets/Images/user-image-.png\">\n      </ion-thumbnail>\n      <ion-label class=\"ion-text-wrap\">\n        <ion-text color=\"tertiary\">\n          <h2>{{item.name | titlecase}}</h2>\n        </ion-text>\n        <p>Email - {{item.email}}</p>\n        <!-- <p>Admin - {{(item.administrator === 'true') ? 'Yes' : 'No'}}</p> -->\n        <p>Disabled - {{(item.disabled === 'true') ? 'Yes' : 'No'}}</p>\n      </ion-label>\n      <ion-badge slot=\"end\" *ngIf=\"item.administrator === true\" color=\"tertiary\">Admin</ion-badge>\n      <ion-chip slot=\"end\" (click)=\"addDevice(item)\" color=\"tertiary\">Add Device</ion-chip>\n    </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.module.ts ***!
  \***************************************************************/
/*! exports provided: UsersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageModule", function() { return UsersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _users_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users.page */ "./src/app/tabs/dashboard/settings/users/users.page.ts");







var routes = [
    {
        path: '',
        component: _users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]
    }
];
var UsersPageModule = /** @class */ (function () {
    function UsersPageModule() {
    }
    UsersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]]
        })
    ], UsersPageModule);
    return UsersPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3NldHRpbmdzL3VzZXJzL3VzZXJzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.page.ts ***!
  \*************************************************************/
/*! exports provided: UsersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPage", function() { return UsersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var UsersPage = /** @class */ (function () {
    function UsersPage(apiCall, ionStorage, route, alertController) {
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.route = route;
        this.alertController = alertController;
        this.userList = [];
        this.devicesList = [];
        this.addedDevicesList = [];
    }
    UsersPage.prototype.ngOnInit = function () { };
    UsersPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionStorage.get('token').then(function (token) {
            debugger;
            _this.userToken = token;
            _this.getCustomer();
        });
    };
    UsersPage.prototype.getCustomer = function () {
        var _this = this;
        var url = '/api/users';
        this.apiCall.getService123(url, this.userToken).subscribe(function (resp) {
            var res = JSON.parse(JSON.stringify(resp));
            console.log("users: " + res);
            _this.userList = res;
        }, function (err) {
            console.log(err);
        });
    };
    UsersPage.prototype.addUser = function () {
        this.route.navigateByUrl('/maintabs/tabs/settings/users/add-user');
    };
    UsersPage.prototype.addDevice = function (userData) {
        var _this = this;
        var url = '/api/devices';
        this.apiCall.getService123(url, this.userToken).subscribe(function (resp) {
            _this.devicesList = JSON.parse(JSON.stringify(resp));
            var _url = '/api/devices?userId=' + userData.id;
            _this.apiCall.getService123(_url, _this.userToken).subscribe(function (response) {
                _this.addedDevicesList = JSON.parse(JSON.stringify(response));
                var alertOptions = [];
                var alertOptions123 = [];
                for (var i = 0; i < _this.devicesList.length; i++) {
                    alertOptions.push({
                        name: _this.devicesList[i].name,
                        type: 'checkbox',
                        label: _this.devicesList[i].name,
                        value: _this.devicesList[i]
                    });
                }
                for (var j = 0; j < _this.addedDevicesList.length; j++) {
                    alertOptions123.push({
                        name: _this.addedDevicesList[j].name,
                        type: 'checkbox',
                        label: _this.addedDevicesList[j].name,
                        value: _this.addedDevicesList[j],
                        checked: true
                    });
                }
                var result = Object.values([].concat(alertOptions, alertOptions123)
                    .reduce(function (r, c) { return (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r); }, {}));
                // debugger
                // console.log(result);
                _this.alertController.create({
                    header: 'Select Device',
                    inputs: result,
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: function () {
                                console.log('Confirm Cancel');
                            }
                        }, {
                            text: 'Ok',
                            handler: function (data) {
                                console.log('Confirm Ok', data);
                                _this.addPermissions(userData, data);
                            }
                        }
                    ]
                }).then(function (alertEl) {
                    alertEl.present();
                });
            }, function (err) {
                console.log("got error: ", err);
            });
        }, function (err) {
            console.log("got error: ", err);
        });
    };
    UsersPage.prototype.addPermissions = function (userData, selectedData) {
        debugger;
        var url = '/api/permissions';
        var that = this;
        // outerthis.stoppageReport = [];
        var i = 0, howManyTimes = selectedData.length;
        function f() {
            var payload = {
                "userId": userData.id,
                "deviceId": selectedData[i].id
            };
            that.apiCall.postService123(url, that.userToken, payload).subscribe(function (resp) {
                var res = JSON.parse(JSON.stringify(resp));
                console.log("check permissions: ", res);
            }, function (err) {
                console.log("got err: ", err);
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    UsersPage.ctorParameters = function () { return [
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
    ]; };
    UsersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! raw-loader!./users.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/users.page.html"),
            styles: [__webpack_require__(/*! ./users.page.scss */ "./src/app/tabs/dashboard/settings/users/users.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]])
    ], UsersPage);
    return UsersPage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-settings-users-users-module-es5.js.map