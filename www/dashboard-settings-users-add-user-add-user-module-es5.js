(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-settings-users-add-user-add-user-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/add-user/add-user.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/settings/users/add-user/add-user.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/settings/users\"></ion-back-button>\n    <ion-title>Add New User</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\">Name</ion-label>\n            <ion-input type=\"text\" [(ngModel)]=\"name\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Email</ion-label>\n            <ion-input type=\"email\" [(ngModel)]=\"email\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Password</ion-label>\n            <ion-input *ngIf=\"!showPass\" type=\"password\" [(ngModel)]=\"password\"></ion-input>\n            <ion-input *ngIf=\"showPass\" type=\"text\" [(ngModel)]=\"password\"></ion-input>\n            <ion-icon *ngIf=\"!showPass\" slot=\"end\" name=\"eye-off\" (click)=\"showPass = true\"></ion-icon>\n            <ion-icon *ngIf=\"showPass\" slot=\"end\" name=\"eye\" (click)=\"showPass = false\"></ion-icon>\n          </ion-item>\n\n          <ion-item-group>\n            <ion-item-divider color=\"light\">Permissions</ion-item-divider>\n            <ion-row style=\"padding: 10px 10px 0px 10px;\">\n              <ion-col col-6>\n                <ion-label>Disabled:</ion-label>\n              </ion-col>\n              <ion-col col-6>\n                <ion-item>\n                  <ion-checkbox color=\"dark\" [(ngModel)]=\"disabled\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"padding: 0px 10px 0px 10px;\" *ngIf=\"(userToken ? userToken.administrator : false)\">\n              <ion-col col-6>\n                <ion-label>Admin:</ion-label>\n              </ion-col>\n              <ion-col col-6>\n                <ion-item>\n                  <ion-checkbox color=\"dark\" [(ngModel)]=\"admin\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"padding: 0px 10px 0px 10px;\">\n              <ion-col col-6>\n                <ion-label>Readonly:</ion-label>\n              </ion-col>\n              <ion-col col-6>\n                <ion-item>\n                  <ion-checkbox color=\"dark\" [(ngModel)]=\"readonly\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"padding: 0px 10px 0px 10px;\">\n              <ion-col col-6>\n                <ion-label>Device Readonly:</ion-label>\n              </ion-col>\n              <ion-col col-6>\n                <ion-item>\n                  <ion-checkbox color=\"dark\" [(ngModel)]=\"device_readonly\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"padding: 0px 10px 0px 10px;\">\n              <ion-col col-6>\n                <ion-label>Limit Commands:</ion-label>\n              </ion-col>\n              <ion-col col-6>\n                <ion-item>\n                  <ion-checkbox color=\"dark\" [(ngModel)]=\"limit_commands\"></ion-checkbox>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-item>\n              <ion-label position=\"floating\">Expiration:</ion-label>\n              <ion-datetime displayFormat=\"DD/MM/YYYY h:mm A\" pickerFormat=\"DD/MM/YYYY h mm A\" [(ngModel)]=\"expiration_date\"></ion-datetime>\n            </ion-item>\n            <ion-item *ngIf=\"(userToken ? userToken.administrator : false)\">\n              <ion-label position=\"floating\">Device Limit:</ion-label>\n              <ion-input type=\"text\" [(ngModel)]=\"device_limit\"></ion-input>\n            </ion-item>\n            <ion-item *ngIf=\"(userToken ? userToken.administrator : false)\">\n              <ion-label position=\"floating\">User Limit:</ion-label>\n              <ion-input type=\"text\" [(ngModel)]=\"user_limit\"></ion-input>\n            </ion-item>\n          </ion-item-group>\n          <ion-button expand=\"block\" color=\"tertiary\" (click)=\"addUser()\">Add User</ion-button>\n\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/add-user/add-user.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/add-user/add-user.module.ts ***!
  \***************************************************************************/
/*! exports provided: AddUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPageModule", function() { return AddUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-user.page */ "./src/app/tabs/dashboard/settings/users/add-user/add-user.page.ts");







var routes = [
    {
        path: '',
        component: _add_user_page__WEBPACK_IMPORTED_MODULE_6__["AddUserPage"]
    }
];
var AddUserPageModule = /** @class */ (function () {
    function AddUserPageModule() {
    }
    AddUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_user_page__WEBPACK_IMPORTED_MODULE_6__["AddUserPage"]]
        })
    ], AddUserPageModule);
    return AddUserPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/add-user/add-user.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/add-user/add-user.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3NldHRpbmdzL3VzZXJzL2FkZC11c2VyL2FkZC11c2VyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/add-user/add-user.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/add-user/add-user.page.ts ***!
  \*************************************************************************/
/*! exports provided: AddUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPage", function() { return AddUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var AddUserPage = /** @class */ (function () {
    function AddUserPage(apiCall, ionStorage, toastCtrl, navCtrl) {
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.showPass = false;
    }
    AddUserPage.prototype.ngOnInit = function () {
    };
    AddUserPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionStorage.get('token').then(function (data) {
            _this.userToken = data;
        });
    };
    AddUserPage.prototype.addUser = function () {
        var _this = this;
        var url = '/api/users';
        var payload = {
            "id": -1,
            "name": this.name,
            "login": "",
            "email": this.email,
            "password": this.password,
            "phone": "",
            "readonly": (this.readonly ? this.readonly : false),
            "administrator": false,
            "map": "",
            "latitude": 0,
            "longitude": 0,
            "zoom": 0,
            "twelveHourFormat": false,
            "coordinateFormat": "",
            "disabled": (this.disabled ? this.disabled : false),
            "expirationTime": (this.expiration_date ? this.expiration_date : null),
            "deviceLimit": (this.device_limit ? this.device_limit : 0),
            "userLimit": (this.user_limit ? this.user_limit : 0),
            "deviceReadonly": (this.device_readonly ? this.device_readonly : false),
            "limitCommands": (this.limit_commands ? this.limit_commands : false),
            "poiLayer": "",
            "token": ""
        };
        this.apiCall.postService123(url, this.userToken, payload).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            console.log("check if user is created or not? ", res);
            _this.toastCtrl.create({
                message: 'User has been created successfully.',
                position: 'bottom',
                duration: 3000
            }).then(function (toastEl) {
                toastEl.present();
                setTimeout(function () {
                    _this.navCtrl.pop();
                }, 3000);
            });
        }, function (err) {
            console.log(err);
        });
    };
    AddUserPage.ctorParameters = function () { return [
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] }
    ]; };
    AddUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-user',
            template: __webpack_require__(/*! raw-loader!./add-user.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/add-user/add-user.page.html"),
            styles: [__webpack_require__(/*! ./add-user.page.scss */ "./src/app/tabs/dashboard/settings/users/add-user/add-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])
    ], AddUserPage);
    return AddUserPage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-settings-users-add-user-add-user-module-es5.js.map