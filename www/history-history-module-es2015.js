(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/maintabs/tabs/vehicle-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{paramData.name}}</ion-title>\n    <ion-buttons slot=\"primary\" *ngIf=\"hideplayback\">\n      <ion-icon color=\"dark\" name=\"rewind\" style=\"font-size:19px;margin-top:11px;margin-right: 20px\" (click)=\"changeSpeed('low')\">\n      </ion-icon>\n      <ion-icon color=\"dark\" name=\"arrow-dropright-circle\" style=\"font-size:24px;margin-top:10px;margin-right: 15px\"\n        class=\"play\" *ngIf=\"(allData.flag2 == 'init') || (allData.flag2 == 'start') \" (click)=\"play2()\"></ion-icon>\n      <ion-icon color=\"dark\" name=\"pause\" style=\"font-size:24px;margin-top:10px;margin-right: 15px\" class=\"pause\"\n        *ngIf=\"allData.flag2 == 'stop'\" (click)=\"play2()\"></ion-icon>\n      <ion-icon color=\"dark\" name=\"fastforward\" style=\"font-size:19px;margin-top:11px;margin-right: 20px\"\n        (click)=\"changeSpeed('high')\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-row>\n    <ion-col size=\"5\" no-padding>\n      <ion-row>\n        <ion-col size=\"3\" style=\"padding: 5px 0px 5px 0px;\">\n          <ion-icon style=\"font-size: 2.3em; margin: 0px; padding: 0px;\" name=\"clock\" color=\"medium\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"9\" no-padding>\n          <ion-row>\n            <ion-col>\n              <ion-label style=\"font-size: 0.9em\">From Date</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-datetime displayFormat=\"DD-MM-YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\"\n                [(ngModel)]=\"datetimeStart\" style=\"padding: 0px;font-size:9px;color: #2ec95c;\">\n              </ion-datetime>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n    <ion-col size=\"5\" no-padding>\n      <ion-row>\n        <ion-col size=\"3\" style=\"padding: 5px 0px 5px 0px;\">\n          <ion-icon style=\"font-size: 2.3em; margin: 0px; padding: 0px;\" name=\"clock\" color=\"medium\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"9\" no-padding>\n          <ion-row>\n            <ion-col>\n              <ion-label style=\"font-size: 0.9em\">To Date</ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-datetime displayFormat=\"DD-MM-YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [(ngModel)]=\"datetimeEnd\"\n                style=\"padding: 0px;font-size:9px;color: #dc0f0f;\">\n              </ion-datetime>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n    <ion-col size=\"2\" no-padding>\n      <div style=\"margin-top: 9px; float: right\">\n        <ion-icon color=\"medium\" ios=\"ios-search\" md=\"md-search\" style=\"font-size:2.3em;\" (click)=\"onClick();\">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n\n</ion-header>\n\n<ion-content>\n  <div #map id=\"map_canvas\" style=\"height:100%;\"></div>\n</ion-content>\n<ion-footer *ngIf=\"(allData.flag2 == 'init') || (allData.flag2 == 'start')\">\n  <ion-row style=\"background-color: #dfdfdf; padding: 0px !important;\">\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px; text-align:center;\">\n        <ion-icon src=\"assets/imgs/163-1638744_vector-roads-milestone-vector-library-library-milestone-icon.svg\" style=\"color:#209c43;font-size:15px;\"></ion-icon>&nbsp;\n        <span>{{ summaryData ? meterIntoKelometer(summaryData.distance) : 0 }} kms</span>\n      </p>\n    </ion-col>\n\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px;text-align:center;\">\n        <ion-icon name=\"speedometer\" style=\"color:#cd4343\"></ion-icon>&nbsp;\n        <span>{{ summaryData ? (summaryData.averageSpeed  | number: \"1.0-2\") : 0 }} km/h</span>\n      </p>\n    </ion-col>\n\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px;text-align:center;\">\n        <ion-icon name=\"time\" style=\"color:#4968be\"></ion-icon>&nbsp;\n        <span>{{ summaryData ? parseMillisecondsIntoReadableTime(summaryData.engineHours) : '0h 0min' }}</span>\n      </p>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<ion-footer *ngIf=\"allData.flag2 == 'stop'\">\n  <ion-row style=\"background-color: #dfdfdf; padding: 0px !important;\">\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px; text-align:center;\">\n        <ion-icon name=\"time\" style=\"color:#209c43;font-size:15px;\"></ion-icon>&nbsp;\n        <span *ngIf=\"updatetimedate\">{{ updatetimedate }}&nbsp;</span>\n        <span *ngIf=\"!updatetimedate\">0:0&nbsp;</span>\n      </p>\n    </ion-col>\n\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px;text-align:center;\">\n        <ion-icon name=\"speedometer\" style=\"color:#cd4343\"></ion-icon>&nbsp;\n        <span *ngIf=\"speedMarker\">{{ speedMarker }} km/h</span>\n        <span *ngIf=\"!speedMarker\">0 km/h</span>\n      </p>\n    </ion-col>\n\n    <ion-col size=\"4\" style=\"padding: 0px\">\n      <p style=\"color:black;font-size:14px;text-align:center;\">\n        <ion-icon src=\"assets/imgs/163-1638744_vector-roads-milestone-vector-library-library-milestone-icon.svg\" style=\"color:#4968be\"></ion-icon>&nbsp;\n        <span>{{ cumu_distance ? (cumu_distance | number: \"1.0-2\") : 0 }} kms</span>\n      </p>\n    </ion-col>\n  </ion-row>\n</ion-footer>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.module.ts ***!
  \***********************************************************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history.page */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.ts");







const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]
    }
];
let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]],
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map_canvas {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9zZWNvbmQtdGFicy9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic3JjL2FwcC90YWJzL2Rhc2hib2FyZC92ZWhpY2xlLWxpc3Qvc2Vjb25kLXRhYnMvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9zZWNvbmQtdGFicy9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI21hcF9jYW52YXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iLCIjbWFwX2NhbnZhcyB7XG4gIGhlaWdodDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.ts ***!
  \*********************************************************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps/ngx */ "./node_modules/@ionic-native/google-maps/ngx/index.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);









let HistoryPage = class HistoryPage {
    constructor(route, googleMaps, appService, elementRef, ionicStorage, plt, ngZone, toastController, loadingCtrl, events, alertCtrl, datePipe) {
        this.route = route;
        this.googleMaps = googleMaps;
        this.appService = appService;
        this.elementRef = elementRef;
        this.ionicStorage = ionicStorage;
        this.plt = plt;
        this.ngZone = ngZone;
        this.toastController = toastController;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.datePipe = datePipe;
        this.hideplayback = false;
        this.allData = {};
        this.latlongObjArr = [];
        this.cumu_distance = 0;
        this.hideplayback = false;
        this.target = 0;
        this.datetimeStart = moment__WEBPACK_IMPORTED_MODULE_8__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = moment__WEBPACK_IMPORTED_MODULE_8__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
        if (this.route.snapshot.data['hist']) {
            this.paramData = this.route.snapshot.data['hist'];
            // console.log("parammap history: ", this.paramData)
        }
    }
    ngOnInit() {
        // console.log("parammap history: ", this.paramData)
    }
    ngOnDestroy() {
        localStorage.removeItem("markerTarget");
    }
    ionViewWillEnter() {
        localStorage.removeItem("markerTarget");
        this.plt.ready();
        // this.ngZone.run(() => this.initMap());
    }
    ionViewDidEnter() {
        // localStorage.removeItem("markerTarget");
        // this.plt.ready();
        this.ngZone.run(() => this.initMap());
        this.allData.flag2 = 'init';
        this.ionicStorage.get('token').then((val) => {
            console.log('Your age is', val);
            console.log("parammap history inside ionview: ", this.paramData);
            this.userdetails = val;
            this.trackerId = this.paramData.id;
            this.trackerType = (this.paramData.category ? this.paramData.category : 'car');
            this.DeviceId = this.paramData.uniqueId;
            this.trackerName = this.paramData.name;
            this.maphistory();
        });
    }
    initMap() {
        if (this.map === undefined) {
            this.map = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"].create(this.element.nativeElement);
            this.map.one(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_READY).then((data) => { });
        }
    }
    parseMillisecondsIntoReadableTime(milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        var h = absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        if (h === 0) {
            return m + 'min';
        }
        else {
            if (m === '00') {
                return '0min';
            }
            else {
                return h + 'h ' + m + 'min';
            }
        }
    }
    onClick() {
        if (this.map) {
            this.map.clear();
        }
        this.initMap();
        this.maphistory();
    }
    maphistory() {
        this.data2 = [];
        if (new Date(this.datetimeEnd).toISOString() >= new Date(this.datetimeStart).toISOString()) {
        }
        else {
            this.toastController.create({
                message: 'To time always greater than From Time',
                duration: 1500,
                position: 'bottom'
            }).then((toastEl => {
                toastEl.present();
            }));
            return false;
        }
        var burl = '/api/positions?deviceId=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
        this.loadingCtrl.create({
            keyboardClose: true, message: 'Fetching history data..'
        }).then(loadingEl => {
            loadingEl.present();
            this.appService.getService123(burl, this.userdetails)
                .subscribe(data => {
                loadingEl.dismiss();
                var respData = JSON.parse(JSON.stringify(data));
                if (respData.length === 0) {
                    this.alertCtrl.create({
                        header: 'No Data Found',
                        message: 'Vehicle has not moved from ' + this.datePipe.transform(new Date(this.datetimeStart), 'medium') + ' to ' + this.datePipe.transform(new Date(this.datetimeEnd), 'medium'),
                        buttons: [{
                                text: 'Okay',
                                handler: () => {
                                    this.hideplayback = false;
                                }
                            }]
                    }).then((alertEl) => {
                        alertEl.present();
                    });
                    return;
                }
                this.getSummaryData();
                this.data2 = respData;
                this.hideplayback = true;
                this.gps(this.data2);
            }, err => {
                loadingEl.dismiss();
            });
        });
    }
    getSummaryData() {
        let _burl = '/api/reports/summary?deviceId=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
        this.appService.getService123(_burl, this.userdetails)
            .subscribe(data => {
            let res = JSON.parse(JSON.stringify(data));
            this.summaryData = res[0];
        }, err => {
            console.log(err);
        });
    }
    changeSpeed(key) {
        console.log(key);
        let that = this;
        if (key === 'high') {
            that.speed = 3 * 100;
        }
        else if (key === 'low') {
            that.speed = 1 * 100;
        }
    }
    meterIntoKelometer(value) {
        var km = value / 1000;
        return km.toFixed(1);
    }
    gps(data3) {
        let that = this;
        that.latlongObjArr = data3;
        that.dataArrayCoords = [];
        var cumulativeDistance = 0;
        for (var i = 0; i < that.latlongObjArr.length; i++) {
            if (that.latlongObjArr[i].latitude && that.latlongObjArr[i].longitude) {
                var arr = [];
                var startdatetime = new Date(that.latlongObjArr[i].deviceTime);
                arr.push(that.latlongObjArr[i].latitude);
                arr.push(that.latlongObjArr[i].longitude);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": that.latlongObjArr[i].speed });
                // debugger
                cumulativeDistance = cumulativeDistance + Number(that.meterIntoKelometer(that.latlongObjArr[i].attributes.distance));
                // if (that.latlongObjArr[i].attributes) {
                //   if (i === 0) {
                //     cumulativeDistance += 0;
                //   } else {
                //     cumulativeDistance += that.latlongObjArr[i].attributes.distance ? parseFloat(that.latlongObjArr[i].attributes.distance) : 0;
                //   }
                //   that.latlongObjArr[i].attributes.distance = (cumulativeDistance);
                //   arr.push({ "cumu_dist": that.latlongObjArr[i].attributes.distance });
                // }
                // else {
                that.latlongObjArr[i].attributes.distance = cumulativeDistance;
                arr.push({ "cumu_dist": that.latlongObjArr[i].attributes.distance });
                // }
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data3.map(function (d) {
            return { lat: d.latitude, lng: d.longitude };
        });
        that.mapData.reverse();
        let bounds = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLngBounds"](that.mapData);
        that.map.moveCamera({
            target: bounds
        });
        this.map.on(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_CLICK).subscribe((data) => {
            console.log('Click MAP');
            // that.drawerHidden1 = true;
        });
        that.map.addMarker({
            // title: 'D',
            position: that.mapData[0],
            icon: {
                url: './assets/imgs/redFlag.png',
                size: {
                    height: 40,
                    width: 40
                }
            },
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then((marker) => {
            marker.showInfoWindow();
            that.map.addMarker({
                // title: 'S',
                position: that.mapData[that.mapData.length - 1],
                icon: {
                    url: './assets/imgs/greenFlag.png',
                    size: {
                        height: 40,
                        width: 40
                    }
                },
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then((marker) => {
                marker.showInfoWindow();
            });
        });
        that.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
    }
    Playback() {
        let that = this;
        // that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playing = !that.playing; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playing) {
            that.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                var icicon;
                if (that.plt.is('ios')) {
                    icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                else if (that.plt.is('android')) {
                    icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                that.map.addMarker({
                    icon: {
                        url: icicon,
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](that.startPos[0], that.startPos[1]),
                }).then((marker) => {
                    that.allData.mark = marker;
                    that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](that.startPos[0], that.startPos[1]));
        }
    }
    play2() {
        let that = this;
        that.allData.speed = 50;
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        if (that.allData.flag2 == 'init') {
            if (that.allData.mark == undefined) {
                var icicon;
                if (that.plt.is('ios')) {
                    icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                else if (that.plt.is('android')) {
                    icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                that.map.addMarker({
                    icon: {
                        url: icicon,
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng),
                }).then((marker) => {
                    that.allData.mark = marker;
                    that.animateMarker2(that.map, that.allData.mark, null, that.dataArrayCoords);
                    that.allData.flag2 = 'stop';
                });
            }
            else {
                that.allData.mark.setPosition({ lat: lat, lng: lng });
                that.map.setCameraTarget({ lat: lat, lng: lng });
                that.animateMarker2(that.map, that.allData.mark, null, that.dataArrayCoords);
                that.allData.flag2 = 'stop';
            }
        }
        else if (that.allData.flag2 == 'start') {
            that._moveMarker2();
            that.allData.flag2 = 'stop';
        }
        else if (that.allData.flag2 == 'stop') {
            //  dmap.speed = 0;
            clearTimeout(that.allData.start2);
            that.allData.flag2 = 'start';
        }
        if (that.allData.flag2 == 'reset') {
            console.log("flag2 is: ", that.allData.flag2);
            console.log("check reset coords: " + that.dataArrayCoords[0][0]);
            that.allData.mark.setPosition({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
            that.map.setCameraTarget({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
            // that.seekBarValue = 0;
            clearTimeout(that.allData.start2);
            that.allData.flag2 = 'init';
        }
        return that.allData.flag2;
    }
    ;
    animateMarker2(map, mark, icons, coords) {
        let that = this;
        that.cumu_distance = 0;
        that.allData.speed = 50;
        that.allData.delay = 10;
        if (that.allData.start2)
            clearTimeout(that.allData.start2);
        var target = 0;
        that._goToPoint2 = function () {
            if (that.speed) {
                that.allData.speed = that.speed;
            }
            ///////////////////////////////////////////////
            // if (that.rangeDetector === true) {
            //   a = that.indexValue;
            //   target = that.indexValue;
            //   that.sliderValue = that.indexValue;
            // }
            ///////////////////////////////////////////////
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (that.allData.speed * 1000 * that.allData.delay) / 3600000; // in meters
            if (coords[target] === undefined) {
                if (that.allData.start2)
                    clearTimeout(that.allData.start2);
                that.allData.flag2 = 'init';
                // that.sliderValue = 0;
                return;
            }
            var dest = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](coords[target][0], coords[target][1]);
            var distance = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                // console.log("check marker: ", mark)
                if (Number.isNaN(parseInt(deg))) {
                    // console.log("check degree: " + parseInt(deg))
                }
                else {
                    // console.log("check not: " + parseInt(deg))
                    if (mark) {
                        mark.setRotation(deg);
                    }
                }
            }
            that._moveMarker2 = function () {
                var head;
                // that.sliderValue = a;
                that.cumu_distance = coords[target][4].cumu_dist;
                console.log("cumulative distance: ", that.cumu_distance);
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    map.setCameraTarget({ lat: lat, lng: lng });
                    // that.getAddress(lat, lng);
                    that.allData.start2 = setTimeout(that._moveMarker2, that.allData.delay);
                }
                else {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    // that.getAddress(dest.lat, dest.lng);
                    target++;
                    if (target == coords.length) {
                        that.allData.flag2 = 'reset';
                        clearTimeout(that.allData.start2);
                    }
                    that.allData.start2 = setTimeout(that._goToPoint2, that.allData.delay);
                }
            };
            a++;
            // that.rangeDetector = false;
            console.log("aaaaaaaaaaaaaaaaa " + a);
            console.log("coords length " + coords.length);
            if (a > coords.length) {
                console.log("inside this aaaaaaaaaaaaaaaaa " + a);
            }
            else {
                console.log("coords target: ", coords[target]);
                that.speedMarker = (coords[target][3].speed).toFixed(2);
                that.updatetimedate = coords[target][2].time;
                // that.cumu_distance = coords[target][5].cumu_dist;
                // that.battery = coords[target][6].battery;
                console.log('move marker running');
                that._moveMarker2();
            }
        };
        var a = 0;
        that._goToPoint2();
    }
    liveTrack(map, mark, coords, target, startPos, speed, delay) {
        let that = this;
        that.events.subscribe("SpeedValue:Updated", (sdata) => {
            speed = sdata;
        });
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](coords[target][0], coords[target][1]);
            var distance = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    map.setCameraTarget(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["LatLng"](lat, lng));
                    setTimeout(_moveMarker, delay);
                }
                else {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["Spherical"].computeHeading(mark.getPosition(), dest);
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = (coords[target][3].speed).toFixed(2);
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    }
};
HistoryPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"] },
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Events"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', {
        static: true
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], HistoryPage.prototype, "element", void 0);
HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history',
        template: __webpack_require__(/*! raw-loader!./history.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.html"),
        styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/history/history.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"],
        src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Events"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
        _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]])
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=history-history-module-es2015.js.map