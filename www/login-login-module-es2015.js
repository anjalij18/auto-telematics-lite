(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html":
/*!*****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <div class=\"warning\">\n    <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" style=\"margin-top: 20%;\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\" class=\"ion-text-center\">\n            <ion-img src=\"assets/icon.png\" style=\"width: 120px; margin: auto\">\n            </ion-img>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-list style=\"background-color: transparent;\">\n              <ion-item style=\"color: #fff;\">\n                <ion-label position=\"floating\">E-Mail / Mobile Number</ion-label>\n                <ion-input type=\"text\" ngModel name=\"email\" required #emailCtrl=\"ngModel\"></ion-input>\n              </ion-item>\n              <ion-item style=\"color: #fff;\">\n                <ion-label position=\"floating\">Password</ion-label>\n                <ion-input type=\"password\" ngModel name=\"password\" required #passwordCtrl=\"ngModel\">\n                </ion-input>\n              </ion-item>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <!-- <ion-button color=\"tertiary\" type=\"button\" fill=\"clear\" expand=\"block\" (click)=\"onSwitchAuthMode()\">Switch To\n              {{ isLogin ? \"Signup\" : \"Login\" }}</ion-button> -->\n            <ion-button color=\"tertiary\" type=\"submit\" expand=\"block\" [disabled]=\"!f.valid\">Login\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".warning {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background: rgba(0, 0, 0, 0.65);\n  color: #fff;\n}\n\nion-content {\n  --background: #fff\n    url('tendencias-en-innovacion-tecnologica-y-proptechs-inmobiliarias-533x337xsmart-cities-opt.jpg.pagespeed.ic_.zsvuwxwqfv-2018-03-02_12-58-50_266848.png')\n    no-repeat center center / cover;\n}\n\nion-item {\n  --ion-background-color: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksa0JBQUE7RUFFQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLCtCQUFBO0VBQ0EsV0FBQTtBQ0RKOztBRElBO0VBRUU7O21DQUFBO0FDQUY7O0FES0E7RUFDRSxtQ0FBQTtBQ0ZGIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4ud2FybmluZyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8vIHBhZGRpbmc6IDIwJSAwO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuNjUpO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIC8vIGJhY2tkcm9wLWZpbHRlcjogY29udHJhc3QoNCkgYmx1cigxMHB4KTtcbn1cbmlvbi1jb250ZW50IHtcblxuICAtLWJhY2tncm91bmQ6ICNmZmZcbiAgICB1cmwoXCIuLi8uLi9hc3NldHMvdGVuZGVuY2lhcy1lbi1pbm5vdmFjaW9uLXRlY25vbG9naWNhLXktcHJvcHRlY2hzLWlubW9iaWxpYXJpYXMtNTMzeDMzN3hzbWFydC1jaXRpZXMtb3B0LmpwZy5wYWdlc3BlZWQuaWNfLnpzdnV3eHdxZnYtMjAxOC0wMy0wMl8xMi01OC01MF8yNjY4NDgucG5nXCIpXG4gICAgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbiIsIi53YXJuaW5nIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNjUpO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNmZmZcbiAgICB1cmwoXCIuLi8uLi9hc3NldHMvdGVuZGVuY2lhcy1lbi1pbm5vdmFjaW9uLXRlY25vbG9naWNhLXktcHJvcHRlY2hzLWlubW9iaWxpYXJpYXMtNTMzeDMzN3hzbWFydC1jaXRpZXMtb3B0LmpwZy5wYWdlc3BlZWQuaWNfLnpzdnV3eHdxZnYtMjAxOC0wMy0wMl8xMi01OC01MF8yNjY4NDgucG5nXCIpXG4gICAgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login.service */ "./src/app/login/login.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");








// import { AuthService } from '../services/auth.service';
let LoginPage = class LoginPage {
    constructor(router, loadingCtrl, authService, authenticateService, loginServ, ionicStorage, toastController) {
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.authenticateService = authenticateService;
        this.loginServ = loginServ;
        this.ionicStorage = ionicStorage;
        this.toastController = toastController;
        this.isLogin = true;
        this.isLoading = false;
        this.fdata = "ngModel";
        this.credentials = {};
        localStorage.removeItem("password");
    }
    ngOnInit() { }
    onSwitchAuthMode() {
        this.isLogin = !this.isLogin;
    }
    onSubmit(form) {
        this.credentials = {};
        if (!form.valid) {
            return;
        }
        const email = form.value.email;
        const pass = form.value.password;
        this.credentials = {
            email: email,
            password: pass
        };
        if (this.isLogin) {
            this.loadingCtrl
                .create({
                keyboardClose: true,
                message: "Loging in..."
            })
                .then(loadingEl => {
                loadingEl.present();
                this.authService.authenticateUser(this.credentials).subscribe(response => {
                    loadingEl.dismiss();
                    form.reset();
                    let res = JSON.parse(JSON.stringify(response));
                    if (res.disabled === false) {
                        this.authenticateService.login();
                        localStorage.setItem("password", pass);
                        this.ionicStorage.set("token", res).then(() => {
                            this.loginServ.login();
                            this.router.navigateByUrl("/maintabs/tabs/dashboard");
                        });
                    }
                }, err => {
                    loadingEl.dismiss();
                    this.toastController
                        .create({
                        message: "Login Falied. Please use valid credentials...",
                        duration: 1500,
                        position: "bottom"
                    })
                        .then(toastEl => {
                        toastEl.present();
                    });
                });
            });
        }
        else {
            //send request to signup servers
        }
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
    { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"] },
    { type: _login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-login",
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
        _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"],
        _login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
], LoginPage);



/***/ }),

/***/ "./src/app/login/login.service.ts":
/*!****************************************!*\
  !*** ./src/app/login/login.service.ts ***!
  \****************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let LoginService = class LoginService {
    constructor(appService, loadingCtrl, router) {
        this.appService = appService;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this._userIsAuthenticated = false;
        this.isLoading = false;
    }
    get userIsAuthenticated() {
        return this._userIsAuthenticated;
    }
    login() {
        // this.isLoading = true;
        // this.loadingCtrl.create({
        //   keyboardClose: true, message: 'Loging in...'
        // }).then(loadingEl => {
        //   loadingEl.present();
        //   this.appService.authenticateUser(payLoad).subscribe(() => {
        //     this.isLoading = false;
        //     loadingEl.dismiss();
        this._userIsAuthenticated = true;
        //     this.router.navigateByUrl('/maintabs/tabs/dashboard');
        //   })
        // });
    }
    logout() {
        this._userIsAuthenticated = false;
    }
};
LoginService.ctorParameters = () => [
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], LoginService);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map