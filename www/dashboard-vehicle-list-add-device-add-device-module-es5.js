(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-vehicle-list-add-device-add-device-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/vehicle-list\"></ion-back-button>\n    <ion-title>Add Device</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-list>\n    <ion-item>\n      <ion-label position=\"floating\">Name</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"name\"></ion-input>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\" lines=\"none\" *ngIf=\"attempToSubmit && (name === undefined)\">\n      <p style=\"font-size: 0.85em; color: red; margin: 0px;\">Please enter device name is required.</p>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Identifier</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"imei\"></ion-input>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\" lines=\"none\" *ngIf=\"attempToSubmit && (imei === undefined)\">\n      <p style=\"font-size: 0.85em; color: red; margin: 0px;\">Please enter identifier is required.</p>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Sim Number</ion-label>\n      <ion-input type=\"number\" [(ngModel)]=\"sim_num\"></ion-input>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\" lines=\"none\" *ngIf=\"attempToSubmit && (sim_num === undefined)\">\n      <p style=\"font-size: 0.85em; color: red; margin: 0px;\">Please enter device sim number required.</p>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Model</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"model\"></ion-input>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\" lines=\"none\" *ngIf=\"attempToSubmit && (model === undefined)\">\n      <p style=\"font-size: 0.85em; color: red; margin: 0px;\">Please enter device model required.</p>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Category</ion-label>\n      <ion-select placeholder=\"Select One\" [(ngModel)]=\"category\">\n        <ion-select-option *ngFor=\"let item of categories\" [value]=\"item.name\">{{item.name}}</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\" lines=\"none\" *ngIf=\"attempToSubmit && (category === undefined)\">\n      <p style=\"font-size: 0.85em; color: red; margin: 0px;\">Please choose device category.</p>\n    </ion-item>\n\n    <ion-button expand=\"block\" color=\"tertiary\" (click)=\"addDevice()\">Add Device</ion-button>\n\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-device/add-device.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-device/add-device.module.ts ***!
  \*****************************************************************************/
/*! exports provided: AddDevicePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDevicePageModule", function() { return AddDevicePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_device_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-device.page */ "./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.ts");







var routes = [
    {
        path: '',
        component: _add_device_page__WEBPACK_IMPORTED_MODULE_6__["AddDevicePage"]
    }
];
var AddDevicePageModule = /** @class */ (function () {
    function AddDevicePageModule() {
    }
    AddDevicePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_device_page__WEBPACK_IMPORTED_MODULE_6__["AddDevicePage"]]
        })
    ], AddDevicePageModule);
    return AddDevicePageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9hZGQtZGV2aWNlL2FkZC1kZXZpY2UucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.ts ***!
  \***************************************************************************/
/*! exports provided: AddDevicePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDevicePage", function() { return AddDevicePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var AddDevicePage = /** @class */ (function () {
    function AddDevicePage(apiCall, ionStorage, toastCtrl, navCtrl) {
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.categories = [
            {
                name: 'Arrow'
            },
            {
                name: 'Default'
            },
            {
                name: 'Animal'
            },
            {
                name: 'Bicycle'
            },
            {
                name: 'Boat'
            },
            {
                name: 'Bus'
            },
            {
                name: 'Car'
            },
            {
                name: 'Crane'
            },
            {
                name: 'Helicopter'
            },
            {
                name: 'Motorcycle'
            },
            {
                name: 'Offroad'
            },
            {
                name: 'Person'
            },
            {
                name: 'Pickup'
            },
            {
                name: 'Plane'
            },
            {
                name: 'Ship'
            },
            {
                name: 'Tractor'
            },
            {
                name: 'Train'
            },
            {
                name: 'Tram'
            },
            {
                name: 'Truck'
            },
            {
                name: 'Trollybus'
            },
            {
                name: 'Van'
            },
            {
                name: 'Scooter'
            }
        ];
        this.attempToSubmit = false;
    }
    AddDevicePage.prototype.ngOnInit = function () {
    };
    AddDevicePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionStorage.get('token').then(function (val) {
            _this.userToken = val;
        });
    };
    AddDevicePage.prototype.addDevice = function () {
        var _this = this;
        if (this.name === undefined && this.sim_num === undefined && this.model === undefined && this.imei === undefined && this.category === undefined) {
            this.attempToSubmit = true;
            return;
        }
        var url = '/api/devices';
        var payload = {
            "id": 1,
            "name": this.name,
            "uniqueId": this.imei,
            "phone": this.sim_num,
            "model": this.model,
            "contact": "",
            "category": String(this.category).toLowerCase(),
            "status": null,
            "lastUpdate": null,
            "groupId": 0,
            "disabled": false
        };
        this.apiCall.postService123(url, this.userToken, payload).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res) {
                _this.toastCtrl.create({
                    message: 'Device has been added successfully!',
                    duration: 3000,
                    position: 'bottom'
                }).then(function (toastEl) {
                    toastEl.present();
                    setTimeout(function () {
                        _this.navCtrl.pop();
                    }, 3000);
                });
            }
        });
    };
    AddDevicePage.ctorParameters = function () { return [
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] }
    ]; };
    AddDevicePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-device',
            template: __webpack_require__(/*! raw-loader!./add-device.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.html"),
            styles: [__webpack_require__(/*! ./add-device.page.scss */ "./src/app/tabs/dashboard/vehicle-list/add-device/add-device.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])
    ], AddDevicePage);
    return AddDevicePage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-vehicle-list-add-device-add-device-module-es5.js.map