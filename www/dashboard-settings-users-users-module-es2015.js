(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-settings-users-users-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/users.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/settings/users/users.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-back-button slot=\"start\" defaultHref=\"/maintabs/tabs/settings\"></ion-back-button>\n    <ion-title>Users</ion-title>\n    <!-- <ion-icon *ngIf=\"(userToken ? userToken.administrator : false)\" slot=\"end\" name=\"add\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addUser()\"></ion-icon> -->\n    <!-- <div *ngIf=\"userToken\"> -->\n    <ion-icon *ngIf=\"(userToken ? (userToken.administrator === true || userToken.userLimit !== 0) : false)\" slot=\"end\" name=\"add\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addUser()\"></ion-icon>\n    <!-- </div> -->\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor=\"let item of userList\">\n      <ion-thumbnail slot=\"start\">\n        <img src=\"assets/Images/user-image-.png\">\n      </ion-thumbnail>\n      <ion-label class=\"ion-text-wrap\">\n        <ion-text color=\"tertiary\">\n          <h2>{{item.name | titlecase}}</h2>\n        </ion-text>\n        <p>Email - {{item.email}}</p>\n        <!-- <p>Admin - {{(item.administrator === 'true') ? 'Yes' : 'No'}}</p> -->\n        <p>Disabled - {{(item.disabled === 'true') ? 'Yes' : 'No'}}</p>\n      </ion-label>\n      <ion-badge slot=\"end\" *ngIf=\"item.administrator === true\" color=\"tertiary\">Admin</ion-badge>\n      <ion-chip slot=\"end\" (click)=\"addDevice(item)\" color=\"tertiary\">Add Device</ion-chip>\n    </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.module.ts ***!
  \***************************************************************/
/*! exports provided: UsersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageModule", function() { return UsersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _users_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users.page */ "./src/app/tabs/dashboard/settings/users/users.page.ts");







const routes = [
    {
        path: '',
        component: _users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]
    }
];
let UsersPageModule = class UsersPageModule {
};
UsersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]]
    })
], UsersPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3NldHRpbmdzL3VzZXJzL3VzZXJzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/tabs/dashboard/settings/users/users.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/tabs/dashboard/settings/users/users.page.ts ***!
  \*************************************************************/
/*! exports provided: UsersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPage", function() { return UsersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let UsersPage = class UsersPage {
    constructor(apiCall, ionStorage, route, alertController) {
        this.apiCall = apiCall;
        this.ionStorage = ionStorage;
        this.route = route;
        this.alertController = alertController;
        this.userList = [];
        this.devicesList = [];
        this.addedDevicesList = [];
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.ionStorage.get('token').then((token) => {
            debugger;
            this.userToken = token;
            this.getCustomer();
        });
    }
    getCustomer() {
        let url = '/api/users';
        this.apiCall.getService123(url, this.userToken).subscribe(resp => {
            let res = JSON.parse(JSON.stringify(resp));
            console.log("users: " + res);
            this.userList = res;
        }, err => {
            console.log(err);
        });
    }
    addUser() {
        this.route.navigateByUrl('/maintabs/tabs/settings/users/add-user');
    }
    addDevice(userData) {
        let url = '/api/devices';
        this.apiCall.getService123(url, this.userToken).subscribe((resp) => {
            this.devicesList = JSON.parse(JSON.stringify(resp));
            let _url = '/api/devices?userId=' + userData.id;
            this.apiCall.getService123(_url, this.userToken).subscribe((response) => {
                this.addedDevicesList = JSON.parse(JSON.stringify(response));
                let alertOptions = [];
                let alertOptions123 = [];
                for (var i = 0; i < this.devicesList.length; i++) {
                    alertOptions.push({
                        name: this.devicesList[i].name,
                        type: 'checkbox',
                        label: this.devicesList[i].name,
                        value: this.devicesList[i]
                    });
                }
                for (var j = 0; j < this.addedDevicesList.length; j++) {
                    alertOptions123.push({
                        name: this.addedDevicesList[j].name,
                        type: 'checkbox',
                        label: this.addedDevicesList[j].name,
                        value: this.addedDevicesList[j],
                        checked: true
                    });
                }
                const result = Object.values([].concat(alertOptions, alertOptions123)
                    .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {}));
                // debugger
                // console.log(result);
                this.alertController.create({
                    header: 'Select Device',
                    inputs: result,
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: () => {
                                console.log('Confirm Cancel');
                            }
                        }, {
                            text: 'Ok',
                            handler: (data) => {
                                console.log('Confirm Ok', data);
                                this.addPermissions(userData, data);
                            }
                        }
                    ]
                }).then((alertEl) => {
                    alertEl.present();
                });
            }, err => {
                console.log("got error: ", err);
            });
        }, err => {
            console.log("got error: ", err);
        });
    }
    addPermissions(userData, selectedData) {
        debugger;
        let url = '/api/permissions';
        let that = this;
        // outerthis.stoppageReport = [];
        var i = 0, howManyTimes = selectedData.length;
        function f() {
            let payload = {
                "userId": userData.id,
                "deviceId": selectedData[i].id
            };
            that.apiCall.postService123(url, that.userToken, payload).subscribe((resp) => {
                let res = JSON.parse(JSON.stringify(resp));
                console.log("check permissions: ", res);
            }, err => {
                console.log("got err: ", err);
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    }
};
UsersPage.ctorParameters = () => [
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
];
UsersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-users',
        template: __webpack_require__(/*! raw-loader!./users.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/settings/users/users.page.html"),
        styles: [__webpack_require__(/*! ./users.page.scss */ "./src/app/tabs/dashboard/settings/users/users.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]])
], UsersPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-settings-users-users-module-es2015.js.map