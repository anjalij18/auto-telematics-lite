(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["live-track-live-track-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/maintabs/tabs/vehicle-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{data.name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <!-- <ion-row>\n    <ion-col>\n      <p style=\"background: black; color: white; font-weight: 600;font-size: 1.3em;text-align: center;\">{{newVariable}}</p>\n    </ion-col>\n  </ion-row> -->\n  <div #map123 id=\"map_canvas123\">\n    <!-- fab placed to the top start -->\n    <ion-fab vertical=\"top\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button (click)=\"onMapType()\" size=\"small\" color=\"light\">\n        <ion-icon *ngIf=\"afterMapTypeClick\" name=\"logo-buffer\"></ion-icon>\n        <ion-icon *ngIf=\"!afterMapTypeClick\" src=\"assets/imgs/icon/stack-of-square-papers.svg\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n    <ion-fab horizontal=\"end\" slot=\"fixed\" style=\"margin-top: 14%;\">\n      <ion-fab-button (click)=\"onTraffic()\" size=\"small\" color=\"light\">\n        <ion-icon *ngIf=\"!afterClick\" src=\"assets/imgs/icon/traffic-light-before.svg\">\n        </ion-icon>\n        <ion-icon *ngIf=\"afterClick\" src=\"assets/imgs/icon/traffic-light.svg\">\n        </ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n    <!-- <ion-fab *ngIf=\"ignitionKey !== undefined\" horizontal=\"end\" slot=\"fixed\" style=\"margin-top: 28%;\">\n      <ion-fab-button (click)=\"onImmo123()\" size=\"small\" color=\"light\">\n        <ion-icon *ngIf=\"ignitionKey == false\" name=\"lock\" color=\"danger\">\n        </ion-icon>\n        <ion-icon *ngIf=\"ignitionKey == true\" name=\"unlock\" color=\"success\">\n        </ion-icon>\n      </ion-fab-button>\n    </ion-fab> -->\n    <ion-fab horizontal=\"end\" slot=\"fixed\" style=\"margin-top: 28%;\">\n      <ion-fab-button (click)=\"onImmo123()\" size=\"small\" color=\"light\">\n        <ion-icon name=\"lock\" color=\"tertiary\">\n        </ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n\n    <ion-fab horizontal=\"end\" slot=\"fixed\" style=\"margin-top: 41%;\">\n      <ion-fab-button (click)=\"addGeofence()\" size=\"small\" color=\"light\">\n        <ion-icon src=\"assets/imgs/locate.svg\" color=\"tertiary\">\n        </ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n\n    <ion-row>\n      <ion-col>\n        <p id=\"para\" style=\"text-align: center;font-size:18px;color: #633ce0;font-weight: 600;\">\n          <!-- {{newVariable}} -->\n          <span style=\"font-size:16px;\">km/h</span></p>\n        <!-- <p class=\"blink\" style=\"text-align: center;font-size:18px;color: green;font-weight: 600;\"\n          *ngIf=\"vehicle_speed <= 60\">\n          {{ vehicle_speed }}\n          <span style=\"font-size:16px;\">km/h</span>\n        </p>\n        <p class=\"blink\" style=\"text-align: center;font-size:18px;color: red;font-weight: 600;\"\n          *ngIf=\"vehicle_speed > 60\">\n          {{ vehicle_speed }}\n          <span style=\"font-size:16px;\">km/h</span>\n        </p> -->\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: LiveTrackPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiveTrackPageModule", function() { return LiveTrackPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _live_track_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./live-track.page */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.ts");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");





// import { GoogleMaps } from '@ionic-native/google-maps/ngx';



const routes = [
    {
        path: '',
        component: _live_track_page__WEBPACK_IMPORTED_MODULE_6__["LiveTrackPage"]
    }
];
let LiveTrackPageModule = class LiveTrackPageModule {
};
LiveTrackPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_live_track_page__WEBPACK_IMPORTED_MODULE_6__["LiveTrackPage"]],
        providers: [src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_7__["GeocoderAPIService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]]
    })
], LiveTrackPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map_canvas123 {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL3RhYnMvZGFzaGJvYXJkL3ZlaGljbGUtbGlzdC9zZWNvbmQtdGFicy9saXZlLXRyYWNrL2xpdmUtdHJhY2sucGFnZS5zY3NzIiwic3JjL2FwcC90YWJzL2Rhc2hib2FyZC92ZWhpY2xlLWxpc3Qvc2Vjb25kLXRhYnMvbGl2ZS10cmFjay9saXZlLXRyYWNrLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC90YWJzL2Rhc2hib2FyZC92ZWhpY2xlLWxpc3Qvc2Vjb25kLXRhYnMvbGl2ZS10cmFjay9saXZlLXRyYWNrLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtYXBfY2FudmFzMTIzIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLy8gaGVpZ2h0OiA5MCU7XG4gICAgLy8gd2lkdGg6IDE1MHB4O1xuICAgIC8vIGhlaWdodDogMTUwcHg7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmVkO1xuICB9IiwiI21hcF9jYW52YXMxMjMge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.ts ***!
  \***************************************************************************************/
/*! exports provided: LiveTrackPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiveTrackPage", function() { return LiveTrackPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/google-maps/ngx */ "./node_modules/@ionic-native/google-maps/ngx/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var src_app_app_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/app.model */ "./src/app/app.model.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/geocoder-api.service */ "./src/app/geocoder-api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");






// import { DrawerState } from 'ion-bottom-drawer';





let LiveTrackPage = class LiveTrackPage {
    constructor(route, toastController, 
    // private router: Router,
    googleMaps, appService, elementRef, ionicStorage, plt, constURL, 
    // private socket: Socket,
    loadingCtrl, ngZone, events, alertController, geocoderApi, datePipe
    // private http: HttpClient,
    ) {
        this.route = route;
        this.toastController = toastController;
        this.googleMaps = googleMaps;
        this.appService = appService;
        this.elementRef = elementRef;
        this.ionicStorage = ionicStorage;
        this.plt = plt;
        this.constURL = constURL;
        this.loadingCtrl = loadingCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.alertController = alertController;
        this.geocoderApi = geocoderApi;
        this.datePipe = datePipe;
        this.socketChnl = [];
        this.allData = {};
        this.deviceDeatils = {};
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.clicked = false;
        this.vehicle_speed = 0.00;
        this.positionsArray = [];
        this.count = 0;
        this.afterClick = false;
        this.afterMapTypeClick = false;
        this.vehData = [];
        this.geofences = [];
        this.tempArray = [];
        if (this.route.snapshot.data['special']) {
            this.data = this.route.snapshot.data['special'];
            // this.device_type = this.data.device_model.device_type;
            console.log("parammap: ", this.data);
        }
        this.server_ip = localStorage.getItem("server_ip");
    }
    ngOnInit() {
        this.allData = {};
        this.socketChnl = [];
        this.socketSwitch = {};
    }
    ngOnDestroy() {
    }
    ionViewWillEnter() {
        console.log("Hello Anjali, I'm here!!");
        this.plt.ready();
        this.ngZone.run(() => this.initMap());
    }
    ionViewWillLeave() {
        // this.socket.disconnect();
    }
    ionViewDidLeave() {
        let that = this;
        CordovaWebsocketPlugin.wsClose(that.webSocketId, 1000, "I'm done!");
    }
    ionViewDidEnter() {
        console.log("Hello Deepa, I'm here!!");
        localStorage.removeItem("SocketFirstTime");
        localStorage.setItem("CurrentDevice", JSON.stringify(this.data));
        // localStorage.setItem("CurrentDevice", this.data);
        // let that = this;
        /////////////////////////////
        this.ionicStorage.get('token').then((val) => {
            console.log('Your age is', val);
            this.userdetails = val;
            this.live(this.data);
            let url = '/api/session';
            this.loadingCtrl.create({
                message: 'Loading...',
                spinner: 'lines'
            }).then((loadEl) => {
                loadEl.present();
                this.appService.getServiceTemp123(url, val).subscribe(
                // this.appService.getService123(url, val).subscribe(
                response => {
                    let res = JSON.parse(JSON.stringify(response));
                    this.ionicStorage.set("token", res).then(() => {
                        this.ionicStorage.get('token').then((val123) => {
                            console.log('Your age is', val123);
                            this.userdetails = val123;
                            /////////////////////////////
                            this.sockerConnection(loadEl);
                            ///////////////////
                        });
                    });
                }, err => {
                    loadEl.dismiss();
                    console.log(err);
                });
            });
        });
    }
    sockerConnection(loadEl) {
        let that = this;
        this.appService.getSessionwithToken(this.userdetails.token).subscribe((resp) => {
            console.log("intercept works or not ....", resp);
            cookieMaster.getCookieValue(that.server_ip + "/api/session", 'JSESSIONID', function (data) {
                // console.log("we got cookie... " + data.cookieValue);
                let options = {
                    url: 'ws://128.199.21.17:8082/api/socket',
                    pingInterval: 3000,
                    headers: { "Cookie": "JSESSIONID=" + data.cookieValue },
                };
                loadEl.dismiss();
                CordovaWebsocketPlugin.wsConnect(options, function (recvEvent) {
                    // console.log("Received callback from WebSocket: " + JSON.parse(JSON.stringify(recvEvent.message)));
                    if (JSON.parse(recvEvent.message).positions) {
                        var a = JSON.parse(recvEvent.message).positions;
                        that.positionsArray = a;
                        that.furtherCalc(that.positionsArray);
                    }
                    if (JSON.parse(recvEvent.message).devices) {
                        var b = JSON.parse(recvEvent.message).devices;
                        that.furtherCalc123(b);
                    }
                }, function (success) {
                    console.log("Connected to WebSocket with id: " + success.webSocketId);
                    that.webSocketId = success.webSocketId;
                }, function (error) {
                    console.log("Failed to connect to WebSocket: " +
                        "code: " + error["code"] +
                        ", reason: " + error["reason"] +
                        ", exception: " + error["exception"]);
                });
                // var url = 'ws://128.199.21.17:8082/api/socket';
                // var connection = new WebSocket(url, []);
                // // , { 'headers': { 'Cookie': resp.headers['set-cookie'][0] } }
                // connection.onopen = () => {
                //   connection.send('Message From Client')
                // }
                // connection.onerror = (error) => {
                //   console.log('WebSocket error: ${error}', error)
                // }
                // connection.onmessage = (e) => {
                //   console.log("socket message:" + e.data)
                // }
            });
        }, err => {
            loadEl.dismiss();
            console.log("intercept error ", err);
        });
    }
    furtherCalc123(b) {
        let that = this;
        var i = 0, howManyTimes = b.length;
        function f() {
            if (b[i].id === that.data.id) {
                that.data = b[i];
            }
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    }
    furtherCalc(a) {
        let that = this;
        console.log("inside function");
        for (var t = 0; t < a.length; t++) {
            if (a[t].deviceId === that.data.id) {
                console.log("dfsgdfgfhgjhkjkjkljkjkjkj");
                that.newVariable = (a[t].speed ? Number(a[t].speed) : 0);
                var something = document.getElementById("para");
                something.innerText = (that.newVariable * 1.943844).toFixed(2) + ' km/h';
                console.log("socket positon speed data: " + that.newVariable);
                that.socketInit(a[t]);
            }
        }
    }
    initMap() {
        let style = [];
        let mapOptions = {
            backgroundColor: 'white',
            controls: {
                compass: false,
                zoom: true,
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            styles: style
        };
        if (this.map123 === undefined) {
            console.log(this.element.nativeElement);
            this.map123 = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMaps"].create(this.element.nativeElement, mapOptions);
            this.map123.one(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsEvent"].MAP_READY).then((data) => {
            });
        }
    }
    onTraffic() {
        // debugger
        this.afterClick = !this.afterClick;
        if (this.afterClick) {
            this.map123.setTrafficEnabled(true);
        }
        else {
            this.map123.setTrafficEnabled(false);
        }
    }
    onMapType() {
        // debugger
        this.afterMapTypeClick = !this.afterMapTypeClick;
        if (this.afterMapTypeClick) {
            this.map123.setMapTypeId(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsMapTypeId"].HYBRID);
        }
        else {
            this.map123.setMapTypeId(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsMapTypeId"].NORMAL);
        }
    }
    showSettings() {
    }
    // ignitionKey: any;
    live(data) {
        //////////////////////// get data from traccar api /////////////////////////
        let _bUrl = '/api/positions?deviceId=' + data.id;
        this.appService.getService123(_bUrl, this.userdetails).subscribe((response => {
            let res = JSON.parse(JSON.stringify(response));
            console.log("check data response: ", res);
            this.vehData = res;
            // if (this.vehData[0].attributes.ignition !== undefined) {
            //   this.ignitionKey = this.vehData[0].attributes.ignition;
            // }
            console.log("check category: ", this.data.category);
            this.innerFunc(res, data);
        }));
        //////////////////////// get data from traccar api /////////////////////////
    }
    innerFunc(data1, data) {
        let res = data1[0];
        let that = this;
        if (res) {
            if (res.latitude && res.longitude) {
                if (that.plt.is('android')) {
                    that.map123.moveCamera({
                        target: { lat: res.latitude, lng: res.longitude },
                        zoom: 18,
                        duration: 2000,
                        padding: 0 // default = 20px
                    });
                    // let icicon;
                    // if (data.status === 'online') {
                    //   icicon = './assets/imgs/vehicles/runningcar.png';
                    // } else {
                    //   icicon = './assets/imgs/vehicles/outofreachcar.png';
                    // }
                    // console.log("selected icons: ", icicon);
                }
                else {
                }
            }
            else {
            }
        }
        this.getAddedGeofence();
    }
    onImmo() {
        let that = this;
        let d = that.data;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (that.clicked) {
                this.alertController.create({
                    message: "Process is already going on. Please wait till complete.",
                    buttons: ['Okay']
                }).then(alertEl => {
                    alertEl.present();
                });
            }
            else {
                this.messages = undefined;
                // this.dataEngine = d;
                // this.apiCall.startLoading().present();
                this.loadingCtrl.create({
                    message: 'Please wait...'
                }).then(loadEl => {
                    loadEl.present();
                    this.subFunc(d, loadEl);
                });
            }
        }
    }
    onImmo123() {
        let _bUrl = "/api/commands/send?deviceId=" + this.data.id;
        this.appService.getService123(_bUrl, this.userdetails).subscribe((response) => {
            let res = JSON.parse(JSON.stringify(response));
            console.log("check commands: ", res);
            this.commandTypes = res;
            this.presentAlertRadio();
        }, err => {
            console.log("got error: ", err);
        });
    }
    getAddedGeofence() {
        let that = this;
        let _url = '/api/geofences?deviceId=' + this.data.id;
        this.appService.getService123(_url, this.userdetails).subscribe((response) => {
            let addedGeofences = JSON.parse(JSON.stringify(response));
            if (addedGeofences.length > 0) {
                that.drawCircle(addedGeofences);
            }
        }, err => {
            console.log("error: ", err);
        });
    }
    drawCircle(a) {
        let that = this;
        var i = 0, howManyTimes = a.length;
        function f() {
            if (a[i].id) {
                let str = a[i].area;
                var res = str.split(/(?:,| )+/);
                let str1 = res[1];
                var res1 = str1.split("(");
                var str2 = res[3];
                var res2 = str2.split(")");
                let ltln = { "lat": res1[1], "lng": res[2] };
                let circle = that.map123.addCircleSync({
                    'center': ltln,
                    'radius': res2[0],
                    'strokeColor': '#7044ff',
                    'strokeWidth': 2,
                    'fillColor': 'rgb(112, 68, 255, 0.5)'
                });
            }
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    }
    addGeofence() {
        let _url = '/api/geofences?deviceId=' + this.data.id;
        this.appService.getService123(_url, this.userdetails).subscribe((response) => {
            let addedGeofences = JSON.parse(JSON.stringify(response));
            var _bUrl = '/api/geofences';
            this.appService.getService123(_bUrl, this.userdetails).subscribe(resp => {
                console.log("geofences: " + resp);
                this.geofences = JSON.parse(JSON.stringify(resp));
                // console.log(this.geofences);
                let alertOptions = [];
                let alertOptions123 = [];
                for (var i = 0; i < this.geofences.length; i++) {
                    alertOptions.push({
                        name: this.geofences[i].name,
                        type: 'checkbox',
                        label: this.geofences[i].name,
                        value: this.geofences[i]
                    });
                }
                for (var j = 0; j < addedGeofences.length; j++) {
                    alertOptions123.push({
                        name: addedGeofences[j].name,
                        type: 'checkbox',
                        label: addedGeofences[j].name,
                        value: addedGeofences[j],
                        checked: true
                    });
                }
                const result = Object.values([].concat(alertOptions, alertOptions123)
                    .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {}));
                this.alertController.create({
                    header: 'Select Geofence',
                    inputs: result,
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: () => {
                                console.log('Confirm Cancel');
                            }
                        }, {
                            text: 'Ok',
                            handler: (data) => {
                                console.log('Confirm Ok', data);
                                this.addPermissions(this.userdetails, data);
                            }
                        }
                    ]
                }).then((alertEl) => {
                    alertEl.present();
                });
            }, err => {
                console.log(err);
            });
        });
    }
    addPermissions(userData, selectedData) {
        let url = '/api/permissions';
        let that = this;
        // outerthis.stoppageReport = [];
        var i = 0, howManyTimes = selectedData.length;
        function f() {
            let payload = {
                "deviceId": that.data.id,
                "geofenceId": selectedData[i].id
            };
            that.appService.postService123(url, userData, payload).subscribe((resp) => {
                let res = JSON.parse(JSON.stringify(resp));
                console.log("check permissions: ", res);
            }, err => {
                console.log("got err: ", err);
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    }
    presentAlertRadio() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let radio_options = [];
            for (let i = 0; i < this.commandTypes.length; ++i) {
                radio_options.push({
                    type: 'radio',
                    label: this.commandTypes[i].description,
                    value: this.commandTypes[i],
                });
            }
            const alert = yield this.alertController.create({
                header: 'Choose command',
                inputs: radio_options,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Ok',
                        handler: (data) => {
                            console.log("check data: ", data);
                            console.log('Confirm Ok');
                            this.sendCommand(data);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    sendCommand(data) {
        // debugger
        data.deviceId = this.data.id;
        let url = "/api/commands/send";
        this.appService.postService123(url, this.userdetails, data).subscribe((response) => {
            let res = JSON.parse(JSON.stringify(response));
            console.log("check command res: ", res);
        }, err => {
            console.log("error: ", err);
        });
    }
    subFunc(d, loadEl) {
        let that = this;
        var baseURLp;
        if (d.device_model.device_type === undefined) {
            baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + this.device_type;
        }
        else {
            baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + d.device_model.device_type;
        }
        this.appService.getService(baseURLp)
            .subscribe(data => {
            loadEl.dismiss();
            // debugger;
            console.log("immo data: ", data);
            this.deviceConfigStatus = data;
            let immobType = data[0].imobliser_type;
            if (d.ignitionLock == '1') {
                this.messages = 'Do you want to unlock the engine?';
            }
            else {
                if (d.ignitionLock == '0') {
                    this.messages = 'Do you want to lock the engine?';
                }
            }
            this.alertController.create({
                message: this.messages,
                buttons: [{
                        text: 'YES',
                        handler: () => {
                            if (immobType == 0 || immobType == undefined) {
                                that.clicked = true;
                                var devicedetail = {
                                    "_id": d._id,
                                    "engine_status": !d.engine_status
                                };
                                const bUrl = this.constURL.mainUrl + 'devices/deviceupdate';
                                // this.apiCall.startLoading().present();
                                this.appService.getServiceWithParams(bUrl, devicedetail)
                                    .subscribe(response => {
                                    // this.apiCall.stopLoading();
                                    // this.editdata = response;
                                    let res = JSON.parse(JSON.stringify(response));
                                    this.toastController.create({
                                        message: res.message,
                                        duration: 2000,
                                        position: 'top'
                                    }).then(toastEl => {
                                        toastEl.present();
                                    });
                                    // this.responseMessage = "Edit successfully";
                                    // this.getdevices();
                                    var msg;
                                    if (!d.engine_status) {
                                        msg = this.deviceConfigStatus[0].resume_command;
                                    }
                                    else {
                                        msg = this.deviceConfigStatus[0].immoblizer_command;
                                    }
                                    // this.sms.send(d.sim_number, msg);
                                    // const toast1 = this.toastCtrl.create({
                                    //   message: this.translate.instant('SMS sent successfully'),
                                    //   duration: 2000,
                                    //   position: 'bottom'
                                    // });
                                    // toast1.present();
                                    that.clicked = false;
                                }, error => {
                                    that.clicked = false;
                                    // this.apiCall.stopLoading();
                                    console.log(error);
                                });
                            }
                            else {
                                console.log("Call server code here!!");
                                that.serverLevelOnOff(d);
                            }
                        }
                    },
                    {
                        text: 'NO'
                    }]
            }).then(alertEl => {
                alertEl.present();
            });
        }, error => {
            loadEl.dismiss();
            // this.apiCall.stopLoading();
            console.log("some error: ", error._body.message);
        });
    }
    serverLevelOnOff(d) {
        let that = this;
        that.clicked = true;
        var data = {};
        if (d.device_model.device_type === undefined) {
            data = {
                "imei": d.Device_ID,
                "_id": d._id,
                "engine_status": d.ignitionLock,
                "protocol_type": this.device_type
            };
        }
        else {
            data = {
                "imei": d.Device_ID,
                "_id": d._id,
                "engine_status": d.ignitionLock,
                "protocol_type": d.device_model.device_type
            };
        }
        let bURL = this.constURL.mainUrl + 'devices/addCommandQueue';
        // this.apiCall.startLoading().present();
        this.appService.getServiceWithParams(bURL, data)
            .subscribe(resp => {
            // this.apiCall.stopLoading();
            console.log("ignition on off=> ", resp);
            let respMsg = JSON.parse(JSON.stringify(resp));
            const urlTel = this.constURL.mainUrl + 'trackRouteMap/getRideStatusApp?_id=' + respMsg._id;
            // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
            this.intervalID = setInterval(() => {
                this.appService.getService(urlTel)
                    .subscribe(data => {
                    console.log("interval=> " + data);
                    let commandStatus = JSON.parse(JSON.stringify(data)).status;
                    if (commandStatus == 'SUCCESS') {
                        clearInterval(this.intervalID);
                        that.clicked = false;
                        this.callObjFunc(d);
                        // this.apiCall.stopLoadingnw();
                        this.toastController.create({
                            message: 'Process has been completed successfully...!!',
                            duration: 1500,
                            position: 'bottom'
                        }).then((toastEl => {
                            toastEl.present();
                        }));
                    }
                });
            }, 5000);
        }, err => {
            // this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            that.clicked = false;
        });
    }
    callObjFunc(d) {
        let that = this;
        var _bUrl = this.constURL.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
        this.loadingCtrl.create({
            message: 'Plaese wait...'
        }).then(loadEl => {
            loadEl.present();
            this.appService.getService(_bUrl)
                .subscribe(resp => {
                loadEl.dismiss();
                console.log("updated device object=> " + resp);
                if (!resp) {
                    return;
                }
                else {
                    that.data = resp;
                }
            });
        });
        // this.apiCall.startLoading().present();
    }
    isNight() {
        //Returns true if the time is between
        //7pm to 5am
        let time = new Date().getHours();
        return (time > 5 && time < 19) ? false : true;
    }
    reCenterMe() {
        this.map123.animateCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            // zoom: 16,
            tilt: 30,
            // bearing: this.allData.map.getCameraBearing(),
            duration: 1800,
        }).then(() => {
        });
    }
    getAddress(coordinates) {
        let that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        let tempcord = {
            "lat": coordinates.lat,
            "long": coordinates.long
        };
        this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
            .then(res => {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.address = str;
            // console.log("inside", that.address);
        });
    }
    socketInit(pdata, center = false) {
        this.allData.allKey = [];
        // this.socketChnl.push(pdata.deviceId);
        let that = this;
        if (pdata == undefined) {
            return;
        }
        that.deviceDeatils = pdata;
        // that.vehicle_speed = (that.deviceDeatils.speed).toFixed(2);
        if (pdata.id != undefined && pdata.latitude != undefined && pdata.longitude != undefined) {
            var key = pdata.deviceId;
            // that.vehicle_speed = (pdata.speed).toFixed(2);
            that.recenterMeLat = pdata.latitude;
            that.recenterMeLng = pdata.longitude;
            // if (localStorage.getItem("SocketFirstTime") !== null) {
            //   that.reCenterMe();
            // }
            // that.vehicle_speed = pdata.speed;
            // console.log("check speed: ", that.vehicle_speed)
            if (that.allData[key]) {
                that.socketSwitch[key] = pdata;
                if (that.allData[key].mark !== undefined) {
                    // debugger
                    that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                    that.showTrail(pdata.latitude, pdata.longitude, that.map123);
                    that.allData[key].mark.setIcon(that.getIconUrl(pdata));
                    var temp = lodash__WEBPACK_IMPORTED_MODULE_5__["cloneDeep"](that.allData[key].poly[1]);
                    that.allData[key].poly[0] = lodash__WEBPACK_IMPORTED_MODULE_5__["cloneDeep"](temp);
                    that.allData[key].poly[1] = { lat: pdata.latitude, lng: pdata.longitude };
                    let coordinates = {
                        lat: pdata.latitude,
                        long: pdata.longitude
                    };
                    that.getAddress(coordinates);
                    // var speed = data.status == "RUNNING" ? data.last_speed : 0;
                    // that.liveTrack(that.map123, that.allData[key].mark, that.getIconUrl(pdata), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, that.data.category, that.data.status, that);
                    that.liveTrack(that.map123, that.allData[key].mark, that.allData[key].poly, parseFloat(pdata.speed), 150, center, pdata.deviceId, that);
                }
            }
            else {
                that.allData[key] = {};
                // that.socketSwitch[key] = pdata;
                that.allData.allKey.push(key);
                that.allData[key].poly = [];
                that.allData[key].poly.push({ lat: pdata.latitude, lng: pdata.longitude });
                that.showTrail(that.allData[key].poly[0].lat, that.allData[key].poly[0].lng, that.map123);
                that.map123.addMarker({
                    title: that.data.name,
                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                    icon: {
                        url: that.getIconUrl(pdata),
                        size: {
                            height: 40,
                            width: 20
                        }
                    },
                }).then((marker) => {
                    that.allData[key].mark = marker;
                    ///////////////////////////////
                    localStorage.setItem("SocketFirstTime", "SocketFirstTime");
                    marker.addEventListener(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsEvent"].MARKER_CLICK)
                        .subscribe(e => {
                        that.getAddressTitle(marker);
                    });
                    let coordinates = {
                        lat: pdata.latitude,
                        long: pdata.longitude
                    };
                    that.getAddress(coordinates);
                    that.liveTrack(that.map123, that.allData[key].mark, that.allData[key].poly, parseFloat(pdata.speed), 150, center, pdata.deviceId, that);
                    // that.liveTrack(that.map123, that.allData[key].mark, that.getIconUrl(pdata), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, that.data.category, that.data.status, that);
                });
            }
        }
    }
    showTrail(lat, lng, map) {
        debugger;
        let that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length === 2) {
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then((polyline) => {
                that.allData.polylineID = polyline;
                // that.polylineID.push(polyline);
            });
        }
        else if (that.tempArray.length > 2) {
            that.tempArray.shift();
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then((polyline) => {
                that.allData.polylineID = polyline;
                // that.allData.polylineID.push(polyline);
            });
        }
    }
    getIconUrl(pdata) {
        let that = this;
        var iconUrl;
        if (that.data.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/stopped' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/idling' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/stopped' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        else if (that.data.status === 'offline' || that.data.status === 'unknown') {
            iconUrl = './assets/imgs/vehicles/offline' + (that.data.category ? that.data.category : 'car') + '.png';
        }
        console.log("check status: ", that.data.status);
        console.log("check ignition: ", pdata.attributes.ignition);
        console.log("check motion: ", pdata.attributes.motion);
        console.log("icon url: ", iconUrl);
        return iconUrl;
    }
    getAddressTitle(marker) {
        let that = this;
        this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
            .then(res => {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            // debugger
            let snippetString = `Speed - ${Math.round(that.deviceDeatils.speed * 1.943844)} \nLast Update - ${that.datePipe.transform(new Date(that.data.lastUpdate).toISOString(), 'medium')}\nAddress - ${str}`;
            marker.setSnippet(snippetString);
        });
    }
    liveTrack(map, mark, coords, speed, delay, center, id, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["LatLng"](coords[target].lat, coords[target].lng);
            var distance = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                // debugger
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["LatLng"](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    map.setCameraTarget(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["LatLng"](lat, lng));
                    that.latlngCenter = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["LatLng"](lat, lng);
                    mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["LatLng"](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["Spherical"].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    }
};
LiveTrackPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ToastController"] },
    { type: _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMaps"] },
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"] },
    { type: src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["LoadingController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Events"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["AlertController"] },
    { type: src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_9__["GeocoderAPIService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map123', {
        static: true
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], LiveTrackPage.prototype, "element", void 0);
LiveTrackPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-live-track',
        template: __webpack_require__(/*! raw-loader!./live-track.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.html"),
        styles: [__webpack_require__(/*! ./live-track.page.scss */ "./src/app/tabs/dashboard/vehicle-list/second-tabs/live-track/live-track.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ToastController"],
        _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_4__["GoogleMaps"],
        src_app_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"],
        src_app_app_model__WEBPACK_IMPORTED_MODULE_6__["URLs"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["LoadingController"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Events"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["AlertController"],
        src_app_geocoder_api_service__WEBPACK_IMPORTED_MODULE_9__["GeocoderAPIService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"]
        // private http: HttpClient,
    ])
], LiveTrackPage);



/***/ })

}]);
//# sourceMappingURL=live-track-live-track-module-es2015.js.map