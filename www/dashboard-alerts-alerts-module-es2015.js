(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-alerts-alerts-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/alerts/alerts.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/alerts/alerts.page.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-title>Geofences</ion-title>\n    <ion-icon slot=\"end\" name=\"add\" style=\"font-size: 1.8em; padding-right: 10px;\" (click)=\"addGeofence()\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid style=\"padding: initial;\">\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list *ngFor=\"let geo of geofences\" style=\"padding: initial\">\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <ion-img src=\"assets/imgs/locate.svg\" style=\"width: 25px;\n              height: 25px;\n              margin: auto;\n              margin-top: 15%;\"></ion-img>\n            </ion-avatar>\n            <ion-label>\n              <h3>{{geo.name}}</h3>\n            </ion-label>\n            <p *ngIf=\"geo.description\" style=\"\n                  font-size: smaller;color: gray; margin-bottom: unset;\">Description - {{geo.description}}</p>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/alerts.module.ts":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/alerts.module.ts ***!
  \********************************************************/
/*! exports provided: AlertsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsPageModule", function() { return AlertsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alerts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alerts.page */ "./src/app/tabs/dashboard/alerts/alerts.page.ts");







const routes = [
    {
        path: '',
        component: _alerts_page__WEBPACK_IMPORTED_MODULE_6__["AlertsPage"]
    }
];
let AlertsPageModule = class AlertsPageModule {
};
AlertsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_alerts_page__WEBPACK_IMPORTED_MODULE_6__["AlertsPage"]]
    })
], AlertsPageModule);



/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/alerts.page.scss":
/*!********************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/alerts.page.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvZGFzaGJvYXJkL2FsZXJ0cy9hbGVydHMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/alerts/alerts.page.ts":
/*!******************************************************!*\
  !*** ./src/app/tabs/dashboard/alerts/alerts.page.ts ***!
  \******************************************************/
/*! exports provided: AlertsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsPage", function() { return AlertsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






let AlertsPage = class AlertsPage {
    constructor(events, appService, ionicStorage, router) {
        this.events = events;
        this.appService = appService;
        this.ionicStorage = ionicStorage;
        this.router = router;
        // limit: number = 50;
        // pageNo: number = 1;
        this.geofences = [];
    }
    ionViewWillEnter() {
        this.ionicStorage.get("token").then(val => {
            this.userData = val;
            this.events.publish('cart:updated', 0);
            this.getGeofences();
        });
    }
    ngOnInit() { }
    getGeofences() {
        var _bUrl = '/api/geofences';
        this.appService.getService123(_bUrl, this.userData).subscribe(resp => {
            console.log("geofences: " + resp);
            this.geofences = JSON.parse(JSON.stringify(resp));
            console.log(this.geofences);
        }, err => {
            console.log(err);
        });
    }
    addGeofence() {
        this.router.navigateByUrl('maintabs/tabs/alerts/add-geofence');
    }
};
AlertsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"] },
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
AlertsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-alerts",
        template: __webpack_require__(/*! raw-loader!./alerts.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/alerts/alerts.page.html"),
        styles: [__webpack_require__(/*! ./alerts.page.scss */ "./src/app/tabs/dashboard/alerts/alerts.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"], src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
], AlertsPage);



/***/ })

}]);
//# sourceMappingURL=dashboard-alerts-alerts-module-es2015.js.map