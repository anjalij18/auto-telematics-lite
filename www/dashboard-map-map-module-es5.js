(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-map-map-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/map/map.page.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/dashboard/map/map.page.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"tertiary\">\n    <ion-title>Map</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div #mapElement id=\"mapElement\">\n    <ion-fab vertical=\"top\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button (click)=\"onMapType()\" size=\"small\" color=\"light\">\n        <ion-icon *ngIf=\"afterMapTypeClick\" name=\"logo-buffer\"></ion-icon>\n        <ion-icon *ngIf=\"!afterMapTypeClick\" src=\"assets/imgs/icon/stack-of-square-papers.svg\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n    <ion-fab horizontal=\"end\" slot=\"fixed\" style=\"margin-top: 14%;\">\n      <ion-fab-button (click)=\"onTraffic()\" size=\"small\" color=\"light\">\n        <ion-icon *ngIf=\"!afterClick\" src=\"assets/imgs/icon/traffic-light-before.svg\">\n        </ion-icon>\n        <ion-icon *ngIf=\"afterClick\" src=\"assets/imgs/icon/traffic-light.svg\">\n        </ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/tabs/dashboard/map/map.module.ts":
/*!**************************************************!*\
  !*** ./src/app/tabs/dashboard/map/map.module.ts ***!
  \**************************************************/
/*! exports provided: MapPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map.page */ "./src/app/tabs/dashboard/map/map.page.ts");







var routes = [
    {
        path: '',
        component: _map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]
    }
];
var MapPageModule = /** @class */ (function () {
    function MapPageModule() {
    }
    MapPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]]
        })
    ], MapPageModule);
    return MapPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/dashboard/map/map.page.scss":
/*!**************************************************!*\
  !*** ./src/app/tabs/dashboard/map/map.page.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card {\n  background-color: #f7f7f7;\n  border: solid 1px #ddd;\n  padding: 9px;\n  height: 85px;\n  width: 98%;\n  margin-left: auto;\n  margin-right: auto;\n  margin: auto;\n  text-align: end;\n  font-size: medium;\n  font-family: initial;\n}\n\n.d {\n  margin-left: 20px;\n  margin-top: 10px;\n}\n\nion-item {\n  --background: transparent;\n}\n\n#cart-btn {\n  position: relative;\n  top: 5px;\n}\n\n#cart-badge {\n  position: absolute;\n  top: 1px;\n  right: 15px;\n  border-radius: 50%;\n}\n\n#mapElement {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZmxlZXQtdHJhY2stdHJhY2Nhci9zcmMvYXBwL3RhYnMvZGFzaGJvYXJkL21hcC9tYXAucGFnZS5zY3NzIiwic3JjL2FwcC90YWJzL2Rhc2hib2FyZC9tYXAvbWFwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDQ0o7O0FEQ0U7RUFDRSxpQkFBQTtFQUNBLGdCQUFBO0FDRUo7O0FEQUU7RUFDRSx5QkFBQTtBQ0dKOztBRERFO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0FDSUo7O0FEREU7RUFDRSxrQkFBQTtFQUdBLFFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNFSjs7QURBRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDR0oiLCJmaWxlIjoic3JjL2FwcC90YWJzL2Rhc2hib2FyZC9tYXAvbWFwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xuICAgIGJvcmRlcjogc29saWQgMXB4ICNkZGQ7XG4gICAgcGFkZGluZzogOXB4O1xuICAgIGhlaWdodDogODVweDtcbiAgICB3aWR0aDogOTglO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBtYXJnaW46IGF1dG87XG4gICAgdGV4dC1hbGlnbjogZW5kO1xuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xuICAgIGZvbnQtZmFtaWx5OiBpbml0aWFsO1xuICB9XG4gIC5kIHtcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICB9XG4gIGlvbi1pdGVtIHtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICB9XG4gICNjYXJ0LWJ0biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogNXB4O1xuICB9XG4gIFxuICAjY2FydC1iYWRnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8vIHRvcDogLTVweDtcbiAgICAvLyByaWdodDogMHB4O1xuICAgIHRvcDogMXB4O1xuICAgIHJpZ2h0OiAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgfVxuICAjbWFwRWxlbWVudCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9IiwiaW9uLWNhcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZGRkO1xuICBwYWRkaW5nOiA5cHg7XG4gIGhlaWdodDogODVweDtcbiAgd2lkdGg6IDk4JTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luOiBhdXRvO1xuICB0ZXh0LWFsaWduOiBlbmQ7XG4gIGZvbnQtc2l6ZTogbWVkaXVtO1xuICBmb250LWZhbWlseTogaW5pdGlhbDtcbn1cblxuLmQge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4jY2FydC1idG4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNXB4O1xufVxuXG4jY2FydC1iYWRnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxcHg7XG4gIHJpZ2h0OiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbiNtYXBFbGVtZW50IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/tabs/dashboard/map/map.page.ts":
/*!************************************************!*\
  !*** ./src/app/tabs/dashboard/map/map.page.ts ***!
  \************************************************/
/*! exports provided: MapPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPage", function() { return MapPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-maps/ngx */ "./node_modules/@ionic-native/google-maps/ngx/index.js");








var MapPage = /** @class */ (function () {
    function MapPage(navCtrl, ionicStorage, appService, 
    // private constUrl: URLs,
    loadingCtrl, 
    // private modalController: ModalController,
    router, 
    // private events: Events,
    googleMaps, elementRef) {
        this.navCtrl = navCtrl;
        this.ionicStorage = ionicStorage;
        this.appService = appService;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.googleMaps = googleMaps;
        this.elementRef = elementRef;
        this.cartCount = 0;
        this.dashdata = {};
        this.user = null;
        this.positionsArray = [];
        this.webSocketId = [];
        this.data = [];
        this.allData = {};
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        // furtherFunc() {
        //   let url = '/api/session';
        //   this.loadingCtrl.create({
        //     message: 'Loading...',
        //     spinner: 'lines'
        //   }).then((loadEl) => {
        //     loadEl.present();
        //     this.appService.getServiceTemp123(url, this.userToken).subscribe(
        //       // this.appService.getService123(url, val).subscribe(
        //       response => {
        //         let res = JSON.parse(JSON.stringify(response));
        //         this.ionicStorage.set("token", res).then(() => {
        //           this.ionicStorage.get('token').then((val123) => {
        //             console.log('Your age is', val123);
        //             this.userToken = val123;
        //             /////////////////////////////
        //             this.sockerConnection(loadEl);
        //             ///////////////////
        //           })
        //         });
        //       },
        //       err => {
        //         loadEl.dismiss();
        //         console.log(err);
        //       });
        //   });
        // }
        this.mapData = [];
        // furtherFunc(pData) {
        //   let that = this;
        //   // let tempArray = [];
        //   let _bUrl = '/api/positions';
        //   this.appService.getService123(_bUrl, this.userToken).subscribe((response => {
        //     let res = JSON.parse(JSON.stringify(response));
        //     let _bUrl123 = '/api/devices';
        //     this.appService.getService123(_bUrl123, this.userToken).subscribe(res123 => {
        //       let res111 = JSON.parse(JSON.stringify(res123));
        //       console.log("check data response 111: ", res111);
        //       for (var i = 0; i < res111.length; i++) {
        //         for (var j = 0; j < res.length; j++) {
        //           if (res[j].deviceId === res111[i].id) {
        //             res[j].category = res111[i].category;
        //             res[j].name = res111[i].name;
        //             res[j].lastUpdate = res111[i].lastUpdate;
        //             res[j].status = res111[i].status;
        //           }
        //         }
        //       }
        //       console.log("check data response 123: ", res);
        //       this.vehData = res;
        //       this.map.addListener('click', function (e) {
        //         console.log("clicked on map!!");
        //         // that.ngZone.run(() => {
        //         //   that.router.navigateByUrl('/maintabs/tabs/dashboard/live');
        //         // });
        //       });
        //       this.vehData = res.map(function (d) {
        //         if (d.latitude !== undefined && d.longitude !== undefined) {
        //           return d;
        //         } else {
        //           return null;
        //         }
        //       });
        //       var filtered = this.vehData.filter(function (el) {
        //         return el != null;
        //       });
        //       var infowindow = new google.maps.InfoWindow();
        //       var bounds = new google.maps.LatLngBounds();
        //       for (var i = 0; i < filtered.length; i++) {
        //         let image;
        //         console.log("check category: ", filtered[i].category)
        //         if (filtered[i].status === 'online' && filtered[i].attributes.ignition === true && filtered[i].attributes.motion === true) {
        //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/running' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
        //             new google.maps.Size(20, 40),
        //             new google.maps.Point(0, 0),
        //             new google.maps.Point(0, 40));
        //         } else if (filtered[i].status === 'online' && filtered[i].attributes.ignition === false && filtered[i].attributes.motion === false) {
        //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/stopped' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
        //             new google.maps.Size(20, 40),
        //             new google.maps.Point(0, 0),
        //             new google.maps.Point(0, 40));
        //         } else if (filtered[i].status === 'online' && filtered[i].attributes.ignition === true && filtered[i].attributes.motion === false) {
        //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/idling' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
        //             new google.maps.Size(20, 40),
        //             new google.maps.Point(0, 0),
        //             new google.maps.Point(0, 40));
        //         } else if (filtered[i].status === 'offline') {
        //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/offline' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
        //             new google.maps.Size(20, 40),
        //             new google.maps.Point(0, 0),
        //             new google.maps.Point(0, 40));
        //         }
        //         var beach = filtered[i];
        //         var myLatLng = new google.maps.LatLng(beach.latitude, beach.longitude);
        //         var marker = new google.maps.Marker({
        //           position: myLatLng,
        //           map: this.map,
        //           icon: image,
        //           // shape: shape,
        //           title: beach.deviceId,
        //           zIndex: i + 1
        //         });
        //         bounds.extend(myLatLng);
        //         var contentString = `${filtered[i].name}`;
        //         // var contentString = `Name - ${filtered[i].name}\nSpeed - ${(filtered[i].speed).toFixed(2)} km/hr\nLast Updated - ${(filtered[i].lastUpdate ? that.datePipe.transform(new Date(filtered[i].lastUpdate), 'mediumDate') : null)}`;
        //         var infowindow = new google.maps.InfoWindow({
        //           content: contentString
        //         });
        //         infowindow.open(this.map, marker);
        //       }
        //       this.map.fitBounds(bounds);
        //     },
        //       err => {
        //         console.log(err);
        //       })
        //     // debugger
        //   }))
        // }
        // async presentModal() {
        //   const modal = await this.modalController.create({
        //     component: LiveComponent
        //   });
        //   return await modal.present();
        // }
        this.afterClick = false;
        this.afterMapTypeClick = false;
        this.server_ip = localStorage.getItem("server_ip");
    }
    MapPage.prototype.ionViewWillEnter = function () { };
    MapPage.prototype.ionViewDidLeave = function () {
        var that = this;
        for (var r = 0; r < that.webSocketId.length; r++) {
            CordovaWebsocketPlugin.wsClose(that.webSocketId[r], 1000, "I'm done!");
        }
    };
    MapPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ionicStorage.get('token').then(function (val) {
            console.log('Your age is', val);
            _this.userToken = val;
            _this.getVehicleList();
        });
        var notifToken = localStorage.getItem("notificationTokens");
        if (notifToken !== null) {
            this.saveToken(notifToken);
        }
    };
    MapPage.prototype.sockerConnection = function (loadEl) {
        var that = this;
        this.appService.getSessionwithToken(this.userToken.token).subscribe(function (resp) {
            console.log("intercept works or not ....", resp);
            cookieMaster.getCookieValue(that.server_ip + "/api/session", 'JSESSIONID', function (data) {
                // console.log("we got cookie... " + data.cookieValue);
                var options = {
                    url: 'ws://128.199.21.17:8082/api/socket',
                    pingInterval: 3000,
                    headers: { "Cookie": "JSESSIONID=" + data.cookieValue },
                };
                loadEl.dismiss();
                CordovaWebsocketPlugin.wsConnect(options, function (recvEvent) {
                    // debugger
                    // console.log("Received callback from WebSocket: " + JSON.parse(JSON.stringify(recvEvent.message)));
                    if (JSON.parse(recvEvent.message).positions) {
                        var a = JSON.parse(recvEvent.message).positions;
                        that.positionsArray = a;
                        that.furtherCalc(that.positionsArray);
                    }
                    if (JSON.parse(recvEvent.message).devices) {
                        var b = JSON.parse(recvEvent.message).devices;
                        that.furtherCalc123(b);
                    }
                }, function (success) {
                    console.log("Connected to WebSocket with id: " + success.webSocketId);
                    that.webSocketId.push(success.webSocketId);
                }, function (error) {
                    console.log("Failed to connect to WebSocket: " +
                        "code: " + error["code"] +
                        ", reason: " + error["reason"] +
                        ", exception: " + error["exception"]);
                });
            });
        }, function (err) {
            loadEl.dismiss();
            console.log("intercept error ", err);
        });
    };
    MapPage.prototype.furtherCalc123 = function (b) {
        var that = this;
        var i = 0, howManyTimes = b.length;
        function f() {
            if (b[i].id === that.data[i].id) {
                that.data[i] = b[i];
            }
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    MapPage.prototype.furtherCalc = function (a) {
        var that = this;
        var i = 0, howManyTimes = a.length;
        function f() {
            // debugger
            if (a[i].deviceId === that.data[i].id) {
                that.socketInit(a[i], that.data[i]);
            }
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    MapPage.prototype.socketInit = function (pdata, data_i, center) {
        if (center === void 0) { center = false; }
        this.allData.allKey = [];
        // this.socketChnl.push(pdata.deviceId);
        var that = this;
        if (pdata == undefined) {
            return;
        }
        // that.deviceDeatils = pdata;
        if (pdata.id != undefined && pdata.latitude != undefined && pdata.longitude != undefined) {
            var key = pdata.deviceId;
            if (that.allData[key]) {
                // that.socketSwitch[key] = pdata;
                if (that.allData[key].mark !== undefined) {
                    that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                    that.allData[key].mark.setIcon(that.getIconUrl(pdata, data_i));
                    var temp = lodash__WEBPACK_IMPORTED_MODULE_6__["cloneDeep"](that.allData[key].poly[1]);
                    that.allData[key].poly[0] = lodash__WEBPACK_IMPORTED_MODULE_6__["cloneDeep"](temp);
                    that.allData[key].poly[1] = { lat: pdata.latitude, lng: pdata.longitude };
                    // let coordinates = {
                    //   lat: pdata.latitude,
                    //   long: pdata.longitude
                    // }
                    // that.getAddress(coordinates);
                    // var speed = data.status == "RUNNING" ? data.last_speed : 0;
                    // that.liveTrack(that.map, that.allData[key].mark, that.getIconUrl(pdata, data_i), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, data_i.category, data_i.status, that);
                    that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
                    that.liveTrack(that.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that);
                }
            }
            else {
                that.allData[key] = {};
                // that.socketSwitch[key] = pdata;
                that.allData.allKey.push(key);
                that.allData[key].poly = [];
                that.allData[key].poly.push({ lat: pdata.latitude, lng: pdata.longitude });
                // that.iconUrl = './assets/imgs/vehicles/running' + (this.data.category ? this.data.category : 'car') + '.png';
                // console.log("icon url check: ", this.iconUrl);
                that.showMergedMarkers(pdata, key, that.getIconUrl(pdata, data_i), center, data_i);
                // that.map.addMarker({
                //   title: data_i.name,
                //   // snippet: titleStr,
                //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                //   icon: {
                //     url: that.getIconUrl(pdata, data_i),
                //     size: {
                //       height: 40,
                //       width: 20
                //     }
                //   },
                // }).then((marker: Marker) => {
                //   that.allData[key].mark = marker;
                //   ///////////////////////////////
                //   localStorage.setItem("SocketFirstTime", "SocketFirstTime");
                //   // marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                //   //   .subscribe(e => {
                //   //     that.getAddressTitle(marker);
                //   //   });
                //   // let coordinates = {
                //   //   lat: pdata.latitude,
                //   //   long: pdata.longitude
                //   // }
                //   // that.getAddress(coordinates);
                //   // var speed = data.status == "RUNNING" ? data.last_speed : 0;
                //   that.liveTrack(that.map, that.allData[key].mark, that.getIconUrl(pdata, data_i), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, data_i.category, data_i.status, that);
                // });
            }
        }
    };
    MapPage.prototype.showMergedMarkers = function (data, key, ic, center, data_i) {
        var that = this;
        /////////////// merge images using canvas
        var c = document.createElement("canvas");
        c.width = 100;
        c.height = 60;
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#1c2f66";
        ctx.fillRect(0, 0, 100, 20);
        // ctx.strokeRect(0, 0, 100, 70);
        ctx.font = "9pt sans-serif";
        ctx.fillStyle = "white";
        ctx.fillText(data_i.name, 12, 15);
        var img = c.toDataURL();
        that.map.addMarker({
            // title: data.Device_Name,
            position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
            icon: {
                url: img,
                size: {
                    height: 60,
                    width: 100
                }
            },
        }).then(function (marker1) {
            that.allData[key].mark1 = marker1;
            that.map.addMarker({
                // title: data.Device_Name,
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: {
                    url: ic,
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                that.allData[key].mark = marker;
                // marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                //   .subscribe(e => {
                //     if (that.selectedVehicle == undefined) {
                //       that.getAddressTitle(marker);
                //     } else {
                //       that.liveVehicleName = data.Device_Name;
                //       // that.drawerHidden = false;
                //       that.showaddpoibtn = true;
                //       that.drawerState = DrawerState.Docked;
                //       that.onClickShow = true;
                //     }
                //   });
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
            });
        });
    };
    MapPage.prototype.getIconUrl = function (pdata, data_i) {
        var iconUrl;
        if (data_i.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/stopped' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/idling' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === false) {
            iconUrl = './assets/imgs/vehicles/stopped' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === true) {
            iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        else if (data_i.status === 'offline' || data_i.status === 'unknown') {
            iconUrl = './assets/imgs/vehicles/offline' + (data_i.category ? data_i.category : 'car') + '.png';
        }
        console.log("vehicle name: ", data_i.name);
        console.log("check status: ", data_i.status);
        console.log("check ignition: ", pdata.attributes.ignition);
        console.log("check motion: ", pdata.attributes.motion);
        console.log("icon url: ", iconUrl);
        return iconUrl;
    };
    // getAddressTitle(marker) {
    //   let that = this;
    //   this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
    //     .then(res => {
    //       var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
    //       debugger
    //       let snippetString = `Speed - ${Math.round(that.deviceDeatils.speed)} \nLast Update - ${that.datePipe.transform(new Date(that.data.lastUpdate).toISOString(), 'medium')}\nAddress - ${str}`;
    //       marker.setSnippet(snippetString);
    //     })
    // }
    MapPage.prototype.liveTrack = function (map, mark, mark1, coords, speed, delay, center, id, that) {
        // liveTrack(map, mark, mark1, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            // console.log("issue coming from livetrack 1", mark)
            var lat = 0;
            var lng = 0;
            if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
                lat = mark.getPosition().lat;
                lng = mark.getPosition().lng;
            }
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLng"](coords[target].lat, coords[target].lng);
            var distance = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLng"](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(new LatLng(lat, lng));
                    // }
                    that.latlngCenter = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLng"](lat, lng);
                    mark.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLng"](lat, lng));
                    if (that.displayNames) {
                        mark1.setPosition(new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLng"](lat, lng));
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(lat, lng))
                    ////////
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["Spherical"].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(dest);
                    // }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    if (that.displayNames) {
                        mark1.setPosition(dest);
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
                    ////////
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    MapPage.prototype.ngOnInit = function () { };
    MapPage.prototype.ngAfterContentInit = function () {
        // this.map = new google.maps.Map(
        //   this.mapElement.nativeElement,
        //   {
        //     disableDefaultUI: true,
        //     mapTypeControl: true,
        //     streetViewControl: true
        //   }
        // );
        this.initMap();
    };
    MapPage.prototype.initMap = function () {
        if (this.map) {
            this.map.remove();
        }
        var style = [];
        var mapOptions = {
            backgroundColor: 'white',
            controls: {
                compass: false,
                zoom: true,
            },
            gestures: {
                rotate: true,
                tilt: false
            },
            styles: style
        };
        if (this.map === undefined) {
            this.map = _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["GoogleMaps"].create('mapElement', mapOptions);
        }
    };
    MapPage.prototype.gotoDetail = function (key) {
        console.log('check key: ', key);
        this.router.navigateByUrl('/maintabs/tabs/vehicle-list/' + key);
    };
    MapPage.prototype.getVehicleList = function () {
        var _this = this;
        this.dashdata.offlineData = [];
        this.dashdata.onlineData = [];
        var _baseUrl = '/api/devices?entityId=' + this.userToken.id;
        this.loadingCtrl.create({
            keyboardClose: true, message: 'Loading data...'
        }).then(function (loadingEl) {
            loadingEl.present();
            _this.appService.getService123(_baseUrl, _this.userToken)
                .subscribe(function (respData) {
                loadingEl.dismiss();
                var res = JSON.parse(JSON.stringify(respData));
                _this.data = res;
                for (var i = 0; i < res.length; i++) {
                    if (res[i].status === 'offline') {
                        _this.dashdata.offlineData.push(res[i]);
                        console.log("offline : " + _this.dashdata.offlineData.length);
                    }
                    else if (res[i].status === 'online') {
                        _this.dashdata.onlineData.push(res[i]);
                        console.log("onlineData : " + _this.dashdata.onlineData.length);
                    }
                }
                _this.furtherFunc();
            }, function (err) {
                loadingEl.dismiss();
            });
        });
    };
    MapPage.prototype.furtherFunc = function () {
        var _this = this;
        var _bUrl = '/api/positions';
        this.appService.getService123(_bUrl, this.userToken).subscribe((function (response) {
            var resPositopns = JSON.parse(JSON.stringify(response));
            var _bUrl123 = '/api/devices';
            _this.appService.getService123(_bUrl123, _this.userToken).subscribe(function (res123) {
                var resDevices = JSON.parse(JSON.stringify(res123));
                // this.data = resDevices;
                // console.log("check data response 111: ", resDevices);
                for (var i = 0; i < resDevices.length; i++) {
                    for (var j = 0; j < resPositopns.length; j++) {
                        if (resPositopns[j].deviceId === resDevices[i].id) {
                            resPositopns[j].category = resDevices[i].category;
                            resPositopns[j].name = resDevices[i].name;
                            resPositopns[j].lastUpdate = resDevices[i].lastUpdate;
                            resPositopns[j].status = resDevices[i].status;
                        }
                    }
                }
                _this.vehData = resPositopns;
                _this.vehData = resPositopns.map(function (d) {
                    if (d.latitude !== undefined && d.longitude !== undefined) {
                        return d;
                    }
                    else {
                        return null;
                    }
                });
                var filtered = _this.vehData.filter(function (el) {
                    return el != null;
                });
                _this.mapData = filtered.map(function (d) {
                    return { lat: d.latitude, lng: d.longitude };
                });
                var bounds = new _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["LatLngBounds"](_this.mapData);
                _this.map.moveCamera({
                    target: bounds,
                    zoom: 10
                });
                var url = '/api/session';
                _this.loadingCtrl.create({
                    message: 'Loading...',
                    spinner: 'lines'
                }).then(function (loadEl) {
                    loadEl.present();
                    _this.appService.getServiceTemp123(url, _this.userToken).subscribe(
                    // this.appService.getService123(url, val).subscribe(
                    function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        _this.ionicStorage.set("token", res).then(function () {
                            _this.ionicStorage.get('token').then(function (val123) {
                                console.log('Your age is', val123);
                                _this.userToken = val123;
                                /////////////////////////////
                                _this.sockerConnection(loadEl);
                                ///////////////////
                            });
                        });
                    }, function (err) {
                        loadEl.dismiss();
                        console.log(err);
                    });
                });
            }, function (err) {
                console.log(err);
            });
        }));
    };
    MapPage.prototype.onTraffic = function () {
        // debugger
        this.afterClick = !this.afterClick;
        if (this.afterClick) {
            this.map.setTrafficEnabled(true);
        }
        else {
            this.map.setTrafficEnabled(false);
        }
    };
    MapPage.prototype.onMapType = function () {
        // debugger
        this.afterMapTypeClick = !this.afterMapTypeClick;
        if (this.afterMapTypeClick) {
            this.map.setMapTypeId(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["GoogleMapsMapTypeId"].HYBRID);
        }
        else {
            this.map.setMapTypeId(_ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["GoogleMapsMapTypeId"].NORMAL);
        }
    };
    MapPage.prototype.saveToken = function (notifToken) {
        var _this = this;
        var url = '/api/session';
        this.ionicStorage.get('token').then(function (user) {
            _this.appService.getServiceTemp123(url, user).subscribe(function (response) {
                var res = JSON.parse(JSON.stringify(response));
                _this.ionicStorage.set("token", res).then(function () {
                    _this.ionicStorage.get('token').then(function (val123) {
                        console.log('Your age is', val123);
                        // debugger
                        // if (!user['attributes']['notificationTokens'] || user['attributes']['notificationTokens'].indexOf(token) < 0) {
                        if (!val123['attributes']['notificationTokens']) {
                            var tokens = notifToken;
                            var stringSplited = tokens.split(',');
                            _this.UpdateAppToken(stringSplited, val123);
                        }
                        else {
                            //////////////////////////////////////////////
                            var tokens = val123['attributes']['notificationTokens'] += ',' + notifToken;
                            var stringSplited = tokens.split(',');
                            var obj = {};
                            for (var i = 0, len = stringSplited.length; i < len; i++)
                                obj[stringSplited[i]] = stringSplited[i];
                            stringSplited = new Array();
                            for (var key in obj)
                                stringSplited.push(obj[key]);
                            console.log(stringSplited);
                            _this.UpdateAppToken(stringSplited, val123);
                            //////////////////////////////////////////////
                        }
                    });
                });
            }, function (err) {
                console.log(err);
            });
        });
    };
    MapPage.prototype.UpdateAppToken = function (token, user) {
        var url = '/api/users/' + user.id;
        user.attributes.notificationTokens = token.join();
        var payload = user;
        this.appService.putService123(url, user, payload).subscribe(function (resp) {
            var res = JSON.parse(JSON.stringify(resp));
            console.log(res);
        }, function (err) {
            console.log("component error : ", err);
        });
    };
    MapPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["GoogleMaps"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mapElement', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map',
            template: __webpack_require__(/*! raw-loader!./map.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/dashboard/map/map.page.html"),
            styles: [__webpack_require__(/*! ./map.page.scss */ "./src/app/tabs/dashboard/map/map.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _ionic_native_google_maps_ngx__WEBPACK_IMPORTED_MODULE_7__["GoogleMaps"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], MapPage);
    return MapPage;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-map-map-module-es5.js.map