(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fleet-manager-fleet-manager-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fleet-manager/fleet-manager.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fleet-manager/fleet-manager.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <div style=\"margin: auto; margin-top: 50%;\">\n          <div style=\"text-align: center; margin: auto;\">\n            <p style=\"font-size: 1.5em; font-weight: 500;\">OneQlik Server</p>\n          </div>\n\n          <ion-list>\n            <ion-item>\n              <ion-label position=\"fixed\">Server</ion-label>\n              <ion-input type=\"text\" [(ngModel)]=\"server_ip\"></ion-input>\n            </ion-item>\n          </ion-list>\n\n          <div style=\"text-align: center; margin: auto;\">\n            <p style=\"font-size: 1em; font-weight: 500; color: gray;\">This screen will be shown only once. After you\n              click the button,\n              server address will be saved in preferences. To Change it, clear app data from application manager or\n              logout\n              from app.</p>\n          </div>\n\n          <ion-row>\n            <ion-col size=\"12\" class=\"ion-text-center\">\n              <ion-button color=\"tertiary\" (click)=\"onConnect()\" [disabled]=\"!server_ip\">Start</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/fleet-manager/fleet-manager.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/fleet-manager/fleet-manager.module.ts ***!
  \*******************************************************/
/*! exports provided: FleetManagerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FleetManagerPageModule", function() { return FleetManagerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fleet_manager_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fleet-manager.page */ "./src/app/fleet-manager/fleet-manager.page.ts");







const routes = [
    {
        path: '',
        component: _fleet_manager_page__WEBPACK_IMPORTED_MODULE_6__["FleetManagerPage"]
    }
];
let FleetManagerPageModule = class FleetManagerPageModule {
};
FleetManagerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_fleet_manager_page__WEBPACK_IMPORTED_MODULE_6__["FleetManagerPage"]]
    })
], FleetManagerPageModule);



/***/ }),

/***/ "./src/app/fleet-manager/fleet-manager.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/fleet-manager/fleet-manager.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZsZWV0LW1hbmFnZXIvZmxlZXQtbWFuYWdlci5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/fleet-manager/fleet-manager.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/fleet-manager/fleet-manager.page.ts ***!
  \*****************************************************/
/*! exports provided: FleetManagerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FleetManagerPage", function() { return FleetManagerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let FleetManagerPage = class FleetManagerPage {
    constructor(router, apiCall, alertController) {
        this.router = router;
        this.apiCall = apiCall;
        this.alertController = alertController;
        this.server_ip = 'http://128.199.21.17:8082';
    }
    ngOnInit() {
    }
    onConnect() {
        if (this.server_ip === undefined) {
            return;
        }
        localStorage.setItem("server_ip", this.server_ip);
        let timestamp = new Date().getTime();
        let _burl = '/api/server?_dc=' + timestamp;
        this.apiCall.getServiceTemp(_burl)
            .subscribe((data) => {
            // this.alertController.create({
            //   message: 'Connection successfull..!',
            //   buttons: ['Okay']
            // }).then(alertEl => {
            //   alertEl.present();
            // });
            this.router.navigate(['login']);
        }, err => {
            console.log("got session error=> ", err);
            this.alertController.create({
                message: 'Connection failed.. please try with valid ip address!',
                buttons: ['Okay']
            }).then(alertEl => {
                alertEl.present();
            });
        });
        // }
    }
};
FleetManagerPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
];
FleetManagerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fleet-manager',
        template: __webpack_require__(/*! raw-loader!./fleet-manager.page.html */ "./node_modules/raw-loader/index.js!./src/app/fleet-manager/fleet-manager.page.html"),
        styles: [__webpack_require__(/*! ./fleet-manager.page.scss */ "./src/app/fleet-manager/fleet-manager.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
], FleetManagerPage);



/***/ })

}]);
//# sourceMappingURL=fleet-manager-fleet-manager-module-es2015.js.map