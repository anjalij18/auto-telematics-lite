import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { Router } from '@angular/router';

@Injectable({
  providedIn: "root"
})
export class AppService {
  startLoading() {
    throw new Error("Method not implemented.");
  }
  loading: any;
  loading1: any;
  server_ip: any;
  constructor(
    private http: HttpClient,
    private router: Router
  ) {

  }

  authenticateUser(authParams) {

    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let body = `email=${authParams.email}&password=${authParams.password}`;
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    return this.http
      .post(this.server_ip + "/api/session", body, { headers: headers })
      .pipe(
        tap(respData => {
          // console.log(respData)
        })
      );
  }

  getService(passedUrl) {
    return this.http.get(passedUrl).pipe(tap(respData => {
    }))
  }
  getService123(passedUrl, token) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");
    var string = token.email + ':' + pass;
    // var string = token.email + ':' + token.name;
    // Encode the String
    var encodedString = btoa(string);
    console.log(encodedString);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': encodedString
      });
    return this.http.get(this.server_ip + passedUrl, { headers: headers }).pipe(tap(respData => {
    }))
  }

  postService123(passedUrl, token, payload) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");
    var string = token.email + ':' + pass;
    // Encode the String
    var encodedString = btoa(string);
    console.log(encodedString);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': encodedString
      });
    return this.http.post(this.server_ip + passedUrl, payload, { headers: headers }).pipe(tap(respData => {
    }))
  }

  putService123(passedUrl, token, payload) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");
    var string = token.email + ':' + pass;
    // Encode the String
    var encodedString = btoa(string);
    console.log(encodedString);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': encodedString
      });
    return this.http.put(this.server_ip + passedUrl, payload, { headers: headers }).pipe(tap(respData => {
    }))
  }

  getServiceTemp(url) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        // 'Authorization': encodedString
      });
    return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true }).pipe(tap(respData => {
    }))
  }

  // getServiceTemp111(url, token) {
  //   if (localStorage.getItem('server_ip') !== null) {
  //     this.server_ip = localStorage.getItem('server_ip');
  //   } else {
  //     this.router.navigate(['fleet-manager']);
  //   }
  //   var string = token.email + ':' + token.name;
  //   // Encode the String
  //   var encodedString = btoa(string);
  //   let headers = new HttpHeaders(
  //     {
  //       'Content-Type': 'application/json',
  //       'Authorization': encodedString
  //     });
  //   return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true }).pipe(tap(respData => {
  //   }))
  // }

  getServiceTemp123(url, authParams) {

    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");
    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let body = `email=${authParams.email}&password=${pass}`;
    // let body = `email=${authParams.email}&password=${authParams.name}`;
    return this.http.post(this.server_ip + url, body, { headers: headers, withCredentials: true }).pipe(tap(respData => {
    }))
  }

  getSessionwithToken(token) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    // let body = `email=${authParams.email}&password=${authParams.name}`;
    return this.http.get(this.server_ip + '/api/session?token=' + token, { headers: headers, withCredentials: true }).pipe(tap(respData => {
    }))
  }

  getReportData(url, data) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");
    var string = data.email + ':' + pass;
    // Encode the String
    var encodedString = btoa(string);
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': encodedString
      }
    );
    return this.http.get(this.server_ip + url, { headers: headers, withCredentials: true })
  }

  getCookieWithSession(passedUrl, token) {
    if (localStorage.getItem('server_ip') !== null) {
      this.server_ip = localStorage.getItem('server_ip');
    } else {
      this.router.navigate(['fleet-manager']);
    }
    let pass = localStorage.getItem("password");

    let body = `email=${token.email}&password=${pass}`;
    // let body = `email=${token.email}&password=${token.name}`;
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/x-www-form-urlencoded'
        // 'Authorization': encodedString
      });
    return this.http.post(this.server_ip + passedUrl, body, { headers: headers }).pipe(tap(respData => {
    }))
  }


  getServiceWithParams(passedUrl, data) {
    return this.http.post(passedUrl, data).pipe(tap(respData => {
    }))
  }

  getData(eventName) {
    // return this.socket.fromEvent(eventName).pipe(map(data => data)); // with pipe rxjs6
  }

  // getAddress(cord) {
  //   return this.http.post("https://www.oneqlik.in/googleAddress/getGoogleAddress", cord)
  //     .pipe(map(res => res));
  // }
}
