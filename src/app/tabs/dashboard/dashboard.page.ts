import { Component, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { Storage } from '@ionic/storage';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import '../../../assets/js/chartjs-plugin-labels.min.js'
// declare var cookieMaster: any;
// declare var CordovaWebsocketPlugin: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {
 
  text1Chart: any;
  @ViewChild('doughnutCanvas', {
    static: true
  }) doughnutCanvas;
  doughnutChart: any;
  server_ip: any;


  constructor(
    private ionicStorage: Storage,
    private appService: AppService,
    private router: Router,
    private toastController: ToastController
  ) {
    this.server_ip = localStorage.getItem("server_ip");
  }

  ionViewWillEnter() {
    this.plugin();
  }
  userToken: any;
  ionViewDidEnter() {
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userToken = val;
      // this.getChart();
      this.getVehicleList();
    })
  }

  vehicleList123: any = [];
  _onlineCount: number = 0;
  _offlineCount: number = 0;
  getVehicleList() {
    this._onlineCount = 0;
    this._offlineCount = 0;
    let _bUrl = '/api/positions';

    this.appService.getService123(_bUrl, this.userToken).subscribe((response: Response) => {
      let res111 = JSON.parse(JSON.stringify(response));
      let baseURLp = '/api/devices?entityId=' + this.userToken.id;

      this.appService.getService123(baseURLp, this.userToken).subscribe(
        resp => {
          console.log("vehicle list: ", resp);
          let res123 = JSON.parse(JSON.stringify(resp));
          let res = [];

          for (var i = 0; i < res123.length; i++) {
            for (var j = 0; j < res111.length; j++) {
              if (res111[j].deviceId === res123[i].id) {
                res123[i].attributes = res111[j].attributes;
              }
            }
          }

          for (var t = 0; t < res123.length; t++) {
            if (res123[t].status === 'online') {
              this._onlineCount = this._onlineCount + 1;
            }
            if (res123[t].status === 'offline' || res123[t].status === 'unknown') {
              this._offlineCount = this._offlineCount + 1;
            }
          }

          this.getChart();
        },
        err => {
          this.toastController
            .create({
              message: "Something went wrong. Please try after some time..",
              position: "bottom",
              duration: 1500
            })
            .then(toastEl => {
              toastEl.present();
            });
        });
    },
      err => {
        console.log("error: ", err);
      });
  }

  plugin() {
    // let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "50px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          // var width = chart.chart.width,
          //   height = chart.chart.height,
          //   ctx = chart.chart.ctx;

          // var d = Math.min(width, height);
          // var a = d / 2;

          // that.text1Chart.style.left = (((width - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.top = (((height - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.width = a + "px";
          // that.text1Chart.style.height = a + "px";
          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });
  }

  getChart() {
    let that = this;
    that.plugin();
    // if the chart is not undefined (e.g. it has been created)
    // then destory the old one so we can create a new one later
    if (this.doughnutChart) {
      this.doughnutChart.destroy();
    }

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ["ONLINE", "OFFLINE"],
        datasets: [{
          label: '# of Votes',
          data: [this._onlineCount, this._offlineCount],
          backgroundColor: ['#11a46e', '#3570de'],
          hoverBackgroundColor: ['rgba(51, 255, 153, 0.7)', 'rgba(159, 189, 245, 0.7)']
        }]
      },
      options: {
        cutoutPercentage: 75,
        title: {
          display: true,
          fontSize: 14,
          text: 'Vehicle Current Status'
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 16,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        elements: {
          center: {
            text: (this._offlineCount + this._onlineCount),
            color: '#f2b818', // Default is #000000
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 10, // Defualt is 20 (as a percentage),
          },

        },
        tooltips: {
          enabled: false
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: false,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 10,
            fontStyle: 'bold',
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        },
        // onClick: function (event, elementsAtEvent) {
        //   var activePoints = elementsAtEvent;
        //   if (activePoints[0]) {
        //     var chartData = activePoints[0]['_chart'].config.data;
        //     var idx = activePoints[0]['_index'];
        //     var label = chartData.labels[idx];
        //     var value = chartData.datasets[0].data[idx];
        //     if (label === 'EXPIRED') {
        //       localStorage.setItem('status', 'Expired');
        //     } else if (label === 'NO GPS') {
        //       localStorage.setItem('status', 'NO GPS FIX');
        //     } else {
        //       localStorage.setItem('status', label);
        //     }
        //   }
        // }
      }
    });
  }

  gotoSettings() {
    this.router.navigateByUrl('/maintabs/tabs/settings');
  }

  // progressBar() {
  //   // Test interval to show the progress bar
  //   this.progressIntervalId = setInterval(() => {
  //     if (this.loadProgress < 100) {
  //       this.loadProgress += 1;

  //       // console.log("progress: ", this.loadProgress);
  //     }
  //     else {
  //       // if(that.totaldevices)
  //       clearInterval(this.loadProgress);
  //     }
  //   }, 100);
  // }

}
