import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  userToken: any;

  constructor(
    private apiCall: AppService,
    private ionStorage: Storage,
    private route: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.ionStorage.get('token').then((token) => {
      debugger
      this.userToken = token;
      this.getCustomer();
    })
  }
  userList: any = [];
  getCustomer() {
    let url = '/api/users';
    this.apiCall.getService123(url, this.userToken).subscribe(resp => {
      let res = JSON.parse(JSON.stringify(resp));
      console.log("users: " + res);
      this.userList = res;
    },
      err => {
        console.log(err);
      });
  }

  addUser() {
    this.route.navigateByUrl('/maintabs/tabs/settings/users/add-user');
  }

  devicesList: any = [];
  addedDevicesList: any = [];
  addDevice(userData) {
    let url = '/api/devices';
    this.apiCall.getService123(url, this.userToken).subscribe((resp: Response) => {
      this.devicesList = JSON.parse(JSON.stringify(resp));
      let _url = '/api/devices?userId=' + userData.id;
      this.apiCall.getService123(_url, this.userToken).subscribe((response: Response) => {
        this.addedDevicesList = JSON.parse(JSON.stringify(response));

        let alertOptions = []; let alertOptions123 = [];
      
        for (var i = 0; i < this.devicesList.length; i++) {
          alertOptions.push({
            name: this.devicesList[i].name,
            type: 'checkbox',
            label: this.devicesList[i].name,
            value: this.devicesList[i]
          })
        }
        for (var j = 0; j < this.addedDevicesList.length; j++) {
          alertOptions123.push({
            name: this.addedDevicesList[j].name,
            type: 'checkbox',
            label: this.addedDevicesList[j].name,
            value: this.addedDevicesList[j],
            checked: true
          })
        }
        const result = Object.values(
          [].concat(alertOptions, alertOptions123)
            .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {})
        );
        // debugger
        // console.log(result);

        this.alertController.create({
          header: 'Select Device',
          inputs: result,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            }, {
              text: 'Ok',
              handler: (data) => {
                console.log('Confirm Ok', data);
                this.addPermissions(userData, data);
              }
            }
          ]
        }).then((alertEl) => {
          alertEl.present();
        })
      },
        err => {
          console.log("got error: ", err);
        })

    },
      err => {
        console.log("got error: ", err);
      });
  }

  addPermissions(userData, selectedData) {
    debugger
    let url = '/api/permissions';
    let that = this;
    // outerthis.stoppageReport = [];
    var i = 0, howManyTimes = selectedData.length;
    function f() {
      let payload = {
        "userId": userData.id,
        "deviceId": selectedData[i].id
      }
      that.apiCall.postService123(url, that.userToken, payload).subscribe((resp: Response) => {
        let res = JSON.parse(JSON.stringify(resp));
        console.log("check permissions: ", res);
      },
        err => {
          console.log("got err: ", err);
        })
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }
}
