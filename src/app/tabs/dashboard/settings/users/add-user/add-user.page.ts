import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  user_limit: any;
  device_limit: any;
  expiration_date: any;
  limit_commands: boolean;
  device_readonly: boolean;
  readonly: boolean;
  admin: boolean;
  disabled: boolean;

  constructor(
    private apiCall: AppService,
    private ionStorage: Storage,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }
  userToken: any;
  showPass: boolean = false;

  ionViewDidEnter() {
    this.ionStorage.get('token').then((data) => {
      this.userToken = data;
    })
  }
  name: string; email: any; password: any;
  addUser() {
    let url = '/api/users';
    let payload = {
      "id": -1,
      "name": this.name,
      "login": "",
      "email": this.email,
      "password": this.password,
      "phone": "",
      "readonly": (this.readonly ? this.readonly : false),
      "administrator": false,
      "map": "",
      "latitude": 0,
      "longitude": 0,
      "zoom": 0,
      "twelveHourFormat": false,
      "coordinateFormat": "",
      "disabled": (this.disabled ? this.disabled : false),
      "expirationTime": (this.expiration_date ? this.expiration_date : null),
      "deviceLimit": (this.device_limit ? this.device_limit : 0),
      "userLimit": (this.user_limit ? this.user_limit : 0),
      "deviceReadonly": (this.device_readonly ? this.device_readonly : false),
      "limitCommands": (this.limit_commands ? this.limit_commands : false),
      "poiLayer": "",
      "token": ""
    }
    this.apiCall.postService123(url, this.userToken, payload).subscribe(response => {
      let res = JSON.parse(JSON.stringify(response));
      console.log("check if user is created or not? ", res);
      this.toastCtrl.create({
        message: 'User has been created successfully.',
        position: 'bottom',
        duration: 3000
      }).then((toastEl) => {
        toastEl.present();
        setTimeout(() => {
          this.navCtrl.pop();
        }, 3000)
      })
    },
      err => {
        console.log(err);
      })
  }

}
