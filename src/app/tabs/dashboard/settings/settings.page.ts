import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { AuthenticationService } from 'src/app/services/authentication.service';
// import { AuthService } from 'src/app/services/auth.service';
import { AlertController } from '@ionic/angular';
// import { AppService } from 'src/app/app.service';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"]
})
export class SettingsPage implements OnInit {
  userdetails: any = {};
  token: string;
  app_version: any;
  constructor(
    private router: Router,
    private ionicStorage: Storage,
    private authService: AuthenticationService,
    // private auth: AuthService,
    // private apiCall: AppService,
    // private platform: Platform,
    private alertCtrl: AlertController,
    // private loadingController: LoadingController,
    private appVersion: AppVersion
  ) {
    this.appVersion.getVersionNumber().then((number) => {
      this.app_version = number;
    })
  }

  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      console.log("Your data is", val);
      this.userdetails = val;
    });
  }

  ngOnInit() {
    // this.ionicStorage.get("token").then(val => {
    //   console.log("Your data is", val);
    //   this.userdetails = val;
    // });
  }

  // onLogout() {
  //   // this.ionicStorage.remove('token').then(val => {
  //   //   this.ionicStorage.remove('vehicleToken');
  //   //   this.router.navigateByUrl("/login");
  //   // })

  //   this.authService.logout();
  //   // this.auth.logout();
  // }

  onLogout() {
    // debugger
    // this.token = localStorage.getItem("DEVICE_TOKEN");
    // var pushdata = {};
    // if (this.platform.is('android')) {
    //   pushdata = {
    //     "uid": this.userdetails._id,
    //     "token": this.token,
    //     "os": "android"
    //   }
    // } else {
    //   pushdata = {
    //     "uid": this.userdetails._id,
    //     "token": this.token,
    //     "os": "ios"
    //   }
    // }

    this.alertCtrl.create({
      message: 'Do you want to logout from the application?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          localStorage.clear();
          localStorage.setItem('count', null)
          this.ionicStorage.clear().then(() => {
            console.log("ionic storage cleared!")
          })
          // this.navCtrl.setRoot('LoginPage');
          this.authService.logout();
          // this.loadingController.create({
          //   message: "please wait..."
          // }).then((loadEl) => {
          //   loadEl.present();
          //   var url = "https://www.oneqlik.in/users/PullNotification";
          //   this.apiCall.getServiceWithParams(url, pushdata)
          //     .subscribe(data => {
          //       loadEl.dismiss();
          //       // this.apiCall.stopLoading();
          //       console.log("push notifications updated " + JSON.parse(JSON.stringify(data)).message)
          //       localStorage.clear();
          //       localStorage.setItem('count', null)
          //       this.ionicStorage.clear().then(() => {
          //         console.log("ionic storage cleared!")
          //       })
          //       // this.navCtrl.setRoot('LoginPage');
          //       this.authService.logout();
          //     },
          //       err => {
          //         loadEl.dismiss();
          //         console.log(err)
          //       });
          // })

        }
      },
      {
        text: 'No',
        handler: () => {
          // this.menuCtrl.close();
        }
      }]
    }).then((alertEl) => {
      alertEl.present()
    })
    // alert.present();
  }

  onClick() {
    this.router.navigateByUrl("/maintabs/tabs/settings/configure");
  }
  cpassword() {
    this.router.navigateByUrl("/maintabs/tabs/settings/change-password");
  }
  support() {
    this.router.navigateByUrl('/maintabs/tabs/settings/support');
  }
}
