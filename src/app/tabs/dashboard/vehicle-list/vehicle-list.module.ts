import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VehicleListPage } from './vehicle-list.page';
import { GeocoderAPIService } from 'src/app/geocoder-api.service';
import { OnCreateDirective } from './on-create.directive';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

const routes: Routes = [
  {
    path: '',
    component: VehicleListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VehicleListPage, OnCreateDirective],
  providers: [GeocoderAPIService, NativeGeocoder]
})
export class VehicleListPageModule { }
