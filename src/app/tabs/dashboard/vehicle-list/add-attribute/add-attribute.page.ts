import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.page.html',
  styleUrls: ['./add-attribute.page.scss'],
})
export class AddAttributePage implements OnInit {
  vehicleData: any;
  constructor(
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private apiCall: AppService,
    private ionStorage: Storage,
    private navCtrl: NavController
  ) {
    if (this.route.snapshot.data['vehicle_data']) {
      this.vehicleData = this.route.snapshot.data['vehicle_data'];
      // this.device_type = this.data.device_model.device_type;
      console.log("parammap: ", this.vehicleData)
    }
  }
  selectedName: any; value: number;
  ngOnInit() {
  }
  userToken: any;
  ionViewDidEnter() {
    this.ionStorage.get('token').then((val) => {
      this.userToken = val;
    })
  }

  addAttribute() {
    if (this.selectedName === undefined || this.value === undefined) {
      this.toastCtrl.create({
        message: 'Please fill all required field and try again...',
        duration: 2000,
        position: 'middle'
      }).then((toastEl) => {
        toastEl.present();
      })
      return;
    }
    // if(this.vehicleData.attributes) {
    //   var temp =  this.vehicleData.attributes.this.selectedName
    // }
    // debugger
    this.vehicleData.attributes = {
      [this.selectedName]: this.convertNumberIntoKmperhour(this.value)
    }
    let payload = this.vehicleData;
    let url = "/api/devices/" + this.vehicleData.id;
    this.apiCall.putService123(url, this.userToken, payload).subscribe((response) => {
      let res = JSON.parse(JSON.stringify(response));
      this.toastCtrl.create({
        message: 'Attributed added successfully...!',
        duration: 2000,
        position: 'middle'
      }).then((toastEl) => {
        toastEl.present();
      })

      setTimeout(() => {
        this.navCtrl.pop();
      }, 2000);
    },
      err => {
        console.log("error: ", err);
      })

  }

  convertNumberIntoKmperhour(valNum) {
    return valNum*1.609344
  }

}
