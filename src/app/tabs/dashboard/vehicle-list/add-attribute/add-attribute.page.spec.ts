import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttributePage } from './add-attribute.page';

describe('AddAttributePage', () => {
  let component: AddAttributePage;
  let fixture: ComponentFixture<AddAttributePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttributePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttributePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
