import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  LatLngBounds,
  LatLng,
  Spherical
} from '@ionic-native/google-maps/ngx';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { Platform, ToastController, LoadingController, Events, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  userdetails: any;
  @ViewChild('map', {
    static: true
  }) element: ElementRef;
  map: GoogleMap;
  paramData: any;
  target: number;
  datetimeStart: any;
  datetimeEnd: any;
  hideplayback: boolean = false;
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  trackerName: any;
  data2: any;
  dataArrayCoords: any[];
  mapData: any[];
  playing: boolean;
  coordreplaydata: any;
  startPos: any[];
  speed: number;
  speedMarker: any;
  updatetimedate: any;
  allData: any = {};

  constructor(
    private route: ActivatedRoute,
    public googleMaps: GoogleMaps,
    public appService: AppService,
    public elementRef: ElementRef,
    private ionicStorage: Storage,
    public plt: Platform,
    private ngZone: NgZone,
    private toastController: ToastController,
    private loadingCtrl: LoadingController,
    public events: Events,
    private alertCtrl: AlertController,
    private datePipe: DatePipe
  ) {
    this.hideplayback = false;
    this.target = 0;
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
    if (this.route.snapshot.data['hist']) {
      this.paramData = this.route.snapshot.data['hist'];
      // console.log("parammap history: ", this.paramData)
    }
  }

  ngOnInit() {
    // console.log("parammap history: ", this.paramData)
  }

  ngOnDestroy() {
    localStorage.removeItem("markerTarget");
  }

  ionViewWillEnter() {
    localStorage.removeItem("markerTarget");
    this.plt.ready();
    // this.ngZone.run(() => this.initMap());
  }

  ionViewDidEnter() {
    // localStorage.removeItem("markerTarget");
    // this.plt.ready();
    this.ngZone.run(() => this.initMap());
    this.allData.flag2 = 'init';

    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      console.log("parammap history inside ionview: ", this.paramData)

      this.userdetails = val;
      this.trackerId = this.paramData.id;
      this.trackerType = (this.paramData.category ? this.paramData.category : 'car');
      this.DeviceId = this.paramData.uniqueId;
      this.trackerName = this.paramData.name;
      this.maphistory();
    });
  }

  initMap() {
    if (this.map === undefined) {
      this.map = GoogleMaps.create(this.element.nativeElement);
      this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => { });
    }
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
    var h = absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    if (h === 0) {
      return m + 'min';
    } else {
      if (m === '00') {
        return '0min';
      } else {
        return h + 'h ' + m + 'min';
      }
    }


  }

  onClick() {
    if (this.map) {
      this.map.clear();
    }
    this.initMap();
    this.maphistory();
  }

  maphistory() {
    this.data2 = [];
    if (new Date(this.datetimeEnd).toISOString() >= new Date(this.datetimeStart).toISOString()) {

    } else {
      this.toastController.create({
        message: 'To time always greater than From Time',
        duration: 1500,
        position: 'bottom'
      }).then((toastEl => {
        toastEl.present();
      }));
      return false;
    }
    var burl = '/api/positions?deviceId=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Fetching history data..'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService123(burl, this.userdetails)
        .subscribe(data => {
          loadingEl.dismiss();
          var respData = JSON.parse(JSON.stringify(data));
          if (respData.length === 0) {
            this.alertCtrl.create({
              header: 'No Data Found',
              message: 'Vehicle has not moved from ' + this.datePipe.transform(new Date(this.datetimeStart), 'medium') + ' to ' + this.datePipe.transform(new Date(this.datetimeEnd), 'medium'),
              buttons: [{
                text: 'Okay',
                handler: () => {
                  this.hideplayback = false;
                }
              }]
            }).then((alertEl) => {
              alertEl.present();
            });
            return;
          }
          this.getSummaryData();
          this.data2 = respData;
          this.hideplayback = true;
          this.gps(this.data2);
        },
          err => {
            loadingEl.dismiss();
          });
    });
  }
  summaryData: any;
  getSummaryData() {
    let _burl = '/api/reports/summary?deviceId=' + this.trackerId + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
    this.appService.getService123(_burl, this.userdetails)
      .subscribe(data => {
        let res = JSON.parse(JSON.stringify(data));
        this.summaryData = res[0];
      },
        err => {
          console.log(err);
        })
  }

  changeSpeed(key) {
    console.log(key);
    let that = this;

    if (key === 'high') {
      that.speed = 3 * 100;
    } else if (key === 'low') {
      that.speed = 1 * 100;
    }
  }

  meterIntoKelometer(value) {
    var km = value / 1000;
    return km.toFixed(1);
  }
  latlongObjArr: any = [];
  gps(data3) {

    let that = this;
    that.latlongObjArr = data3;
    that.dataArrayCoords = [];
    var cumulativeDistance = 0;
    for (var i = 0; i < that.latlongObjArr.length; i++) {
      if (that.latlongObjArr[i].latitude && that.latlongObjArr[i].longitude) {
        var arr = [];
        var startdatetime = new Date(that.latlongObjArr[i].deviceTime);
        arr.push(that.latlongObjArr[i].latitude);
        arr.push(that.latlongObjArr[i].longitude);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": that.latlongObjArr[i].speed });
        // debugger
        cumulativeDistance = cumulativeDistance + Number(that.meterIntoKelometer(that.latlongObjArr[i].attributes.distance));
        // if (that.latlongObjArr[i].attributes) {
        //   if (i === 0) {
        //     cumulativeDistance += 0;
        //   } else {
        //     cumulativeDistance += that.latlongObjArr[i].attributes.distance ? parseFloat(that.latlongObjArr[i].attributes.distance) : 0;
        //   }
        //   that.latlongObjArr[i].attributes.distance = (cumulativeDistance);
        //   arr.push({ "cumu_dist": that.latlongObjArr[i].attributes.distance });
        // }
        // else {
        that.latlongObjArr[i].attributes.distance = cumulativeDistance;
        arr.push({ "cumu_dist": that.latlongObjArr[i].attributes.distance });
        // }
        that.dataArrayCoords.push(arr);
      }
    }

    that.mapData = [];
    that.mapData = data3.map(function (d) {
      return { lat: d.latitude, lng: d.longitude };
    })
    that.mapData.reverse();

    let bounds = new LatLngBounds(that.mapData);

    that.map.moveCamera({
      target: bounds
    })
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
      (data) => {
        console.log('Click MAP');

        // that.drawerHidden1 = true;
      });

    that.map.addMarker({
      // title: 'D',
      position: that.mapData[0],
      icon: {
        url: './assets/imgs/redFlag.png',
        size: {
          height: 40,
          width: 40
        }
      },
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();
      that.map.addMarker({
        // title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: {
          url: './assets/imgs/greenFlag.png',
          size: {
            height: 40,
            width: 40
          }
        },
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })
  }

  Playback() {
    let that = this;
    // that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playing = !that.playing; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    that.startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playing) {
      that.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.allData.mark == undefined) {
        var icicon;
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        that.map.addMarker({
          icon: {
            url: icicon,
            size: {
              width: 20,
              height: 40
            }
          },
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(that.startPos[0], that.startPos[1]),
        }).then((marker: Marker) => {
          that.allData.mark = marker;
          that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
        });
      } else {
        that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
      }
    } else {
      that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
    }
  }

  play2() {
    let that = this;
    that.allData.speed = 50;
    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];
    if (that.allData.flag2 == 'init') {
      if (that.allData.mark == undefined) {
        var icicon;
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        that.map.addMarker({
          icon: {
            url: icicon,
            size: {
              width: 20,
              height: 40
            }
          },
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(lat, lng),
        }).then((marker: Marker) => {
          that.allData.mark = marker;

          that.animateMarker2(that.map, that.allData.mark, null, that.dataArrayCoords);
          that.allData.flag2 = 'stop';
        });
      } else {
        that.allData.mark.setPosition({ lat: lat, lng: lng });
        that.map.setCameraTarget({ lat: lat, lng: lng });
        that.animateMarker2(that.map, that.allData.mark, null, that.dataArrayCoords);
        that.allData.flag2 = 'stop';
      }

    } else
      if (that.allData.flag2 == 'start') {
        that._moveMarker2();
        that.allData.flag2 = 'stop';
      } else
        if (that.allData.flag2 == 'stop') {
          //  dmap.speed = 0;
          clearTimeout(that.allData.start2);
          that.allData.flag2 = 'start';
        }
    if (that.allData.flag2 == 'reset') {
      console.log("flag2 is: ", that.allData.flag2);
      console.log("check reset coords: " + that.dataArrayCoords[0][0])
      that.allData.mark.setPosition({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
      that.map.setCameraTarget({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
      // that.seekBarValue = 0;
      clearTimeout(that.allData.start2);
      that.allData.flag2 = 'init';
    }
    return that.allData.flag2;
  };

  _goToPoint2: () => void;
  _moveMarker2: () => void;
  cumu_distance: number = 0;
  animateMarker2(map, mark, icons, coords) {
    let that = this;
    that.cumu_distance = 0;
    that.allData.speed = 50;

    that.allData.delay = 10;
    if (that.allData.start2)
      clearTimeout(that.allData.start2);
    var target = 0;
    that._goToPoint2 = function () {
      if (that.speed) {
        that.allData.speed = that.speed;
      }
      ///////////////////////////////////////////////
      // if (that.rangeDetector === true) {
      //   a = that.indexValue;
      //   target = that.indexValue;
      //   that.sliderValue = that.indexValue;
      // }
      ///////////////////////////////////////////////
      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;
      var step = (that.allData.speed * 1000 * that.allData.delay) / 3600000; // in meters

      if (coords[target] === undefined) {
        if (that.allData.start2)
          clearTimeout(that.allData.start2);

        that.allData.flag2 = 'init';
        // that.sliderValue = 0;
        return;
      }
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;
      function changeMarker(mark, deg) {
        // console.log("check marker: ", mark)
        if (Number.isNaN(parseInt(deg))) {
          // console.log("check degree: " + parseInt(deg))
        } else {
          // console.log("check not: " + parseInt(deg))
          if (mark) {
            mark.setRotation(deg);
          }
        }
      }
      that._moveMarker2 = function () {
        var head;
        // that.sliderValue = a;
        that.cumu_distance = coords[target][4].cumu_dist;

        console.log("cumulative distance: ", that.cumu_distance);
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng))
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget({ lat: lat, lng: lng });
          // that.getAddress(lat, lng);
          that.allData.start2 = setTimeout(that._moveMarker2, that.allData.delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(dest);
          map.setCameraTarget(dest);
          // that.getAddress(dest.lat, dest.lng);
          target++;
          if (target == coords.length) {
            that.allData.flag2 = 'reset';
            clearTimeout(that.allData.start2);
          }
          that.allData.start2 = setTimeout(that._goToPoint2, that.allData.delay);
        }
      }
      a++

      // that.rangeDetector = false;
      console.log("aaaaaaaaaaaaaaaaa " + a);
      console.log("coords length " + coords.length);
      if (a > coords.length) {
        console.log("inside this aaaaaaaaaaaaaaaaa " + a);
      }
      else {
        console.log("coords target: ", coords[target])
        that.speedMarker = (coords[target][3].speed).toFixed(2);
        that.updatetimedate = coords[target][2].time;
        // that.cumu_distance = coords[target][5].cumu_dist;
        // that.battery = coords[target][6].battery;

        console.log('move marker running')
        that._moveMarker2();
      }
    }
    var a = 0;
    that._goToPoint2();
  }

  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    that.events.subscribe("SpeedValue:Updated", (sdata) => {
      speed = sdata;
    })
    var target = target;
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget(new LatLng(lat, lng))
          setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(dest);
          map.setCameraTarget(dest);
          target++;
          setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        that.speedMarker = (coords[target][3].speed).toFixed(2);
        that.updatetimedate = coords[target][2].time;

        if (that.playing) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }
}
