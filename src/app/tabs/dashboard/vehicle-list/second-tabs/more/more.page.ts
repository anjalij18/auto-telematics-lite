import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { LoadingController, ModalController } from '@ionic/angular';
@Component({
  selector: 'app-more',
  templateUrl: './more.page.html',
  styleUrls: ['./more.page.scss'],
})
export class MorePage implements OnInit {
  data: any;
  from: any;
  to: any;
  constructor(
    private appService: AppService,
    private ionStorage: Storage,
    public modalController: ModalController,
    private loadingController: LoadingController
  ) {
    this.from = moment({ hours: 0 }).format();
    // console.log("start date", this.datetimeStart);
    this.to = moment().format();
    // console.log("stop date", this.to);
    if (localStorage.getItem("CurrentDevice") !== null) {

      this.data = JSON.parse(localStorage.getItem("CurrentDevice"));
      console.log("check notifications data: ", this.data);
    }
  }

  ngOnInit() { }
  userToken: any;
  ionViewDidEnter() {
    this.ionStorage.get('token').then(val => {
      this.userToken = val;
      let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString();
      this.getEventsReport(_bUrl);
    })
  }

  getEventsReport(_bUrl) {
    this.callInnerFunction(_bUrl);
  }

  eventsData: any = [];
  callInnerFunction(_bUrl) {
    let that = this;
    let _SUrl = '/api/session';
    this.appService.getCookieWithSession(_SUrl, this.userToken)
      .subscribe((response: Response) => {
        let res = JSON.parse(JSON.stringify(response));
        console.log("session res: ", res);
        that.loadingController.create({
          message: 'Loading alerts...'
        }).then((loadEl) => {
          loadEl.present();
          that.appService.getReportData(_bUrl, that.userToken).subscribe((response: Response) => {
            loadEl.dismiss();
            let res = JSON.parse(JSON.stringify(response));
            console.log("check report data: ", res);
            that.eventsData = [];
            that.eventsData = res.reverse();
          },
            err => {
              loadEl.dismiss();
              console.log("getting error from reports", err)
            });
        })

      },
        err => {
          console.log("session err: ", err);
        });
  }

  splitString(str) {
    const name = str;
    const nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1)
    let str1 = nameCapitalized.match(/[A-Z][a-z]+/g);
    return str1[0] + ' ' + str1[1];
  }

  showFilter() {
    this.modalController.create({
      component: ModalFilterPage,
      cssClass: 'my-custom-class'
    }).then((modalEl) => {
      modalEl.present();
      // let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString();

      modalEl.onDidDismiss().then((data) => {
        debugger
        console.log("check modal data: ", data.data.data.data.length);

        // if (data.data.data.datefrom !== undefined) {
        let _bUrl = '/api/reports/events?deviceId=' + this.data.id + '&from=' + new Date(data.data.data.datefrom ? data.data.data.datefrom : this.from).toISOString() + '&to=' + new Date(data.data.data.dateto ? data.data.data.dateto : this.to).toISOString();

        // }
        if (data.data.data.data.length > 0) {
          for (var i = 0; i < data.data.data.data.length; i++) {
            _bUrl += '&type=' + data.data.data.data[i];
          }
        }
        this.getEventsReport(_bUrl);
      })
    });

  }

}

@Component({
  selector: 'modal-page',
  templateUrl: 'modal-filter.html',
  styles: [`
 
  `]
})
export class ModalFilterPage {
  selectedAlertType: any;
  datetimeStart: any;
  datetimeEnd: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  constructor(
    private modalController: ModalController
  ) { }

  dismiss(data) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true,
      'data': data
    });
  }

  callThis(data) {
    this.dismiss(data);
  }

  applyFilter() {
    let data = {
      data: this.selectedAlertType,
      datefrom: this.datetimeStart,
      dateto: this.datetimeEnd
    }
    this.dismiss(data);
  }

}
