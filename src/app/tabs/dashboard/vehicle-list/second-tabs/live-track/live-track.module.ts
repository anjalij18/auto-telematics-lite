import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import { GoogleMaps } from '@ionic-native/google-maps/ngx';

import { IonicModule } from '@ionic/angular';

import { LiveTrackPage } from './live-track.page';
import { GeocoderAPIService } from 'src/app/geocoder-api.service';

const routes: Routes = [
  {
    path: '',
    component: LiveTrackPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LiveTrackPage],
  providers: [GeocoderAPIService, DatePipe]
})
export class LiveTrackPageModule {}
