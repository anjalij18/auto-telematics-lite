import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapsMapTypeId, LatLng, Spherical, ILatLng, Circle, Polyline } from '@ionic-native/google-maps/ngx';
import * as _ from "lodash";
// import { DrawerState } from 'ion-bottom-drawer';
import { URLs } from 'src/app/app.model';
import { Storage } from '@ionic/storage';
import { Platform, LoadingController, Events, AlertController, ToastController } from '@ionic/angular';
import { GeocoderAPIService } from 'src/app/geocoder-api.service';
import { DatePipe } from '@angular/common';

declare var cookieMaster: any;
declare var CordovaWebsocketPlugin: any;

@Component({
  selector: 'app-live-track',
  templateUrl: './live-track.page.html',
  styleUrls: ['./live-track.page.scss'],
})
export class LiveTrackPage implements OnInit {
  data: any;
  @ViewChild('map123', {
    static: true
  }) element: ElementRef;
  map123: GoogleMap;

  // shouldBounce = true;
  // dockedHeight = 52;
  // distanceTop = 150;
  // drawerState = DrawerState.Docked;
  // states = DrawerState;
  // minimumHeight = 52;
  userdetails: any;
  socketChnl: any = [];
  allData: any = {};
  socketSwitch: {};
  impkey: any;
  deviceDeatils: any = {};
  loading: any;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  clicked: boolean = false;
  messages: string;
  deviceConfigStatus: any;
  intervalID: any;
  device_type: any;
  server_ip: any;
  webSocketId: any;
  vehicle_speed: any = 0.00;
  newVariable: any;

  constructor(
    private route: ActivatedRoute,
    private toastController: ToastController,
    // private router: Router,
    public googleMaps: GoogleMaps,
    public appService: AppService,
    public elementRef: ElementRef,
    private ionicStorage: Storage,
    public plt: Platform,
    private constURL: URLs,
    // private socket: Socket,
    private loadingCtrl: LoadingController,
    private ngZone: NgZone,
    public events: Events,
    private alertController: AlertController,
    private geocoderApi: GeocoderAPIService,
    private datePipe: DatePipe
    // private http: HttpClient,
  ) {
    if (this.route.snapshot.data['special']) {
      this.data = this.route.snapshot.data['special'];
      // this.device_type = this.data.device_model.device_type;
      console.log("parammap: ", this.data)
    }
    this.server_ip = localStorage.getItem("server_ip");
  }

  ngOnInit() {
    this.allData = {};
    this.socketChnl = [];
    this.socketSwitch = {};
  }
  ngOnDestroy() {
  }

  ionViewWillEnter() {
    console.log("Hello Anjali, I'm here!!")
    this.plt.ready();
    this.ngZone.run(() => this.initMap());
  }

  ionViewWillLeave() {
    // this.socket.disconnect();

  }

  ionViewDidLeave() {
    let that = this;
    CordovaWebsocketPlugin.wsClose(that.webSocketId, 1000, "I'm done!");

  }
  positionsArray: any = [];
  ionViewDidEnter() {
    console.log("Hello Deepa, I'm here!!")
    localStorage.removeItem("SocketFirstTime");
    localStorage.setItem("CurrentDevice", JSON.stringify(this.data));
    // localStorage.setItem("CurrentDevice", this.data);

    // let that = this;
    /////////////////////////////
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userdetails = val;
      this.live(this.data);
      let url = '/api/session';
      this.loadingCtrl.create({
        message: 'Loading...',
        spinner: 'lines'
      }).then((loadEl) => {
        loadEl.present();
        this.appService.getServiceTemp123(url, val).subscribe(
          // this.appService.getService123(url, val).subscribe(
          response => {
            let res = JSON.parse(JSON.stringify(response));
            this.ionicStorage.set("token", res).then(() => {
              this.ionicStorage.get('token').then((val123) => {
                console.log('Your age is', val123);
                this.userdetails = val123;
                /////////////////////////////
                this.sockerConnection(loadEl);
                ///////////////////
              })
            });
          },
          err => {
            loadEl.dismiss();
            console.log(err);
          });
      });
    });
  }

  sockerConnection(loadEl) {
    let that = this;
    this.appService.getSessionwithToken(this.userdetails.token).subscribe((resp: Response) => {
      console.log("intercept works or not ....", resp);
      cookieMaster.getCookieValue(that.server_ip + "/api/session", 'JSESSIONID', function (data) {
        // console.log("we got cookie... " + data.cookieValue);
        let options = {
          url: 'ws://128.199.21.17:8082/api/socket',
          pingInterval: 3000,
          headers: { "Cookie": "JSESSIONID=" + data.cookieValue },
        };
        loadEl.dismiss();
        CordovaWebsocketPlugin.wsConnect(options,
          function (recvEvent) {
            // console.log("Received callback from WebSocket: " + JSON.parse(JSON.stringify(recvEvent.message)));
            if (JSON.parse(recvEvent.message).positions) {
              var a = JSON.parse(recvEvent.message).positions;
              that.positionsArray = a;
              that.furtherCalc(that.positionsArray);
            }

            if (JSON.parse(recvEvent.message).devices) {
              var b = JSON.parse(recvEvent.message).devices;
              that.furtherCalc123(b);
            }

          },
          function (success) {
            console.log("Connected to WebSocket with id: " + success.webSocketId);
            that.webSocketId = success.webSocketId;
          },
          function (error) {
            console.log("Failed to connect to WebSocket: " +
              "code: " + error["code"] +
              ", reason: " + error["reason"] +
              ", exception: " + error["exception"]);
          });

        // var url = 'ws://128.199.21.17:8082/api/socket';
        // var connection = new WebSocket(url, []);
        // // , { 'headers': { 'Cookie': resp.headers['set-cookie'][0] } }
        // connection.onopen = () => {
        //   connection.send('Message From Client')
        // }

        // connection.onerror = (error) => {
        //   console.log('WebSocket error: ${error}', error)
        // }
        // connection.onmessage = (e) => {
        //   console.log("socket message:" + e.data)
        // }
      })
    },
      err => {
        loadEl.dismiss();
        console.log("intercept error ", err)
      })
  }

  furtherCalc123(b) {
    let that = this;
    var i = 0, howManyTimes = b.length;
    function f() {
      if (b[i].id === that.data.id) {
        that.data = b[i];
      }
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }
  count = 0;
  furtherCalc(a) {
    let that = this;
    console.log("inside function")

    for (var t = 0; t < a.length; t++) {
      if (a[t].deviceId === that.data.id) {
        console.log("dfsgdfgfhgjhkjkjkljkjkjkj")
        that.newVariable = (a[t].speed ? Number(a[t].speed) : 0);
        var something = document.getElementById("para");
        something.innerText = (that.newVariable * 1.943844).toFixed(2) + ' km/h';
        console.log("socket positon speed data: " + that.newVariable);
        that.socketInit(a[t]);
      }
    }
  }

  initMap() {
    let style = [];
    let mapOptions = {
      backgroundColor: 'white',
      controls: {
        compass: false,
        zoom: true,
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      styles: style
    }
    if (this.map123 === undefined) {
      console.log(this.element.nativeElement)
      this.map123 = GoogleMaps.create(this.element.nativeElement, mapOptions);
      this.map123.one(GoogleMapsEvent.MAP_READY).then((data: any) => {

      })
    }
  }
  afterClick: boolean = false;
  afterMapTypeClick: boolean = false;
  onTraffic() {
    // debugger
    this.afterClick = !this.afterClick;
    if (this.afterClick) {
      this.map123.setTrafficEnabled(true);
    } else {
      this.map123.setTrafficEnabled(false);
    }
  }

  onMapType() {
    // debugger
    this.afterMapTypeClick = !this.afterMapTypeClick;
    if (this.afterMapTypeClick) {
      this.map123.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      this.map123.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    }
  }

  showSettings() {

  }

  vehData: any = [];
  iconUrl: any;
  // ignitionKey: any;
  live(data) {
    //////////////////////// get data from traccar api /////////////////////////
    let _bUrl = '/api/positions?deviceId=' + data.id;
    this.appService.getService123(_bUrl, this.userdetails).subscribe((response => {
      let res = JSON.parse(JSON.stringify(response));
      console.log("check data response: ", res);
      this.vehData = res;
      // if (this.vehData[0].attributes.ignition !== undefined) {
      //   this.ignitionKey = this.vehData[0].attributes.ignition;
      // }
      console.log("check category: ", this.data.category)
      this.innerFunc(res, data);
    }))
    //////////////////////// get data from traccar api /////////////////////////
  }

  innerFunc(data1, data) {
    let res = data1[0];
    let that = this;
    if (res) {
      if (res.latitude && res.longitude) {
        if (that.plt.is('android')) {

          that.map123.moveCamera({
            target: { lat: res.latitude, lng: res.longitude },
            zoom: 18,
            duration: 2000,
            padding: 0  // default = 20px
          });
          // let icicon;
          // if (data.status === 'online') {
          //   icicon = './assets/imgs/vehicles/runningcar.png';
          // } else {
          //   icicon = './assets/imgs/vehicles/outofreachcar.png';
          // }
          // console.log("selected icons: ", icicon);
        } else {

        }
      } else {
      }
    }
    this.getAddedGeofence();
  }

  onImmo() {
    let that = this;
    let d = that.data;
    if (d.last_ACC != null || d.last_ACC != undefined) {

      if (that.clicked) {
        this.alertController.create({
          message: "Process is already going on. Please wait till complete.",
          buttons: ['Okay']
        }).then(alertEl => {
          alertEl.present();
        })
      } else {
        this.messages = undefined;
        // this.dataEngine = d;
        // this.apiCall.startLoading().present();
        this.loadingCtrl.create({
          message: 'Please wait...'
        }).then(loadEl => {
          loadEl.present();
          this.subFunc(d, loadEl);
        })

      }
    }

  }
  commandTypes: any;
  onImmo123() {
    let _bUrl = "/api/commands/send?deviceId=" + this.data.id;
    this.appService.getService123(_bUrl, this.userdetails).subscribe((response: Response) => {
      let res = JSON.parse(JSON.stringify(response));
      console.log("check commands: ", res);
      this.commandTypes = res;
      this.presentAlertRadio();
    },
      err => {
        console.log("got error: ", err);
      })
  }

  getAddedGeofence() {
    let that = this;
    let _url = '/api/geofences?deviceId=' + this.data.id;
    this.appService.getService123(_url, this.userdetails).subscribe((response: Response) => {
      let addedGeofences = JSON.parse(JSON.stringify(response));
      if (addedGeofences.length > 0) {
        that.drawCircle(addedGeofences);
      }
    },
      err => {
        console.log("error: ", err);
      })
  }

  drawCircle(a) {
    let that = this;

    var i = 0, howManyTimes = a.length;
    function f() {
      if (a[i].id) {
        let str = a[i].area;
        var res = str.split(/(?:,| )+/);

        let str1 = res[1];
        var res1 = str1.split("(");

        var str2 = res[3];
        var res2 = str2.split(")");

        let ltln: ILatLng = { "lat": res1[1], "lng": res[2] };
        let circle: Circle = that.map123.addCircleSync({
          'center': ltln,
          'radius': res2[0],
          'strokeColor': '#7044ff',
          'strokeWidth': 2,
          'fillColor': 'rgb(112, 68, 255, 0.5)'
        });
      }
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }
  geofences: any = [];
  addGeofence() {
    let _url = '/api/geofences?deviceId=' + this.data.id;
    this.appService.getService123(_url, this.userdetails).subscribe((response: Response) => {
      let addedGeofences = JSON.parse(JSON.stringify(response));

      var _bUrl = '/api/geofences';
      this.appService.getService123(_bUrl, this.userdetails).subscribe(resp => {
        console.log("geofences: " + resp);
        this.geofences = JSON.parse(JSON.stringify(resp));
        // console.log(this.geofences);

        let alertOptions = []; let alertOptions123 = [];

        for (var i = 0; i < this.geofences.length; i++) {
          alertOptions.push({
            name: this.geofences[i].name,
            type: 'checkbox',
            label: this.geofences[i].name,
            value: this.geofences[i]
          })
        }
        for (var j = 0; j < addedGeofences.length; j++) {
          alertOptions123.push({
            name: addedGeofences[j].name,
            type: 'checkbox',
            label: addedGeofences[j].name,
            value: addedGeofences[j],
            checked: true
          })
        }
        const result = Object.values(
          [].concat(alertOptions, alertOptions123)
            .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {})
        );

        this.alertController.create({
          header: 'Select Geofence',
          inputs: result,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            }, {
              text: 'Ok',
              handler: (data) => {
                console.log('Confirm Ok', data);
                this.addPermissions(this.userdetails, data);
              }
            }
          ]
        }).then((alertEl) => {
          alertEl.present();
        })
      },
        err => {
          console.log(err);
        });
    });
  }

  addPermissions(userData, selectedData) {
    let url = '/api/permissions';
    let that = this;
    // outerthis.stoppageReport = [];
    var i = 0, howManyTimes = selectedData.length;
    function f() {
      let payload = {
        "deviceId": that.data.id,
        "geofenceId": selectedData[i].id
      }
      that.appService.postService123(url, userData, payload).subscribe((resp: Response) => {
        let res = JSON.parse(JSON.stringify(resp));
        console.log("check permissions: ", res);
      },
        err => {
          console.log("got err: ", err);
        })
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  async presentAlertRadio() {
    let radio_options = [];
    for (let i = 0; i < this.commandTypes.length; ++i) {
      radio_options.push({
        type: 'radio',
        label: this.commandTypes[i].description,
        value: this.commandTypes[i],
        // checked: i === 0
      });
    }

    const alert = await this.alertController.create({
      header: 'Choose command',
      inputs: radio_options,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log("check data: ", data);
            console.log('Confirm Ok');
            this.sendCommand(data);
          }
        }
      ]
    });
    await alert.present();
  }

  sendCommand(data) {
    // debugger
    data.deviceId = this.data.id;
    let url = "/api/commands/send";
    this.appService.postService123(url, this.userdetails, data).subscribe((response: Response) => {
      let res = JSON.parse(JSON.stringify(response));
      console.log("check command res: ", res);
    },
      err => {
        console.log("error: ", err);
      })
  }

  subFunc(d, loadEl) {
    let that = this;
    var baseURLp;
    if (d.device_model.device_type === undefined) {
      baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + this.device_type;

    } else {
      baseURLp = this.constURL.mainUrl + 'deviceModel/getDevModelByName?type=' + d.device_model.device_type;
    }

    this.appService.getService(baseURLp)
      .subscribe(data => {
        loadEl.dismiss();
        // debugger;
        console.log("immo data: ", data)
        this.deviceConfigStatus = data;
        let immobType = data[0].imobliser_type;
        if (d.ignitionLock == '1') {
          this.messages = 'Do you want to unlock the engine?';
        } else {
          if (d.ignitionLock == '0') {
            this.messages = 'Do you want to lock the engine?'
          }
        }
        this.alertController.create({
          message: this.messages,
          buttons: [{
            text: 'YES',
            handler: () => {
              if (immobType == 0 || immobType == undefined) {
                that.clicked = true;
                var devicedetail = {
                  "_id": d._id,
                  "engine_status": !d.engine_status
                }
                const bUrl = this.constURL.mainUrl + 'devices/deviceupdate';
                // this.apiCall.startLoading().present();
                this.appService.getServiceWithParams(bUrl, devicedetail)
                  .subscribe(response => {
                    // this.apiCall.stopLoading();
                    // this.editdata = response;
                    let res = JSON.parse(JSON.stringify(response));
                    this.toastController.create({
                      message: res.message,
                      duration: 2000,
                      position: 'top'
                    }).then(toastEl => {
                      toastEl.present();
                    })
                    // this.responseMessage = "Edit successfully";
                    // this.getdevices();

                    var msg;
                    if (!d.engine_status) {
                      msg = this.deviceConfigStatus[0].resume_command;
                    }
                    else {
                      msg = this.deviceConfigStatus[0].immoblizer_command;
                    }

                    // this.sms.send(d.sim_number, msg);
                    // const toast1 = this.toastCtrl.create({
                    //   message: this.translate.instant('SMS sent successfully'),
                    //   duration: 2000,
                    //   position: 'bottom'
                    // });
                    // toast1.present();
                    that.clicked = false;
                  },
                    error => {
                      that.clicked = false;
                      // this.apiCall.stopLoading();
                      console.log(error);
                    });
              } else {
                console.log("Call server code here!!")
                that.serverLevelOnOff(d);
              }
            }
          },
          {
            text: 'NO'
          }]
        }).then(alertEl => {
          alertEl.present();
        });
      },
        error => {
          loadEl.dismiss();
          // this.apiCall.stopLoading();
          console.log("some error: ", error._body.message);
        });
  }

  serverLevelOnOff(d) {
    let that = this;
    that.clicked = true;
    var data: any = {};
    if (d.device_model.device_type === undefined) {
      data = {
        "imei": d.Device_ID,
        "_id": d._id,
        "engine_status": d.ignitionLock,
        "protocol_type": this.device_type
      }

    } else {
      data = {
        "imei": d.Device_ID,
        "_id": d._id,
        "engine_status": d.ignitionLock,
        "protocol_type": d.device_model.device_type
      }
    }

    let bURL = this.constURL.mainUrl + 'devices/addCommandQueue';
    // this.apiCall.startLoading().present();
    this.appService.getServiceWithParams(bURL, data)
      .subscribe(resp => {
        // this.apiCall.stopLoading();
        console.log("ignition on off=> ", resp)
        let respMsg = JSON.parse(JSON.stringify(resp));
        const urlTel = this.constURL.mainUrl + 'trackRouteMap/getRideStatusApp?_id=' + respMsg._id;
        // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
        this.intervalID = setInterval(() => {
          this.appService.getService(urlTel)
            .subscribe(data => {
              console.log("interval=> " + data)
              let commandStatus = JSON.parse(JSON.stringify(data)).status;

              if (commandStatus == 'SUCCESS') {
                clearInterval(this.intervalID);
                that.clicked = false;
                this.callObjFunc(d);
                // this.apiCall.stopLoadingnw();
                this.toastController.create({
                  message: 'Process has been completed successfully...!!',
                  duration: 1500,
                  position: 'bottom'
                }).then((toastEl => {
                  toastEl.present();
                }));
              }
            })
        }, 5000);
      },
        err => {
          // this.apiCall.stopLoading();
          console.log("error in onoff=>", err);
          that.clicked = false;
        });
  }

  callObjFunc(d) {
    let that = this;
    var _bUrl = this.constURL.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.loadingCtrl.create({
      message: 'Plaese wait...'
    }).then(loadEl => {
      loadEl.present();
      this.appService.getService(_bUrl)
        .subscribe(resp => {
          loadEl.dismiss();
          console.log("updated device object=> " + resp);
          if (!resp) {
            return;
          } else {
            that.data = resp;
          }
        })
    });
    // this.apiCall.startLoading().present();

  }


  isNight() {
    //Returns true if the time is between
    //7pm to 5am
    let time = new Date().getHours();
    return (time > 5 && time < 19) ? false : true;
  }
  recenterMeLat: any;
  recenterMeLng: any;
  reCenterMe() {
    this.map123.animateCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      // zoom: 16,
      tilt: 30,
      // bearing: this.allData.map.getCameraBearing(),
      duration: 1800,
      // setCompassEnabled: false
    }).then(() => {

    })
  }
  address: any;
  getAddress(coordinates) {
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    let tempcord = {
      "lat": coordinates.lat,
      "long": coordinates.long
    }
    this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.address = str;
        // console.log("inside", that.address);
      })
  }

  socketInit(pdata, center = false) {
    this.allData.allKey = [];
    // this.socketChnl.push(pdata.deviceId);
    let that = this;
    if (pdata == undefined) {
      return;
    }
    that.deviceDeatils = pdata;
    // that.vehicle_speed = (that.deviceDeatils.speed).toFixed(2);
    if (pdata.id != undefined && pdata.latitude != undefined && pdata.longitude != undefined) {
      var key = pdata.deviceId;

      // that.vehicle_speed = (pdata.speed).toFixed(2);

      that.recenterMeLat = pdata.latitude;
      that.recenterMeLng = pdata.longitude;
      // if (localStorage.getItem("SocketFirstTime") !== null) {
      //   that.reCenterMe();
      // }
      // that.vehicle_speed = pdata.speed;
      // console.log("check speed: ", that.vehicle_speed)
      if (that.allData[key]) {
        that.socketSwitch[key] = pdata;

        if (that.allData[key].mark !== undefined) {
          // debugger
          that.allData[key].mark.setPosition(that.allData[key].poly[1]);
          that.showTrail(pdata.latitude, pdata.longitude, that.map123);

          that.allData[key].mark.setIcon(that.getIconUrl(pdata));
          var temp = _.cloneDeep(that.allData[key].poly[1]);
          that.allData[key].poly[0] = _.cloneDeep(temp);
          that.allData[key].poly[1] = { lat: pdata.latitude, lng: pdata.longitude };
          let coordinates = {
            lat: pdata.latitude,
            long: pdata.longitude
          }
          that.getAddress(coordinates);
          // var speed = data.status == "RUNNING" ? data.last_speed : 0;
          // that.liveTrack(that.map123, that.allData[key].mark, that.getIconUrl(pdata), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, that.data.category, that.data.status, that);
          that.liveTrack(that.map123, that.allData[key].mark, that.allData[key].poly, parseFloat(pdata.speed), 150, center, pdata.deviceId, that);
        }
      } else {
        that.allData[key] = {};
        // that.socketSwitch[key] = pdata;
        that.allData.allKey.push(key);
        that.allData[key].poly = [];
        that.allData[key].poly.push({ lat: pdata.latitude, lng: pdata.longitude });
        that.showTrail(that.allData[key].poly[0].lat, that.allData[key].poly[0].lng, that.map123);

        that.map123.addMarker({
          title: that.data.name,
          position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
          icon: {
            url: that.getIconUrl(pdata),
            size: {
              height: 40,
              width: 20
            }
          },
        }).then((marker: Marker) => {

          that.allData[key].mark = marker;
          ///////////////////////////////
          localStorage.setItem("SocketFirstTime", "SocketFirstTime");
          marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
            .subscribe(e => {
              that.getAddressTitle(marker);

            });
          let coordinates = {
            lat: pdata.latitude,
            long: pdata.longitude
          }
          that.getAddress(coordinates);
          that.liveTrack(that.map123, that.allData[key].mark, that.allData[key].poly, parseFloat(pdata.speed), 150, center, pdata.deviceId, that);
          // that.liveTrack(that.map123, that.allData[key].mark, that.getIconUrl(pdata), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, that.data.category, that.data.status, that);
        });
      }
    }
  }
  tempArray: any = [];
  showTrail(lat, lng, map) {
    debugger
    let that = this;
    that.tempArray.push({ lat: lat, lng: lng });

    if (that.tempArray.length === 2) {
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.polylineID.push(polyline);
      });
    } else if (that.tempArray.length > 2) {
      that.tempArray.shift();
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.allData.polylineID.push(polyline);
      });
    }
  }

  getIconUrl(pdata) {
    let that = this;
    var iconUrl;
    if (that.data.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/stopped' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/idling' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/stopped' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (that.data.category ? that.data.category : 'car') + '.png';
    } else if (that.data.status === 'offline' || that.data.status === 'unknown') {
      iconUrl = './assets/imgs/vehicles/offline' + (that.data.category ? that.data.category : 'car') + '.png';
    }
    console.log("check status: ", that.data.status)
    console.log("check ignition: ", pdata.attributes.ignition)
    console.log("check motion: ", pdata.attributes.motion)
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }

  getAddressTitle(marker) {
    let that = this;
    this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        // debugger
        let snippetString = `Speed - ${Math.round(that.deviceDeatils.speed * 1.943844)} \nLast Update - ${that.datePipe.transform(new Date(that.data.lastUpdate).toISOString(), 'medium')}\nAddress - ${str}`;
        marker.setSnippet(snippetString);
      })
  }
  liveTrack(map, mark, coords, speed, delay, center, id, that) {
    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);
    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {
        // debugger
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {

          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          map.setCameraTarget(dest);
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }
  // liveTrack(map, mark, coords, speed, delay, center, id, that) {
  //   var target = 0;

  //   clearTimeout(that.ongoingGoToPoint[id]);
  //   clearTimeout(that.ongoingMoveMarker[id]);
  //   if (center) {
  //     map.setCameraTarget(coords[0]);
  //   }

  //   function _goToPoint() {
  //     if (target > coords.length)
  //       return;

  //     var lat = mark.getPosition().lat;
  //     var lng = mark.getPosition().lng;

  //     var step = (speed * 1000 * delay) / 3600000; // in meters
  //     if (coords[target] == undefined)
  //       return;

  //     var dest = new LatLng(coords[target].lat, coords[target].lng);
  //     var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
  //     var numStep = distance / step;
  //     var i = 0;
  //     var deltaLat = (coords[target].lat - lat) / numStep;
  //     var deltaLng = (coords[target].lng - lng) / numStep;

  //     function changeMarker(mark, deg) {
  //       mark.setRotation(deg);
  //       // mark.setIcon(icons);
  //     }
  //     function _moveMarker() {
  //       // debugger
  //       lat += deltaLat;
  //       lng += deltaLng;
  //       i += step;
  //       var head;
  //       if (i < distance) {

  //         head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
  //         head = head < 0 ? (360 + head) : head;
  //         if ((head != 0) || (head == NaN)) {
  //           changeMarker(mark, head);
  //         }

  //         map.setCameraTarget(new LatLng(lat, lng));
  //         that.latlngCenter = new LatLng(lat, lng);
  //         mark.setPosition(new LatLng(lat, lng));
  //         that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
  //       }
  //       else {
  //         head = Spherical.computeHeading(mark.getPosition(), dest);
  //         head = head < 0 ? (360 + head) : head;
  //         if ((head != 0) || (head == NaN)) {
  //           changeMarker(mark, head);
  //         }

  //         map.setCameraTarget(dest);
  //         that.latlngCenter = dest;
  //         mark.setPosition(dest);
  //         target++;
  //         that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
  //       }
  //     }
  //     _moveMarker();
  //   }
  //   _goToPoint();
  // }

}
