import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { AppService } from "src/app/app.service";
import { AlertController, ToastController } from "@ionic/angular";
import { URLs } from "src/app/app.model";
import { DataService } from "src/app/services/data.service";
import { GeocoderAPIService } from 'src/app/geocoder-api.service';

@Component({
  selector: "app-vehicle-list",
  templateUrl: "./vehicle-list.page.html",
  styleUrls: ["./vehicle-list.page.scss"]
})
export class VehicleListPage implements OnInit {
  segment: string = "all";
  userToken: any;
  stat: any;
  pageNo: number = 0;
  limit: number = 6;
  vehicleList: any = [];
  infiniteScroll: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private ionicStorage: Storage,
    private appService: AppService,
    private alertController: AlertController,
    private toastController: ToastController,
    private dataService: DataService, // public events: Events
    private geocoderApi: GeocoderAPIService
  ) { }

  ionViewWillEnter() {

  }
  ionViewDidEnter() {
    this.ionicStorage.get("token").then(val => {
      this.userToken = val;
      console.log("user token: ", this.userToken);
      if (this.activatedRoute.snapshot.paramMap.get('id')) {
        console.log("activated route param map: ", this.activatedRoute.snapshot.paramMap.get('id'));
        const idKey = this.activatedRoute.snapshot.paramMap.get('id');
        this.segment = idKey;
        this.getVehicleList();
      } else {
        this.segment = 'all';
        this.getVehicleList();
      }

    });
  }

  ngOnInit() {

  }

  doInfinite(infiniteScroll) {
    this.infiniteScroll = infiniteScroll;
    this.pageNo = this.pageNo + 1;
    setTimeout(() => {
      // this.getVehicleList();
      this.getVehicleList()
    }, 500);
  }

  onSegmentChange(ev: any) {
    // console.log(ev);
    // debugger
    let stateName = ev.detail.value;
    this.segment = stateName;
    this.stat = undefined;
    this.pageNo = 0;
    this.limit = 6;
    // this.vehicleList = [];
    this.infiniteScroll = undefined;
    this.getVehicleList();
    // let tempArray = [];
    // for (var h = 0; h < this.vehicleList123.length; h++) {
    //   if (stateName === 'moving') {
    //     if (this.vehicleList123[h].status === 'online') {
    //       tempArray.push(this.vehicleList123[h]);
    //     }
    //   } else if (stateName === 'out_of_reach') {
    //     if (this.vehicleList123[h].status === 'offline') {
    //       tempArray.push(this.vehicleList123[h]);
    //     }
    //   } else if (stateName === 'all' || stateName === 'stopped') {
    //     tempArray.push(this.vehicleList123[h]);
    //   }
    // }
    // this.vehicleList = tempArray;
    // this.getVehicleList();
  }
  vehicleList123: any = [];
  getVehicleList() {
    let _bUrl = '/api/positions';

    this.appService.getService123(_bUrl, this.userToken).subscribe((response: Response) => {
      let res111 = JSON.parse(JSON.stringify(response));
      let baseURLp = '/api/devices?entityId=' + this.userToken.id;
      if (!this.infiniteScroll) {
        this.appService.getService123(baseURLp, this.userToken).subscribe(
          resp => {
            console.log("vehicle list: ", resp);
            let res123 = JSON.parse(JSON.stringify(resp));
            let res = [];

            for (var i = 0; i < res123.length; i++) {
              for (var j = 0; j < res111.length; j++) {
                if (res111[j].deviceId === res123[i].id) {
                  res123[i].attributes = res111[j].attributes;
                }
              }
            }

            if (this.segment === "moving") {
              for (var t = 0; t < res123.length; t++) {
                if (res123[t].status === 'online') {
                  res.push(res123[t]);
                }
              }
            } else if (this.segment === "stopped") {
              res = res123;
            } else if (this.segment === "out_of_reach") {
              for (var t = 0; t < res123.length; t++) {
                if (res123[t].status === 'offline') {
                  res.push(res123[t]);
                }
              }
            } else {
              res = res123;
            }
            // debugger
            this.vehicleList = [];
            this.vehicleList = res;
            this.vehicleList123 = res;
          },
          err => {
            this.toastController
              .create({
                message: "Something went wrong. Please try after some time..",
                position: "bottom",
                duration: 1500
              })
              .then(toastEl => {
                toastEl.present();
              });
          });
      } else {
        this.appService.getService(baseURLp).subscribe(
          resp => {
            let that = this;
            var data = JSON.parse(JSON.stringify(resp)).devices;
            for (let i = 0; i < data.length; i++) {
              that.vehicleList.push(data[i]);
              //console.log("vehicle list length: ", that.vehicleList.length);
            }
            this.infiniteScroll.target.complete();
          },
          err => {
            this.toastController
              .create({
                message: "Something went wrong. Please try after some time..",
                position: "bottom",
                duration: 1500
              })
              .then(toastEl => {
                toastEl.present();
              });
            this.infiniteScroll.target.complete();
          });
      }
    },
      err => {
        console.log("error: ", err);
      });
  }

  onVehDetail(veh) {
    this.ionicStorage.set("vehicleToken", veh);
    this.dataService.setData(42, veh);
    this.router.navigateByUrl(
      "/maintabs/tabs/vehicle-list/second-tabs/second-main-tabs/live-track/42"
    );
  }

  setAttribute(veh) {
    // async presentAlertRadio() {
    this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Radio',
      inputs: [
        {
          name: 'Set Attribute',
          type: 'radio',
          label: 'Set Attribute',
          value: 'attribute'
        },
        {
          name: 'Notifications',
          type: 'radio',
          label: 'Notifications',
          value: 'notification'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            debugger
            if (data === 'attribute') {
              this.dataService.setData(45, veh);
              this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-attribute/45");
            } else {
              if (data === 'notification') {
                this.callOther(veh);
              }
            }
          }
        }
      ]
    }).then((alertEl) => {
      alertEl.present();
    })
    // }

  }

  callOther(veh) {
    let alertOptions = [];
    // let url = '/api/notifications?deviceId=' + veh.id;
    let url = '/api/notifications';
    this.appService.getService123(url, this.userToken).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      console.log("check response: ", response);

      let url2 = '/api/notifications?deviceId=' + veh.id;
      this.appService.getService123(url2, this.userToken).subscribe(res => {
        let response1 = JSON.parse(JSON.stringify(res));

        let alertOptions = []; let alertOptions123 = [];
        for (var i = 0; i < response.length; i++) {
          alertOptions.push({
            name: response[i].type,
            type: 'checkbox',
            label: response[i].type,
            value: response[i]
          })
        }
        for (var j = 0; j < response1.length; j++) {
          alertOptions123.push({
            name: response1[j].type,
            type: 'checkbox',
            label: response1[j].type,
            value: response1[j],
            checked: true
          })
        }

        const result = Object.values(
          [].concat(alertOptions, alertOptions123)
            .reduce((r, c) => (r[c.value.id] = Object.assign((r[c.value.id] || {}), c), r), {})
        );

        this.alertController.create({
          header: 'Select Device',
          inputs: result,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            }, {
              text: 'Ok',
              handler: (data) => {
                console.log('Confirm Ok', data);
                this.addNotification(veh, data);
              }
            }
          ]
        }).then((alertEl) => {
          alertEl.present();
        })
      },
      err => {
        console.log("again somthing got wrong!")
      })
    },
      err => {
        console.log("somethinf went wrong...");
      })
  }

  addNotification(veh, selectedData) {
    let url = '/api/permissions';
    let that = this;
    // outerthis.stoppageReport = [];
    var i = 0, howManyTimes = selectedData.length;
    function f() {
      let payload = {
        "deviceId": veh.id,
        "notificationId": selectedData[i].id
      }
      that.appService.postService123(url, that.userToken, payload).subscribe((resp: Response) => {
        let res = JSON.parse(JSON.stringify(resp));
        console.log("check permissions: ", res);
      },
        err => {
          console.log("got err: ", err);
        })
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  device_address(device, index) {
    let that = this;
    that.vehicleList[index].address = "N/A";
    if (!device.latitude) {
      that.vehicleList[index].address = "N/A";
      return;
    }
    let tempcord = {
      "lat": device.latitude,
      "long": device.longitude
    }

    this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.vehicleList[index].address = str;
      })

  }

  addDevice() {
    // debugger
    this.router.navigateByUrl("/maintabs/tabs/vehicle-list/add-device");
  }
}
