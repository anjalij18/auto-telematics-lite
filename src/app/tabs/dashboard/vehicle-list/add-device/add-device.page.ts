import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.page.html',
  styleUrls: ['./add-device.page.scss'],
})
export class AddDevicePage implements OnInit {
  categories: any = [
    {
      name: 'Arrow'
    },
    {
      name: 'Default'
    },
    {
      name: 'Animal'
    },
    {
      name: 'Bicycle'
    },
    {
      name: 'Boat'
    },
    {
      name: 'Bus'
    },
    {
      name: 'Car'
    },
    {
      name: 'Crane'
    },
    {
      name: 'Helicopter'
    },
    {
      name: 'Motorcycle'
    },
    {
      name: 'Offroad'
    },
    {
      name: 'Person'
    },
    {
      name: 'Pickup'
    },
    {
      name: 'Plane'
    },
    {
      name: 'Ship'
    },
    {
      name: 'Tractor'
    },
    {
      name: 'Train'
    },
    {
      name: 'Tram'
    },
    {
      name: 'Truck'
    },
    {
      name: 'Trollybus'
    },
    {
      name: 'Van'
    },
    {
      name: 'Scooter'
    }
  ]
  constructor(
    private apiCall: AppService,
    private ionStorage: Storage,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }
  userToken: any;
  ionViewDidEnter() {
    this.ionStorage.get('token').then((val) => {
      this.userToken = val;
    })
  }
  attempToSubmit: boolean = false;
  name: string; sim_num: number; model: any; category: string; imei: any;
  addDevice() {
    if (this.name === undefined && this.sim_num === undefined && this.model === undefined && this.imei === undefined && this.category === undefined) {
      this.attempToSubmit = true;
      return;
    }
    let url = '/api/devices';
    let payload = {
      "id": 1,
      "name": this.name,
      "uniqueId": this.imei,
      "phone": this.sim_num,
      "model": this.model,
      "contact": "",
      "category": String(this.category).toLowerCase(),
      "status": null,
      "lastUpdate": null,
      "groupId": 0,
      "disabled": false
    };
    this.apiCall.postService123(url, this.userToken, payload).subscribe((response: Response) => {
      let res = JSON.parse(JSON.stringify(response));
      if (res) {
        this.toastCtrl.create({
          message: 'Device has been added successfully!',
          duration: 3000,
          position: 'bottom'
        }).then((toastEl) => {
          toastEl.present();
          setTimeout(() => {
            this.navCtrl.pop();
          }, 3000);
        })
      }
    })
  }

}
