import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import * as _ from "lodash";
import { GoogleMap, GoogleMaps, GoogleMapsMapTypeId, LatLng, LatLngBounds, Marker, Spherical } from '@ionic-native/google-maps/ngx';
declare var cookieMaster: any;
declare var CordovaWebsocketPlugin: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  @ViewChild('mapElement', { static: true }) mapElement: ElementRef;

  map: GoogleMap;
  public cartCount: number = 0;
  // map;
  userToken: any;
  to: string;
  from: string;
  dashdata: any = {};
  vehData: any[];
  user = null;
  // socket: any;
  server_ip: any;
  constructor(
    public navCtrl: NavController,
    private ionicStorage: Storage,
    private appService: AppService,
    // private constUrl: URLs,
    private loadingCtrl: LoadingController,
    // private modalController: ModalController,
    private router: Router,
    // private events: Events,
    public googleMaps: GoogleMaps,
    public elementRef: ElementRef,
  ) {
    this.server_ip = localStorage.getItem("server_ip");
  }

  ionViewWillEnter() {}

  ionViewDidLeave() {
    let that = this;
    for (var r = 0; r < that.webSocketId.length; r++) {
      CordovaWebsocketPlugin.wsClose(that.webSocketId[r], 1000, "I'm done!");
    }
  }

  ionViewDidEnter() {
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userToken = val;
      this.getVehicleList();
    })
    let notifToken = localStorage.getItem("notificationTokens");
    if (notifToken !== null) {
      this.saveToken(notifToken);
    }
  }
  positionsArray: any = [];
  webSocketId: any = [];
  sockerConnection(loadEl) {
    let that = this;
    this.appService.getSessionwithToken(this.userToken.token).subscribe((resp: Response) => {
      console.log("intercept works or not ....", resp);
      cookieMaster.getCookieValue(that.server_ip + "/api/session", 'JSESSIONID', function (data) {
        // console.log("we got cookie... " + data.cookieValue);
        let options = {
          url: 'ws://128.199.21.17:8082/api/socket',
          pingInterval: 3000,
          headers: { "Cookie": "JSESSIONID=" + data.cookieValue },
        };
        loadEl.dismiss();
        CordovaWebsocketPlugin.wsConnect(options,
          function (recvEvent) {
            // debugger
            // console.log("Received callback from WebSocket: " + JSON.parse(JSON.stringify(recvEvent.message)));
            if (JSON.parse(recvEvent.message).positions) {
              var a = JSON.parse(recvEvent.message).positions;
              that.positionsArray = a;
              that.furtherCalc(that.positionsArray);
            }

            if (JSON.parse(recvEvent.message).devices) {
              var b = JSON.parse(recvEvent.message).devices;
              that.furtherCalc123(b);
            }

          },
          function (success) {
            console.log("Connected to WebSocket with id: " + success.webSocketId);
            that.webSocketId.push(success.webSocketId);
          },
          function (error) {
            console.log("Failed to connect to WebSocket: " +
              "code: " + error["code"] +
              ", reason: " + error["reason"] +
              ", exception: " + error["exception"]);
          });
      })
    },
      err => {
        loadEl.dismiss();
        console.log("intercept error ", err)
      })
  }
  data: any = [];
  furtherCalc123(b) {
    let that = this;
    var i = 0, howManyTimes = b.length;
    function f() {
      if (b[i].id === that.data[i].id) {
        that.data[i] = b[i];
      }
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }

  furtherCalc(a) {
    let that = this;

    var i = 0, howManyTimes = a.length;
    function f() {
      // debugger
      if (a[i].deviceId === that.data[i].id) {
        that.socketInit(a[i], that.data[i]);
      }
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }
  allData: any = {};
  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  socketInit(pdata, data_i, center = false) {
    this.allData.allKey = [];
    // this.socketChnl.push(pdata.deviceId);
    let that = this;
    if (pdata == undefined) {
      return;
    }
    // that.deviceDeatils = pdata;
    if (pdata.id != undefined && pdata.latitude != undefined && pdata.longitude != undefined) {
      var key = pdata.deviceId;

      if (that.allData[key]) {
        // that.socketSwitch[key] = pdata;

        if (that.allData[key].mark !== undefined) {
          that.allData[key].mark.setPosition(that.allData[key].poly[1]);
          that.allData[key].mark.setIcon(that.getIconUrl(pdata, data_i));
          var temp = _.cloneDeep(that.allData[key].poly[1]);
          that.allData[key].poly[0] = _.cloneDeep(temp);
          that.allData[key].poly[1] = { lat: pdata.latitude, lng: pdata.longitude };
          // let coordinates = {
          //   lat: pdata.latitude,
          //   long: pdata.longitude
          // }
          // that.getAddress(coordinates);
          // var speed = data.status == "RUNNING" ? data.last_speed : 0;
          // that.liveTrack(that.map, that.allData[key].mark, that.getIconUrl(pdata, data_i), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, data_i.category, data_i.status, that);
          that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
          that.liveTrack(that.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that);
        }
      } else {
        that.allData[key] = {};
        // that.socketSwitch[key] = pdata;
        that.allData.allKey.push(key);
        that.allData[key].poly = [];
        that.allData[key].poly.push({ lat: pdata.latitude, lng: pdata.longitude });

        // that.iconUrl = './assets/imgs/vehicles/running' + (this.data.category ? this.data.category : 'car') + '.png';
        // console.log("icon url check: ", this.iconUrl);
        that.showMergedMarkers(pdata, key, that.getIconUrl(pdata, data_i), center, data_i);
        // that.map.addMarker({
        //   title: data_i.name,
        //   // snippet: titleStr,
        //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
        //   icon: {
        //     url: that.getIconUrl(pdata, data_i),
        //     size: {
        //       height: 40,
        //       width: 20
        //     }
        //   },
        // }).then((marker: Marker) => {

        //   that.allData[key].mark = marker;
        //   ///////////////////////////////
        //   localStorage.setItem("SocketFirstTime", "SocketFirstTime");
        //   // marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
        //   //   .subscribe(e => {
        //   //     that.getAddressTitle(marker);

        //   //   });
        //   // let coordinates = {
        //   //   lat: pdata.latitude,
        //   //   long: pdata.longitude
        //   // }
        //   // that.getAddress(coordinates);
        //   // var speed = data.status == "RUNNING" ? data.last_speed : 0;
        //   that.liveTrack(that.map, that.allData[key].mark, that.getIconUrl(pdata, data_i), that.allData[key].poly, parseFloat(pdata.speed), 10, center, pdata.deviceId, that.elementRef, data_i.category, data_i.status, that);
        // });
      }
    }
  }

  showMergedMarkers(data, key, ic, center, data_i) {
    let that = this;
    /////////////// merge images using canvas

    let c: any = document.createElement("canvas");
    c.width = 100;
    c.height = 60;
    var ctx = c.getContext("2d");

    ctx.fillStyle = "#1c2f66";
    ctx.fillRect(0, 0, 100, 20);
    // ctx.strokeRect(0, 0, 100, 70);

    ctx.font = "9pt sans-serif";
    ctx.fillStyle = "white";
    ctx.fillText(data_i.name, 12, 15);

    var img = c.toDataURL();

    that.map.addMarker({
      // title: data.Device_Name,
      position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
      icon: {
        url: img,
        size: {
          height: 60,
          width: 100
        }
      },
    }).then((marker1: Marker) => {
      that.allData[key].mark1 = marker1;
      that.map.addMarker({
        // title: data.Device_Name,
        position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
        icon: {
          url: ic,
          size: {
            height: 40,
            width: 20
          }
        },
      }).then((marker: Marker) => {
        that.allData[key].mark = marker;
        // marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
        //   .subscribe(e => {
        //     if (that.selectedVehicle == undefined) {
        //       that.getAddressTitle(marker);
        //     } else {
        //       that.liveVehicleName = data.Device_Name;
        //       // that.drawerHidden = false;
        //       that.showaddpoibtn = true;
        //       that.drawerState = DrawerState.Docked;
        //       that.onClickShow = true;
        //     }
        //   });
        var speed = data.status == "RUNNING" ? data.last_speed : 0;
        that.liveTrack(that.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
      });
    });
  }

  getIconUrl(pdata, data_i) {
    var iconUrl;
    if (data_i.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/stopped' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'online' && pdata.attributes.ignition === true && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/idling' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'online' && pdata.attributes.ignition === false && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === false) {
      iconUrl = './assets/imgs/vehicles/stopped' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'online' && pdata.attributes.ignition === undefined && pdata.attributes.motion === true) {
      iconUrl = './assets/imgs/vehicles/running' + (data_i.category ? data_i.category : 'car') + '.png';
    } else if (data_i.status === 'offline' || data_i.status === 'unknown') {
      iconUrl = './assets/imgs/vehicles/offline' + (data_i.category ? data_i.category : 'car') + '.png';
    }
    console.log("vehicle name: ", data_i.name)
    console.log("check status: ", data_i.status)
    console.log("check ignition: ", pdata.attributes.ignition)
    console.log("check motion: ", pdata.attributes.motion)
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }

  // getAddressTitle(marker) {
  //   let that = this;
  //   this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
  //     .then(res => {
  //       var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //       debugger
  //       let snippetString = `Speed - ${Math.round(that.deviceDeatils.speed)} \nLast Update - ${that.datePipe.transform(new Date(that.data.lastUpdate).toISOString(), 'medium')}\nAddress - ${str}`;
  //       marker.setSnippet(snippetString);
  //     })
  // }
  liveTrack(map, mark, mark1, coords, speed, delay, center, id, that) {
    // liveTrack(map, mark, mark1, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;


      // console.log("issue coming from livetrack 1", mark)
      var lat = 0;
      var lng = 0;
      if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
        lat = mark.getPosition().lat;
        lng = mark.getPosition().lng;
      }


      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(new LatLng(lat, lng));
          // }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          if (that.displayNames) {
            mark1.setPosition(new LatLng(lat, lng));
          }

          /////////
          // that.circleObj.setCenter(new LatLng(lat, lng))
          ////////
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(dest);
          // }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          if (that.displayNames) {
            mark1.setPosition(dest);
          }
          /////////
          // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
          ////////
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  ngOnInit(): void { }

  ngAfterContentInit(): void {
    // this.map = new google.maps.Map(
    //   this.mapElement.nativeElement,
    //   {
    //     disableDefaultUI: true,
    //     mapTypeControl: true,
    //     streetViewControl: true
    //   }
    // );
    this.initMap();
  }

  initMap() {
    if (this.map) {
      this.map.remove();
    }
    let style = [];
    let mapOptions = {
      backgroundColor: 'white',
      controls: {
        compass: false,
        zoom: true,
      },
      gestures: {
        rotate: true,
        tilt: false
      },
      styles: style
    }
    if (this.map === undefined) {
      this.map = GoogleMaps.create('mapElement', mapOptions);
    }

  }

  gotoDetail(key) {
    console.log('check key: ', key)
    this.router.navigateByUrl('/maintabs/tabs/vehicle-list/' + key);
  }

  getVehicleList() {
    this.dashdata.offlineData = [];
    this.dashdata.onlineData = [];

    let _baseUrl = '/api/devices?entityId=' + this.userToken.id;

    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loading data...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService123(_baseUrl, this.userToken)
        .subscribe(respData => {

          loadingEl.dismiss();
          let res = JSON.parse(JSON.stringify(respData));
          this.data = res;
          for (var i = 0; i < res.length; i++) {
            if (res[i].status === 'offline') {
              this.dashdata.offlineData.push(res[i]);
              console.log("offline : " + this.dashdata.offlineData.length);
            } else if (res[i].status === 'online') {
              this.dashdata.onlineData.push(res[i]);
              console.log("onlineData : " + this.dashdata.onlineData.length);
            }
          }
          this.furtherFunc();
        },
          err => {
            loadingEl.dismiss();
          })
    });
  }

  // furtherFunc() {
  //   let url = '/api/session';
  //   this.loadingCtrl.create({
  //     message: 'Loading...',
  //     spinner: 'lines'
  //   }).then((loadEl) => {
  //     loadEl.present();
  //     this.appService.getServiceTemp123(url, this.userToken).subscribe(
  //       // this.appService.getService123(url, val).subscribe(
  //       response => {
  //         let res = JSON.parse(JSON.stringify(response));
  //         this.ionicStorage.set("token", res).then(() => {
  //           this.ionicStorage.get('token').then((val123) => {
  //             console.log('Your age is', val123);
  //             this.userToken = val123;
  //             /////////////////////////////
  //             this.sockerConnection(loadEl);
  //             ///////////////////
  //           })
  //         });
  //       },
  //       err => {
  //         loadEl.dismiss();
  //         console.log(err);
  //       });
  //   });
  // }
  mapData: any = [];
  furtherFunc() {
    let _bUrl = '/api/positions';
    this.appService.getService123(_bUrl, this.userToken).subscribe((response => {
      let resPositopns = JSON.parse(JSON.stringify(response));

      let _bUrl123 = '/api/devices';
      this.appService.getService123(_bUrl123, this.userToken).subscribe(res123 => {
        let resDevices = JSON.parse(JSON.stringify(res123));
        // this.data = resDevices;
        // console.log("check data response 111: ", resDevices);
        for (var i = 0; i < resDevices.length; i++) {
          for (var j = 0; j < resPositopns.length; j++) {
            if (resPositopns[j].deviceId === resDevices[i].id) {
              resPositopns[j].category = resDevices[i].category;
              resPositopns[j].name = resDevices[i].name;
              resPositopns[j].lastUpdate = resDevices[i].lastUpdate;
              resPositopns[j].status = resDevices[i].status;
            }
          }
        }
        this.vehData = resPositopns;

        this.vehData = resPositopns.map(function (d) {
          if (d.latitude !== undefined && d.longitude !== undefined) {
            return d;
          } else {
            return null;
          }
        });

        var filtered = this.vehData.filter(function (el) {
          return el != null;
        });
        this.mapData = filtered.map(function (d) {
          return { lat: d.latitude, lng: d.longitude };
        });
        let bounds = new LatLngBounds(this.mapData);
        this.map.moveCamera({
          target: bounds,
          zoom: 10
        });

        let url = '/api/session';
        this.loadingCtrl.create({
          message: 'Loading...',
          spinner: 'lines'
        }).then((loadEl) => {
          loadEl.present();
          this.appService.getServiceTemp123(url, this.userToken).subscribe(
            // this.appService.getService123(url, val).subscribe(
            response => {
              let res = JSON.parse(JSON.stringify(response));
              this.ionicStorage.set("token", res).then(() => {
                this.ionicStorage.get('token').then((val123) => {
                  console.log('Your age is', val123);
                  this.userToken = val123;
                  /////////////////////////////
                  this.sockerConnection(loadEl);
                  ///////////////////
                })
              });
            },
            err => {
              loadEl.dismiss();
              console.log(err);
            });
        });
      },
        err => {
          console.log(err);
        })
    }))

  }

  // furtherFunc(pData) {
  //   let that = this;
  //   // let tempArray = [];
  //   let _bUrl = '/api/positions';

  //   this.appService.getService123(_bUrl, this.userToken).subscribe((response => {
  //     let res = JSON.parse(JSON.stringify(response));

  //     let _bUrl123 = '/api/devices';
  //     this.appService.getService123(_bUrl123, this.userToken).subscribe(res123 => {
  //       let res111 = JSON.parse(JSON.stringify(res123));

  //       console.log("check data response 111: ", res111);
  //       for (var i = 0; i < res111.length; i++) {
  //         for (var j = 0; j < res.length; j++) {
  //           if (res[j].deviceId === res111[i].id) {
  //             res[j].category = res111[i].category;
  //             res[j].name = res111[i].name;
  //             res[j].lastUpdate = res111[i].lastUpdate;
  //             res[j].status = res111[i].status;
  //           }
  //         }
  //       }
  //       console.log("check data response 123: ", res);
  //       this.vehData = res;
  //       this.map.addListener('click', function (e) {
  //         console.log("clicked on map!!");
  //         // that.ngZone.run(() => {
  //         //   that.router.navigateByUrl('/maintabs/tabs/dashboard/live');
  //         // });
  //       });
  //       this.vehData = res.map(function (d) {
  //         if (d.latitude !== undefined && d.longitude !== undefined) {
  //           return d;
  //         } else {
  //           return null;
  //         }
  //       });
  //       var filtered = this.vehData.filter(function (el) {
  //         return el != null;
  //       });
  //       var infowindow = new google.maps.InfoWindow();
  //       var bounds = new google.maps.LatLngBounds();
  //       for (var i = 0; i < filtered.length; i++) {
  //         let image;
  //         console.log("check category: ", filtered[i].category)
  //         if (filtered[i].status === 'online' && filtered[i].attributes.ignition === true && filtered[i].attributes.motion === true) {

  //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/running' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
  //             new google.maps.Size(20, 40),
  //             new google.maps.Point(0, 0),
  //             new google.maps.Point(0, 40));
  //         } else if (filtered[i].status === 'online' && filtered[i].attributes.ignition === false && filtered[i].attributes.motion === false) {

  //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/stopped' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
  //             new google.maps.Size(20, 40),
  //             new google.maps.Point(0, 0),
  //             new google.maps.Point(0, 40));
  //         } else if (filtered[i].status === 'online' && filtered[i].attributes.ignition === true && filtered[i].attributes.motion === false) {

  //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/idling' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
  //             new google.maps.Size(20, 40),
  //             new google.maps.Point(0, 0),
  //             new google.maps.Point(0, 40));
  //         } else if (filtered[i].status === 'offline') {

  //           image = new google.maps.MarkerImage('./assets/imgs/vehicles/offline' + (filtered[i].category ? filtered[i].category : 'car') + '.png',
  //             new google.maps.Size(20, 40),
  //             new google.maps.Point(0, 0),
  //             new google.maps.Point(0, 40));
  //         }
  //         var beach = filtered[i];
  //         var myLatLng = new google.maps.LatLng(beach.latitude, beach.longitude);
  //         var marker = new google.maps.Marker({
  //           position: myLatLng,
  //           map: this.map,
  //           icon: image,
  //           // shape: shape,
  //           title: beach.deviceId,
  //           zIndex: i + 1
  //         });
  //         bounds.extend(myLatLng);
  //         var contentString = `${filtered[i].name}`;
  //         // var contentString = `Name - ${filtered[i].name}\nSpeed - ${(filtered[i].speed).toFixed(2)} km/hr\nLast Updated - ${(filtered[i].lastUpdate ? that.datePipe.transform(new Date(filtered[i].lastUpdate), 'mediumDate') : null)}`;
  //         var infowindow = new google.maps.InfoWindow({
  //           content: contentString
  //         });
  //         infowindow.open(this.map, marker);
  //       }
  //       this.map.fitBounds(bounds);
  //     },
  //       err => {
  //         console.log(err);
  //       })
  //     // debugger

  //   }))

  // }

  // async presentModal() {
  //   const modal = await this.modalController.create({
  //     component: LiveComponent
  //   });
  //   return await modal.present();
  // }

  afterClick: boolean = false;
  afterMapTypeClick: boolean = false;
  onTraffic() {
    // debugger
    this.afterClick = !this.afterClick;
    if (this.afterClick) {
      this.map.setTrafficEnabled(true);
    } else {
      this.map.setTrafficEnabled(false);
    }
  }

  onMapType() {
    // debugger
    this.afterMapTypeClick = !this.afterMapTypeClick;
    if (this.afterMapTypeClick) {
      this.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      this.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    }
  }

  saveToken(notifToken) {
    let url = '/api/session';
    this.ionicStorage.get('token').then((user) => {
      this.appService.getServiceTemp123(url, user).subscribe(
        response => {
          let res = JSON.parse(JSON.stringify(response));
          this.ionicStorage.set("token", res).then(() => {
            this.ionicStorage.get('token').then((val123) => {
              console.log('Your age is', val123);

              // debugger
              // if (!user['attributes']['notificationTokens'] || user['attributes']['notificationTokens'].indexOf(token) < 0) {
              if (!val123['attributes']['notificationTokens']) {
                let tokens = notifToken;
                let stringSplited = tokens.split(',');
                this.UpdateAppToken(stringSplited, val123);
              } else {
                //////////////////////////////////////////////

                let tokens = val123['attributes']['notificationTokens'] += ',' + notifToken;
                let stringSplited = tokens.split(',');

                var obj = {};

                for (var i = 0, len = stringSplited.length; i < len; i++)
                  obj[stringSplited[i]] = stringSplited[i];

                stringSplited = new Array();
                for (var key in obj)
                  stringSplited.push(obj[key]);

                console.log(stringSplited);
                this.UpdateAppToken(stringSplited, val123);
                //////////////////////////////////////////////
              }
            })
          });
        },
        err => {
          console.log(err);
        });

    });
  }

  UpdateAppToken(token, user) {
    let url = '/api/users/' + user.id;
    user.attributes.notificationTokens = token.join();

    let payload = user;
    this.appService.putService123(url, user, payload).subscribe((resp: Response) => {
      let res = JSON.parse(JSON.stringify(resp));
      console.log(res);
    },
      err => {
        console.log("component error : ", err);
      })

  }
}

