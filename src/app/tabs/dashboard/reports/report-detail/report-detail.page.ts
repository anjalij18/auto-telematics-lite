import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { URLs } from "src/app/app.model";
import { Storage } from "@ionic/storage";
import { LoadingController } from "@ionic/angular";
import * as moment from "moment";
import { ActivatedRoute } from "@angular/router";
import { GeocoderAPIService } from 'src/app/geocoder-api.service';
declare var cookieMaster: any;

@Component({
  selector: "app-report-detail",
  templateUrl: "./report-detail.page.html",
  styleUrls: ["./report-detail.page.scss"]
})
export class ReportDetailPage implements OnInit {
  report_name: any;
  overSpeeddevice_id: any;
  ignitiondevice_id: any;
  device_id: any;
  overspeedReport: any[] = [];
  stoppageReport: any[] = [];
  summaryReport: any[] = [];
  tripReport: any[] = [];
  port: any;
  userdetails: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  url: string;
  constructor(
    private appService: AppService,
    private constURL: URLs,
    private ionicStorage: Storage,
    private loadingCtrl: LoadingController,
    private activateRoute: ActivatedRoute,
    private geocoderApi: GeocoderAPIService
  ) {
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log("start date", this.datetimeStart);
    this.datetimeEnd = moment().format();
    console.log("stop date", this.datetimeEnd);

    this.activateRoute.paramMap.subscribe(parammap => {
      if (!parammap.get("id")) {
        return;
      }
      this.report_name = parammap.get("id");

      console.log("report name", this.report_name);
    });

    this.url = this.constURL.mainUrl + 'gps/getaddress';

  }

  ngOnInit() {
    this.ionicStorage.get("token").then(val => {
      console.log("Your age is", val);
      this.userdetails = val;

    });
  }

  getReport(reportId) {
    console.log("inside", reportId);
    this.report_name = reportId;
    let _bUrl;
    if (this.VehData == undefined) {
      alert("choose vehicle first");
      return;
    }
    if (reportId === "events") {
      _bUrl = '/api/reports/events?deviceId=' + this.VehData.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      this.callInnerFunction(_bUrl);
      // this.getReport(this.report_name);
    } else if (reportId === "Summary") {
      _bUrl = '/api/reports/summary?deviceId=' + this.VehData.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    } else if (reportId === "Stoppage") {
      _bUrl = '/api/reports/stops?deviceId=' + this.VehData.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    } else if (reportId === "Trip") {
      _bUrl = '/api/reports/trips?deviceId=' + this.VehData.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    }
  }


  getOverspeeddevice(selectedVehicle) {
    console.log(selectedVehicle);
    this.overSpeeddevice_id = selectedVehicle.Device_ID;
  }
  getIgnitiondevice(selectedVehicle) {
    this.ignitiondevice_id = selectedVehicle.Device_ID;
  }
  getdevice_id(selectedVehicle) {
    this.device_id = selectedVehicle.Device_ID;
  }

  // getdevices() {
  //   var baseURLp =
  //     this.constURL.mainUrl + "devices/getDeviceByUser?id=" +
  //     this.userdetails._id +
  //     "&email=" +
  //     this.userdetails.email;

  //   if (this.userdetails.isSuperAdmin == true) {
  //     baseURLp += "&supAdmin=" + this.userdetails._id;
  //   } else {
  //     if (this.userdetails.isDealer == true) {
  //       baseURLp += "&dealer=" + this.userdetails._id;
  //     }
  //   }
  //   let that = this;
  //   this.loadingCtrl
  //     .create({
  //       keyboardClose: true,
  //       message: "Loading Vehicle List.."
  //     })
  //     .then(loadingEl => {
  //       loadingEl.present();
  //       this.appService.getService(baseURLp).subscribe(resp => {
  //         loadingEl.dismiss();
  //         that.portstemp = resp["devices"];
  //       });
  //     });
  // }
  userToken: any;
  vehicleList: any = [];
  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      this.userToken = val;
      console.log("user token: ", this.userToken);
      this.vehicleList = [];
      this.getVehicleList();
    });
  }

  getVehicleList() {
    let baseURLp = '/api/devices?entityId=' + this.userToken.id;
    // let that = this;
    this.appService.getService123(baseURLp, this.userToken).subscribe(resp => {
      this.vehicleList = JSON.parse(JSON.stringify(resp));
    },
      err => {
        console.log("encountered err: ", err);
      });

  }
  VehData: any;
  portChange(ev) {
    console.log(ev.value);
    console.log("check port value : ", this.port)
    this.VehData = ev.value;
    let _bUrl;
    if (this.report_name === "events") {
      // this.getReport(this.report_name);
      this.eventsData = [];
      _bUrl = '/api/reports/events?deviceId=' + this.VehData.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      this.callInnerFunction(_bUrl);
    } else if (this.report_name === "Summary") {
      this.summaryReport = [];
      _bUrl = '/api/reports/summary?deviceId=' + ev.value.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    } else if (this.report_name === "Stoppage") {
      this.stoppageReport = [];
      _bUrl = '/api/reports/stops?deviceId=' + ev.value.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    } else if (this.report_name === "Trip") {
      this.tripReport = [];
      _bUrl = '/api/reports/trips?deviceId=' + ev.value.id + '&from=' + new Date(this.datetimeStart).toISOString() + '&to=' + new Date(this.datetimeEnd).toISOString();
      // this.getReport(this.report_name);
      this.callInnerFunction(_bUrl);
    }
  }
  eventsData: any = [];
  callInnerFunction(_bUrl) {
    let that = this;
    let _SUrl = '/api/session';
    this.appService.getCookieWithSession(_SUrl, this.userToken)
      .subscribe((response: Response) => {
        let res = JSON.parse(JSON.stringify(response));
        console.log("session res: ", res);
        // cookieMaster.getCookieValue("http://128.199.21.17:8082/api/session", 'JSESSIONID', function (data) {
        //   console.log("we got cookie... " + data.cookieValue);

        that.appService.getReportData(_bUrl, that.userToken).subscribe((response: Response) => {
          let res = JSON.parse(JSON.stringify(response));
          console.log("check report data: ", res);
          if (that.report_name === "Summary") {
            if (res.length > 0) {
              that.innerFunc2(res);
            }
          } else if (that.report_name === "Stoppage") {
            if (res.length > 0) {
              that.innerFunc1(res);
            }
          } else if (that.report_name === "Trip") {
            if (res.length > 0) {
              that.tripFunction(res);
            }
          } else if (that.report_name === 'events') {
            that.eventsData = res;
          }
        },
          err => {
            console.log("getting error from reports", err)
          });
        // })
      },
        err => {
          console.log("session err: ", err);
        });
  }

  splitString(str) {
    const name = str;
    const nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1)
    let str1 = nameCapitalized.match(/[A-Z][a-z]+/g);
    return str1[0] + ' ' + str1[1];
  }
  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
    var h = absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    // return h + ':' + m + ':' + s;
    // return h + 'h ' + m + 'min';
    // if (h === 0) {
    //   return m + ' m';
    // } else {
    if (m === '00') {
      return '0 m';
    } else {
      return h + ' h ' + m + ' m';
    }
    // }


  }

  meterIntoKelometer(value) {
    var km = value / 1000;
    return km.toFixed(1);
    // alert(km.toFixed(1) + " km"); // 1613.8 km
  }

  getOverSpeedReport() {
    var baseUrl;
    if (this.overSpeeddevice_id === undefined) {
      baseUrl =
        this.constURL.mainUrl + "notifs/overSpeedReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id;
    } else {
      baseUrl =
        this.constURL.mainUrl + "notifs/overSpeedReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id +
        "&device=" +
        this.overSpeeddevice_id;
    }
    const that = this;
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Loading Overspeed Report"
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(baseUrl).subscribe(resp => {
          console.log(resp);
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.innerFunc(JSON.parse(JSON.stringify(resp)));
          }
        });
      });
  }

  innerFunc(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {

      outerthis.overspeedReport.push({
        'vehicleName': data[i].vehicleName,
        'overSpeed': data[i].overSpeed,
        'start_location': {
          'lat': data[i].lat,
          'long': data[i].long
        },
      });
      outerthis.start_address(outerthis.overspeedReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.overspeedReport[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.overspeedReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }

    this.geocoderApi.reverseGeocode(tempcord.lat, tempcord.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.overspeedReport[index].StartLocation = str;
      })

  }

  innerFunc1(data) {
    let outerthis = this;
    outerthis.stoppageReport = [];
    var i = 0, howManyTimes = data.length;
    function f() {
      outerthis.stoppageReport.push({
        'arrival_time': new Date(data[i].startTime).toISOString(),
        'departure_time': new Date(data[i].endTime).toISOString(),
        'Durations': outerthis.parseMillisecondsIntoReadableTime(data[i].duration),
        'device': data[i].deviceName,
        'end_location': {
          'lat': data[i].latitude,
          'long': data[i].longitude
        }
      });
      outerthis.end_address(outerthis.stoppageReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();

  }

  end_address(item, index) {
    let that = this;
    that.stoppageReport[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.stoppageReport[index].EndLocation = "N/A";
      return;
    }
    let tempcord1234 = {
      "lat": item.end_location.lat,
      "long": item.end_location.long
    }
    that.geocoderApi.reverseGeocode(tempcord1234.lat, tempcord1234.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.stoppageReport[index].EndLocation = str;
      });
  }

  getSummaryReport() {
    var basep;
    let th = this;
    if (this.device_id === undefined) {
      basep =
        this.constURL.mainUrl + "summary/summaryReport?from=" +
        new Date(this.datetimeStart).toISOString() +
        "&to=" +
        new Date(this.datetimeEnd).toISOString() +
        "&user=" +
        this.userdetails._id;
    } else {
      basep =
        this.constURL.mainUrl + "summary/summaryReport?from=" +
        new Date(this.datetimeStart).toISOString() +
        "&to=" +
        new Date(this.datetimeEnd).toISOString() +
        "&user=" +
        this.userdetails._id +
        "&device=" +
        this.device_id;
    }
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Fetching data..."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(basep).subscribe(resp => {
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.innerFunc2(JSON.parse(JSON.stringify(resp)));
          }
        });
      });
  }

  milesphTOkmph(mph) {
    return mph * 1.609344;
  }

  innerFunc2(data) {
    let outerthis = this;
    outerthis.summaryReport = [];
    var i = 0, howManyTimes = data.length;
    function f() {
      // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
      // var hourconversion = 2.7777778 / 10000000;
      outerthis.summaryReport.push(
        {
          'Device_Name': data[i].deviceName,
          'engineHours': (data[i].engineHours ? outerthis.parseMillisecondsIntoReadableTime(data[i].engineHours) : '0 h 0 m'),
          'averageSpeed': (data[i].averageSpeed ? outerthis.milesphTOkmph(data[i].averageSpeed) : 0),
          'startOdometer': (data[i].startOdometer ? outerthis.meterIntoKelometer(data[i].startOdometer) : 0),
          'endOdometer': (data[i].endOdometer ? outerthis.meterIntoKelometer(data[i].endOdometer) : 0),
          'distance': outerthis.meterIntoKelometer(data[i].distance),
          'spentFuel': data[i].spentFuel ? data[i].spentFuel : '0.0',
          'maxSpeed': data[i].maxSpeed ? outerthis.milesphTOkmph(data[i].maxSpeed) : '0.0'
        });

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();

  }

  getTripReport() {
    var baset;
    let th = this;
    if (this.device_id === undefined) {
      baset =
        this.constURL.mainUrl + "user_trip/trip_detail?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&uId=" +
        this.userdetails._id;
    } else {
      baset =
        this.constURL.mainUrl + "user_trip/trip_detail?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&uId=" +
        this.userdetails._id +
        "&device=" +
        this.device_id;
    }
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Fetching data..."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(baset).subscribe(resp => {
          console.log(resp);
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.tripFunction(JSON.parse(JSON.stringify(resp)));
          }
          // th.tripReport = JSON.parse(JSON.stringify(resp));
        });
      });
  }


  tripFunction(data) {
    let that = this;
    that.tripReport = [];
    var i = 0, howManyTimes = data.length;
    function f() {
      var deviceId = data[i].deviceId;
      var distanceBt = that.meterIntoKelometer(data[i].distance);

      var fd = new Date(data[i].startTime).getTime();
      var td = new Date(data[i].endTime).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
      that.tripReport.push(
        {
          'Device_Name': data[i].deviceName,
          'Device_ID': data[i].deviceId,
          // 'Startetime': Startetime,
          // 'Startdate': new Date(data[i].startTime).toISOString(),
          // 'Endtime': Endtime,
          // 'Enddate': new Date(data[i].endTime).toISOString(),
          'distance': distanceBt,
          '_id': deviceId,
          'start_time': data[i].startTime,
          'end_time': data[i].endTime,
          'duration': Durations,
          'end_location': {
            'lat': data[i].endLat,
            'long': data[i].endLon
          },
          'start_location': {
            'lat': data[i].startLat,
            'long': data[i].startLon
          }
        });

      that.start_add(that.tripReport[i], i);
      that.end_add(that.tripReport[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();

  }

  start_add(item, index) {
    let that = this;
    that.tripReport[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.tripReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord12 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.geocoderApi.reverseGeocode(tempcord12.lat, tempcord12.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.tripReport[index].StartLocation = str;
      });
  }

  end_add(item, index) {
    let that = this;
    that.tripReport[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.tripReport[index].EndLocation = "N/A";
      return;
    }
    let tempcord1 = {
      "lat": item.end_location.lat,
      "long": item.end_location.long
    }

    this.geocoderApi.reverseGeocode(tempcord1.lat, tempcord1.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.tripReport[index].EndLocation = str;
      });
  }
}
