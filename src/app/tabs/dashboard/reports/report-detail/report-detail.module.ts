import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReportDetailPage } from './report-detail.page';
import { GeocoderAPIService } from 'src/app/geocoder-api.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: ReportDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IonicSelectableModule
  ],
  declarations: [ReportDetailPage],
  providers: [GeocoderAPIService, NativeGeocoder]
})
export class ReportDetailPageModule { }
