import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Storage } from '@ionic/storage';
import { GoogleMap, GoogleMaps, Spherical, MyLocation, LatLng, Marker, ILatLng, Circle, TileOverlayOptions, TileOverlay, GoogleMapsEvent } from '@ionic-native/google-maps/ngx';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-geofence',
  templateUrl: './add-geofence.page.html',
  styleUrls: ['./add-geofence.page.scss'],
})
export class AddGeofencePage implements OnInit {
  @ViewChild('map111', {
    static: true
  }) element: ElementRef;
  map111: GoogleMap;

  constructor(
    private apiCall: AppService,
    private ionStorage: Storage,
    public elementRef: ElementRef,
    private toastCtrl: ToastController,
    private navCtrl: NavController) { }

  ngOnInit() {
  }
  userToken: any;
  ionViewDidEnter() {
    this.ionStorage.get('token').then((val) => {
      this.userToken = val;
    })
  }
  showMap: boolean = false;
  addArea() {
    this.showMap = true;
    this.initMap();
  }

  latlat: any;
  lnglng: any;
  circleData: any;
  fence: number = 200;
  initMap() {
    if (this.map111 === undefined) {
      let mapOptions = {
        gestures: {
          rotate: false,
          tilt: false,
          compass: false
        }
      }
      let that = this;
      this.map111 = GoogleMaps.create('map_geofence', mapOptions);
      this.map111.getMyLocation()
        .then((location: MyLocation) => {
          console.log(JSON.stringify(location, null, 2));
          this.latlat = location.latLng.lat;
          this.lnglng = location.latLng.lng
          // Move the map camera to the location with animation
          this.map111.animateCamera({
            target: location.latLng,
            zoom: 14,
            tilt: 30,
            duration: 500
          }).then(() => {
            // this.map111.addMarker({
            //   title: 'My Location',
            //   draggable: true,
            //   position: new LatLng(location.latLng.lat, location.latLng.lng),
            // }).then((marker: Marker) => {
            //   console.log("Marker added")
            //   marker.on(GoogleMapsEvent.MARKER_DRAG_END)
            //     .subscribe(() => {

            //       // this.markerlatlong = marker.getPosition();

            //     });
            // })

            // if (that.circleData != undefined) {
            //   that.circleData.remove();
            // }

            // let ltln: ILatLng = { "lat": location.latLng.lat, "lng": location.latLng.lng };
            // let circle: Circle = this.map111.addCircleSync({
            //   'center': ltln,
            //   'radius': that.fence,
            //   'strokeColor': '#7044ff',
            //   'strokeWidth': 2,
            //   'fillColor': 'rgb(112, 68, 255, 0.5)'
            // });

            // that.circleData = circle;
            // console.log("circle data: => " + that.circleData)

          });

          if (that.circleData != undefined) {
            that.circleData.remove();
          }

          ////////////////////////////////////////
          let marker: Marker = this.map111.addMarkerSync({
            position: location.latLng,
            draggable: true
          });
          // Add circle
          let circle: Circle = this.map111.addCircleSync({
            center: marker.getPosition(),
            radius: 10,
            fillColor: "rgba(0, 0, 255, 0.5)",
            strokeColor: "rgba(0, 0, 255, 0.75)",
            strokeWidth: 1
          });
          // circle.center = marker.position
          marker.bindTo("position", circle, "center");
          that.circleData = circle;
        });
    }
  }

  callThis(fence) {
    let that = this;
    that.circleData.setRadius(fence);
  }
  name: string; description: any;
  addGeofence() {

    if (this.name === undefined || this.fence === 0 || this.circleData === undefined) {
      this.toastCtrl.create({
        message: 'Please enter the name and select the geofence area to proceed further...',
        duration: 3000,
        position: 'middle'
      }).then((toastEl) => {
        toastEl.present();
      })
      return;
    }
    // debugger
    // console.log("bounds: " + this.circleData.getBounds())
    // console.log("check center: ", this.circleData.getCenter());
    // console.log("check radius: ", this.circleData.getRadius());
    let circleString = `CIRCLE (${this.circleData.getCenter().lat} ${this.circleData.getCenter().lng}, ${this.circleData.getRadius()})`;
    let url = '/api/geofences';
    let payload = {
      "name": this.name,
      "description": (this.description ? this.description : null),
      "area": circleString,
      "calendarId": 0
    }

    this.apiCall.postService123(url, this.userToken, payload).subscribe((response: Response) => {
      let res = JSON.parse(JSON.stringify(response));
      if (res) {
        this.toastCtrl.create({
          message: 'Geofence created successfully.',
          duration: 2000,
          position: 'middle'
        }).then((toastEl) => {
          toastEl.present();
        });
        setTimeout(() => {
          this.navCtrl.pop();
        }, 2000);
      }
      // console.log(res);
    },
      err => {
        console.log("geofence error", err);
      })
  }

}
