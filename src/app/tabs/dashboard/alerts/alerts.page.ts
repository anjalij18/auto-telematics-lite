import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { Storage } from "@ionic/storage";
import { Events } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: "app-alerts",
  templateUrl: "./alerts.page.html",
  styleUrls: ["./alerts.page.scss"]
})
export class AlertsPage implements OnInit {
  userData: any;
  // limit: number = 50;
  // pageNo: number = 1;
  geofences: any = [];
  constructor(private events: Events, private appService: AppService, private ionicStorage: Storage, private router: Router) { }
  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      this.userData = val;
      this.events.publish('cart:updated', 0);
      this.getGeofences();
    });
  }
  ngOnInit() { }
  getGeofences() {
    var _bUrl = '/api/geofences';
    this.appService.getService123(_bUrl, this.userData).subscribe(resp => {
      console.log("geofences: " + resp);
      this.geofences = JSON.parse(JSON.stringify(resp));
      console.log(this.geofences);
    },
      err => {
        console.log(err);
      });
  }

  addGeofence() {
    this.router.navigateByUrl('maintabs/tabs/alerts/add-geofence');
  }
}
