import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { TabsRoutingModule } from './tabs-routing.module';
import { SecondTabsPageModule } from './dashboard/vehicle-list/second-tabs/second-tabs.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsRoutingModule,
    SecondTabsPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
