import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DataResolverService } from '../resolver/data-resolver.service';
import { TabsPage } from "./tabs.page";
// import { SecondTabsPage } from './dashboard/vehicle-list/second-tabs/second-tabs.page';

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "dashboard",
        children: [
          {
            path: "",
            loadChildren: "./dashboard/dashboard.module#DashboardPageModule"
          }
        ]
      },
      {
        path: 'vehicle-list',
        children: [
          {
            path: '',
            loadChildren: "./dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule"
          },
          {
            path: 'add-device',
            loadChildren: './dashboard/vehicle-list/add-device/add-device.module#AddDevicePageModule'
          },
          {
            path: 'add-attribute/:id',
            resolve: {
              vehicle_data: DataResolverService
            },
            loadChildren: './dashboard/vehicle-list/add-attribute/add-attribute.module#AddAttributePageModule'
          },
          {
            path: "second-tabs",
            loadChildren:
              "./dashboard/vehicle-list/second-tabs/second-tabs.module#SecondTabsPageModule"
          }
        ]
      },
      {
        path: "reports",
        children: [
          {
            path: "",
            loadChildren: "./dashboard/reports/reports.module#ReportsPageModule"
          },
          {
            path: ":id",
            loadChildren:
              "./dashboard/reports/report-detail/report-detail.module#ReportDetailPageModule"
          },
          {
            path: "",
            redirectTo: "/maintabs/tabs/dashboard",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "alerts",
        children: [
          {
            path: "",
            loadChildren: "./dashboard/alerts/alerts.module#AlertsPageModule"
          },
          {
            path: 'add-geofence',
            loadChildren: './dashboard/alerts/add-geofence/add-geofence.module#AddGeofencePageModule'
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: './dashboard/map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: "settings",
        children: [
          {
            path: '',
            loadChildren: './dashboard/settings/settings.module#SettingsPageModule'
          },
          {
            path: 'users',
            children: [
              {
                path: '',
                loadChildren: './dashboard/settings/users/users.module#UsersPageModule'

              },
              {
                path: 'add-user',
                loadChildren: './dashboard/settings/users/add-user/add-user.module#AddUserPageModule'
              }
            ]
          }
        ]
      },
      {
        path: "",
        redirectTo: "/maintabs/tabs/dashboard",
        pathMatch: "full"
      }
    ]
  }
  // { path: 'map', loadChildren: './dashboard/map/map.module#MapPageModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule { }
