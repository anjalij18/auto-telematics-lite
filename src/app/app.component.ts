import { Component, QueryList, ViewChildren } from '@angular/core';

import { Platform, IonRouterOutlet, ModalController, PopoverController, ActionSheetController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { DashboardPage } from './tabs/dashboard/dashboard.page';
import { TabsService } from './tabs/tabs.service';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { Firebase } from '@ionic-native/firebase/ngx';
// import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen/ngx';
// import { Storage } from '@ionic/storage';
// import { AppService } from './app.service';
// import { AuthService } from './services/auth.service';
// import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
// import { AppService } from './app.service';
// declare var cookieMaster: any;
// declare var lottie: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // set up hardware back button event.
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  rootPage: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    // private lottieSplashScreen: LottieSplashScreen,
    private statusBar: StatusBar,
    public tabs: TabsService,
    private authenticationService: AuthenticationService,
    private router: Router,
    // private apiCall: AppService,
    private navController: NavController,
    // private toastController: ToastController,
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    // private auth: AuthService
    // private push: Push
    private firebase: Firebase,
    // private ionStorage: Storage
  ) {
    this.initializeApp();
    // Initialize BackButton Eevent.
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.rootPage = DashboardPage;

      this.statusBar.styleDefault();
      //Check if user is logged-in; if so the dashboard will be loaded otherwise the login-page
      this.authenticationService.authState.subscribe(state => {
        if (state) {
          console.log("user is logged in");
          this.navController.navigateRoot(['maintabs/tabs/dashboard']);
          this.splashScreen.hide();
          // setTimeout(() => {
            this.splashScreen.hide();
          // }, 3000);
        } else {
          console.log("user is NOT logged in");
          this.navController.navigateRoot('fleet-manager');
          // setTimeout(() => {
            this.splashScreen.hide();
          // }, 3000);
        }
      });
      this.firebaseSetup();
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      // close side menua
      // try {
      //   const element = await this.menu.getOpen();
      //   if (element !== null) {
      //     this.menu.close();
      //     return;

      //   }

      // } catch (error) {

      // }

      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        debugger
        if (outlet && outlet.canGoBack()) {
          // outlet.pop();
          this.router.navigateByUrl('/maintabs')

        } else if (this.router.url === '/login') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            // this.platform.exitApp(); // Exit from app
            navigator['app'].exitApp(); // work for ionic 4

          } else {
            navigator['app'].exitApp();
            // if (this.router.isActive('/miantabs', true)) {
            // this.alertController.create({
            //   message: 'Do you want to exit the app?',
            //   buttons: [
            //     {
            //       text: 'YES PROCCED',
            //       handler: () => {
            //         navigator['app'].exitApp();
            //       }
            //     },
            //     {
            //       text: 'Back',
            //     }
            //   ]
            // }).then(alertEl => {
            //   alertEl.present();
            // })
            // }
            // this.toast.show(
            //   `Press back again to exit App.`,
            //   '2000',
            //   'center')
            //   .subscribe(toast => {
            //     // console.log(JSON.stringify(toast));
            //   });


            // this.toast.create({
            //   message: 'Press back again to exit App.',
            //   duration: 2000,
            //   position: 'middle'
            // }).then((toastEl => {
            //   toastEl.present();
            // }))
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      });
    });
  }

  firebaseSetup() {
    this.firebase.getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));

    this.firebase.onNotificationOpen()
      .subscribe(data => console.log(`User opened a notification ${data}`));

    this.firebase.onTokenRefresh()
      .subscribe((token: string) => {
        console.log(`Got a new token ${token}`);
        localStorage.setItem("notificationTokens", token);
        // this.saveToken(token);
      });
  }
}
