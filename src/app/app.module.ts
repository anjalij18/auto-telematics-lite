import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
// import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
// const config: SocketIoConfig = { url: 'ws://128.199.21.17:8082/api/socket', options: {} };
// const config: SocketIoConfig = { url: 'https://www.oneqlik.in/gps', options: {} };
// const notifConfig: SocketIoConfig = { url: 'https://www.oneqlik.in/notifIO', options: {} }
import { URLs } from './app.model';
import { TabsService } from './tabs/tabs.service';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './services/auth.guard';
// import { Push } from '@ionic-native/push/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AuthInterceptor } from './interceptors/token.interceptor';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { ModalFilterPage } from './tabs/dashboard/vehicle-list/second-tabs/more/more.page';
import { FormsModule } from '@angular/forms';

// import { Injectable } from '@angular/core';
// import { SocketIoModule } from 'ngx-socket-io';
// import { Socket } from 'ngx-socket-io';

// @Injectable({providedIn: 'root'})
// export class SocketOne extends Socket {

//   constructor() {
//     super({ url: 'https://www.oneqlik.in/gps', options: {} });
//   }

// }

// @Injectable({providedIn: 'root'})
// export class SocketTwo extends Socket {

//   constructor() {
//     super({ url: 'https://www.oneqlik.in/notifIO', options: {} });
//   }

// }
@NgModule({
  declarations: [AppComponent, ModalFilterPage],
  entryComponents: [ModalFilterPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    // SocketOne, SocketTwo,
    StatusBar,
    SplashScreen,
    // LottieSplashScreen,
    TabsService,
    URLs,
    GoogleMaps,
    NativeGeocoder,
    AuthGuard,
    AuthenticationService,
    // Push,
    Firebase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
