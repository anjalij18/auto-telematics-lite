import { Injectable } from '@angular/core';
import {
  NativeGeocoder,
  NativeGeocoderResult
} from '@ionic-native/native-geocoder/ngx';
import { Geocoder, GeocoderResult } from '@ionic-native/google-maps';

@Injectable({
  providedIn: 'root'
})
export class GeocoderAPIService {

  constructor(private _GEOCODE: NativeGeocoder) { }

  reverseGeocode(lat: number, lng: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this._GEOCODE.reverseGeocode(lat, lng)
        .then((result: NativeGeocoderResult[]) => {
          var a = result[0].thoroughfare ? result[0].thoroughfare : null;
          var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
          var c = result[0].subLocality ? result[0].subLocality : null;
          var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
          var e = result[0].postalCode ? result[0].postalCode : null;
          var f = result[0].locality ? result[0].locality : null;
          var g = result[0].countryName ? result[0].countryName : null;
          var h = result[0].administrativeArea ? result[0].administrativeArea : null;
          let str = '';
          if (a != null)
            str = a + ', ';
          if (b != null)
            str = str + b + ', ';
          if (c != null)
            str = str + c + ', ';
          if (d != null)
            str = str + d + ', ';
          if (e != null)
            str = str + e + ', ';
          if (f != null)
            str = str + f + ', ';
          if (g != null)
            str = str + g + ', ';
          if (h != null)
            str = str + h + ', ';
          resolve(str);
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  geocoderResult(lat: number, lng: number): Promise<any> {
    return new Promise((resolve, reject) => {
      Geocoder.geocode({
        "position": {
          lat: lat,
          lng: lng
        }
      }).then((results: GeocoderResult[]) => {
        var addr;
        if (results.length == 0) {
          addr = 'N/A';
          resolve(addr);
        } else {
          addr = results[0].extra.lines[0];
          resolve(addr);
        }
      }).catch((error: any) => {
        reject(error);
      });
    })
  }
}
