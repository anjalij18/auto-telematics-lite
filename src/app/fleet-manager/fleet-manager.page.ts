import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { AlertController } from '@ionic/angular';
declare var cookieMaster: any;

@Component({
  selector: 'app-fleet-manager',
  templateUrl: './fleet-manager.page.html',
  styleUrls: ['./fleet-manager.page.scss'],
})
export class FleetManagerPage implements OnInit {
  server_ip: string = 'http://128.199.21.17:8082';

  constructor(
    private router: Router,
    private apiCall: AppService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  onConnect() {
    if (this.server_ip === undefined) {
      return;
    }
    localStorage.setItem("server_ip", this.server_ip);
    let timestamp = new Date().getTime();
    let _burl = '/api/server?_dc=' + timestamp;
    this.apiCall.getServiceTemp(_burl)
      .subscribe((data: Response) => {
        // this.alertController.create({
        //   message: 'Connection successfull..!',
        //   buttons: ['Okay']
        // }).then(alertEl => {
        //   alertEl.present();
        // });
        this.router.navigate(['login']);
      },
        err => {
          console.log("got session error=> ", err);
          this.alertController.create({
            message: 'Connection failed.. please try with valid ip address!',
            buttons: ['Okay']
          }).then(alertEl => {
            alertEl.present();
          })
        });
    // }

  }

}
