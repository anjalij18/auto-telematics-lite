import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetManagerPage } from './fleet-manager.page';

describe('FleetManagerPage', () => {
  let component: FleetManagerPage;
  let fixture: ComponentFixture<FleetManagerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleetManagerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetManagerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
