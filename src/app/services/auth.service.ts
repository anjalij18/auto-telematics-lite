import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable, from, of } from 'rxjs';
import { take, map, switchMap } from 'rxjs/operators';
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { URLs } from '../app.model';

const helper = new JwtHelperService();
const TOKEN_KEY = 'jwt-token';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: Observable<any>;
  userData = new BehaviorSubject(null);
  // authState = new BehaviorSubject(false);
  // public authState = new BehaviorSubject(false);
  constructor(private constUrl: URLs, private storage: Storage, private http: HttpClient, private plt: Platform, private router: Router) { 
    this.loadStoredToken();  
  }

  loadStoredToken() {
    let platformObs = from(this.plt.ready());
 
    this.user = platformObs.pipe(
      switchMap(() => {
        return from(this.storage.get(TOKEN_KEY));
      }),
      map(token => {
        if (token) {
          let decoded = helper.decodeToken(token); 
          this.userData.next(decoded);
          // this.authState.next(true);
          return true;
        } else {
          // this.authState.next(false);
          return null;
        }
      })
    );
  }

  login(credentials: {emailid?: string, ph_num?:string, user_id?: string, psd: string }) {
    // Normally make a POST request to your APi with your login credentials
    if (credentials.emailid === undefined && credentials.psd === undefined) {
      return of(null);
    } else if(credentials.ph_num === undefined && credentials.psd === undefined) {
      return of(null);
    } else if(credentials.user_id === undefined && credentials.psd === undefined) {
      return of(null);
    }
 
    return this.http.post(this.constUrl.mainUrl + 'users/LoginWithOtp', credentials).pipe(
      take(1),
      map(res => {
        // Extract the JWT
        return JSON.parse(JSON.stringify(res)).token;
      }),
      switchMap(token => {
        let decoded = helper.decodeToken(token);
        this.userData.next(decoded);
 
        let storageObs = from(this.storage.set(TOKEN_KEY, token));
        return storageObs;
      })
    );
  }

  getUser() {
    return this.userData.getValue();
  }
 
  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      this.router.navigateByUrl('/');
      this.userData.next(null);
    });
  }
}
