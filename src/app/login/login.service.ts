import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _userIsAuthenticated = false;
  isLoading = false;
  constructor(
    private appService: AppService,
    private loadingCtrl: LoadingController,
    private router: Router
  ) { }

  get userIsAuthenticated() {
    return this._userIsAuthenticated;
  }

  login() {
    // this.isLoading = true;
    // this.loadingCtrl.create({
    //   keyboardClose: true, message: 'Loging in...'
    // }).then(loadingEl => {
    //   loadingEl.present();
    //   this.appService.authenticateUser(payLoad).subscribe(() => {
    //     this.isLoading = false;
    //     loadingEl.dismiss();
        this._userIsAuthenticated = true;
    //     this.router.navigateByUrl('/maintabs/tabs/dashboard');
    //   })
    // });
  }

  logout() {
    this._userIsAuthenticated = false;
  }
}