import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { LoadingController, ToastController } from "@ionic/angular";
import { AppService } from "../app.service";
import { LoginService } from "./login.service";
import { Storage } from "@ionic/storage";
import { AuthenticationService } from '../services/authentication.service';
// import { AuthService } from '../services/auth.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  isLogin = true;
  isLoading = false;
  fdata = "ngModel";
  f: any;

  credentials: any = {};
  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private authService: AppService,
    private authenticateService: AuthenticationService,
    private loginServ: LoginService,
    private ionicStorage: Storage,
    private toastController: ToastController
  ) { 
    localStorage.removeItem("password");
  }

  ngOnInit() { }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    this.credentials = {};
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const pass = form.value.password;

    this.credentials = {
      email: email,
      password: pass
    }
    if (this.isLogin) {
      this.loadingCtrl
        .create({
          keyboardClose: true,
          message: "Loging in..."
        })
        .then(loadingEl => {
          loadingEl.present();
          this.authService.authenticateUser(this.credentials).subscribe(
            response => {
              loadingEl.dismiss();
              form.reset();
              let res = JSON.parse(JSON.stringify(response));
              if (res.disabled === false) {
                this.authenticateService.login();
                localStorage.setItem("password", pass);
                this.ionicStorage.set("token", res).then(() => {
                  this.loginServ.login();
                  this.router.navigateByUrl("/maintabs/tabs/dashboard");
                });
              }
            },
            err => {
              loadingEl.dismiss();
              this.toastController
                .create({
                  message: "Login Falied. Please use valid credentials...",
                  duration: 1500,
                  position: "bottom"
                })
                .then(toastEl => {
                  toastEl.present();
                });
            });
        });
    } else {
      //send request to signup servers
    }
  }
}
